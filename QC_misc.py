###################################################################
# File: QC_misc.py                               ###    #####    ##
# File version: 28/01/2019                       ##  ##  ###  #####
# Python version: 3.*.*                          ##  ##  ###  #####
# Author: Msc. Martin Kalivoda                   ###   #  ###    ##
###################################################################

try:
    import arcpy, os, pandas, subprocess, datetime, smtplib, email, shutil, multiprocessing, time, sys, pdfkit, codecs, jinja2, copy, scipy.stats, traceback, laspy, scipy.linalg, json, math, random, string
    import numpy as np
    import matplotlib.pyplot as plt
except ImportError as e:
    print('Import error occured:', e)
    print('Some of the python modules are missing. Try to call this as an administrator:\n\npip install -r requirements.txt')
from email.mime.text import MIMEText
from components import constants as con
from vendor import rts as rtsComponent
from components import installCheck as installCheckComponent

class QC_misc:
    def __init__(self, lasPath, qcWorkPath, qcGdb, lotNum):    
        self.lotNumStr = "0" + str(lotNum) if len(str(lotNum)) == 1 else str(lotNum)
        self.lasPath = lasPath
        self.qcWorkPath = qcWorkPath

        if self.lotNumStr in qcGdb:
            self.qcGdb = qcGdb if qcGdb.lower().endswith('.gdb') else qcGdb + '.gdb'
        else:
            if qcGdb.lower().endswith('.gdb'):
                self.qcGdb = qcGdb[:-4] + '_' + self.lotNumStr + qcGdb[-4:]
            else:
                self.qcGdb = qcGdb + '_' + self.lotNumStr + '.gdb'
        
        self.assetsPath = con.ASSETS_PATH
        self.lotNum = lotNum
        self.cadastralAreasDir = con.CADASTRAL_AREAS_DIR
        self.cadastralAreasGdb = con.CADASTRAL_AREAS_GDB
        self.cadastralAreasFcTm34 = con.CADASTRAL_AREAS_FC_TM34
        self.cadastralAreasFcJtsk03 = con.CADASTRAL_AREAS_FC_JTSK03
        self.lidarLotsDir = con.LIDAR_LOTS_DIR
        self.lidarLotsGdb = con.LIDAR_LOTS_GDB
        self.lidarLotsFcTm34 = con.LIDAR_LOTS_FC_TM34
        self.lidarLotsFcJtsk03 = con.LIDAR_LOTS_FC_JTSK03
        self.snapRasterDir = con.SNAP_RASTER_DIR
        self.snapRasterRstTm34 = con.SNAP_RASTER_TM34
        self.vgiDir = con.VGI_DIR
        self.zbgisBuildingsDir = con.ZBGIS_BUILDINGS_DIR
        self.zbgisBuildingsGdb = con.ZBGIS_BUILDINGS_GDB
        self.zbgisBuildingsFcTm34 = con.ZBGIS_BUILDINGS_FC_TM34
        self.zbgisBuildingsFcJtsk03 = con.ZBGIS_BUILDINGS_FC_JTSK03
        self.zbgisWatersDir = con.ZBGIS_WATERS_DIR
        self.zbgisWatersGdb = con.ZBGIS_WATERS_GDB
        self.zbgisWatersFcTm34 = con.ZBGIS_WATERS_FC_TM34
        self.zbgisWatersFcJtsk03 = con.ZBGIS_WATERS_FC_JTSK03
        self.transformationsDir = con.TRANSFORMATIONS_DIR
        self.quasigeoidEtrs89hToBpv = con.QUASIGEOID_ETRS89H_TO_BPV
        self.dmr35Dir = con.DMR35_DIR
        self.dmr35etrs89tm34hRst = con.DMR35_TM34_H_RST
        self.shiftGridDir = con.SHIFT_GRID_DIR
        self.jtsk03ToJtskShiftGridFile = con.JTSK03_TO_JTSK_SHIFT_GRID_FILE
        self.inCheck = installCheckComponent.InstallCheck()
        self.inCheck.isLastoolsInstalled()

    def prettyNumber(self, value, decimalPlaces=6):
        if value is not None:
            return '{:,}'.format(round(float(value), decimalPlaces)).replace(',',' ').replace('.',',')
        else:
            return None

    def generateGrid1kmx1km(self):
        print("Creating 1km x 1km grid")
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        if arcpy.Exists("las_attributes_postpro_" + self.lotNumStr):
            las_attributes_fc = "las_attributes_postpro_" + self.lotNumStr
            desc = arcpy.Describe(las_attributes_fc)
        elif arcpy.Exists("las_attributes_prepro_" + self.lotNumStr):
            las_attributes_fc = "las_attributes_prepro_" + self.lotNumStr
            desc = arcpy.Describe(las_attributes_fc)
        else:
            raise RuntimeError("... There is no las_attributes_postpro_" + self.lotNumStr + " or las_attributes_postpro_" + self.lotNumStr + " feature class")
        arcpy.CreateFishnet_management('tempgrid', str(desc.extent.XMin) + " " + str(desc.extent.YMin), str(desc.extent.XMin) + " " + str(desc.extent.YMax), 1000, 1000,"","", str(desc.extent.XMax) + " " + str(desc.extent.YMax), 'NO_LABELS', las_attributes_fc, 'POLYGON')
        print("... cleaning grid")
        extent = arcpy.Polygon(arcpy.Array([arcpy.Point(desc.extent.XMin,desc.extent.YMin), arcpy.Point(desc.extent.XMin,desc.extent.YMax),arcpy.Point(desc.extent.XMax,desc.extent.YMax),arcpy.Point(desc.extent.XMax,desc.extent.YMin)]))
        arcpy.CreateFeatureclass_management("","tempextent", 'POLYGON')
        editor = arcpy.da.Editor(arcpy.env.workspace)
        editor.startEditing()
        editor.startOperation()
        ic = arcpy.da.InsertCursor("tempextent",["SHAPE@"])
        ic.insertRow([extent])
        del ic
        editor.stopOperation()
        editor.stopEditing(True)
        del editor
        arcpy.Erase_analysis("tempextent",las_attributes_fc,"temperase")
        arcpy.AddField_management("tempgrid", 'IS_FULLY_COVERED', 'SHORT', '', '', '' , '' , 'NULLABLE', 'NON_REQUIRED')
        arcpy.CalculateField_management("tempgrid", 'IS_FULLY_COVERED', 1)
        arcpy.MakeFeatureLayer_management('tempgrid',"grid_lyr")
        arcpy.SelectLayerByLocation_management('grid_lyr', 'INTERSECT', "temperase","","")
        arcpy.CalculateField_management('grid_lyr', 'IS_FULLY_COVERED', 0)

        where = "lot_number = '" + self.lotNumStr + "'"
        arcpy.MakeFeatureLayer_management(os.path.join(self.assetsPath, self.lidarLotsDir, self.lidarLotsGdb, self.lidarLotsFcTm34), 'temp_lot', where)
        arcpy.SelectLayerByLocation_management('grid_lyr', 'INTERSECT', "temp_lot", '', 'NEW_SELECTION', 'INVERT')
        arcpy.DeleteFeatures_management("grid_lyr")
        arcpy.CopyFeatures_management("grid_lyr","grid1kmx1km_" + self.lotNumStr)
        arcpy.AddField_management("grid1kmx1km_" + self.lotNumStr, 'ORIGOID', 'LONG', '', '', '' , '' , 'NULLABLE', 'NON_REQUIRED')
        arcpy.CalculateField_management("grid1kmx1km_" + self.lotNumStr, 'ORIGOID', '!OBJECTID!')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'tempextent')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temperase')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'tempgrid')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'grid_lyr')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_lot')

    def clearTempDirs(self, path):
        # Warning! This function removes all directories which name contains the string 'tempdir' (case insensitive).
        print("Removing temporary directories in the path ", path)
        temps = [temp for temp in os.listdir(path) if temp.lower().find("tempdir") >= 0]
        count = 0
        for temp in temps:
            try:
                os.remove(os.path.join(path, temp))
                count += 1
            except:
                print("... ", temp, " was not removed")
        print("... ", count, " temporary directories successfully removed")

    def clearTempLasDatasets(self, path):
        # Warning! This function removes all las datasets (.lasd) which name contains the string 'temp' (case insensitive).
        print("Removing temporary las datasets in the path ", path)
        temps = [temp for temp in os.listdir(path) if temp.lower().find("temp") >= 0 and temp.lower().endswith(".lasd")]
        count = 0
        for temp in temps:
            try:
                os.remove(os.path.join(path, temp))
                count += 1
            except:
                print("... ", temp, " was not removed")
        print("... ", count, " temporary directories successfully removed")

    def clearTempGdbs(self, path):
        # Warning! This function removes all geodatabases (.gdb) which name contains the string 'temp' (case insensitive).
        print("Removing temporary geodatabases in the path ", path)
        temps = [temp for temp in os.listdir(path) if temp.lower().find("temp") >= 0 and temp.lower().endswith(".gdb")]
        count = 0
        for temp in temps:
            try:
                os.remove(os.path.join(path, temp))
                count += 1
            except:
                print("... ", temp, " was not removed")
        print("... ", count, " temporary geodatabases successfully removed")

    def clearTempGdbItems(self):
        # Warning! This function removes all geodatabase items which name contains the string 'temp' (case insensitive).
        print("Removing temporary items in the geodatabase ", self.qcGdb)
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        temps = [temp for temp in arcpy.ListFeatureClasses() if temp.lower().find("temp")]
        for temp in temps:
            try:
                os.remove(os.path.join(path, temp))
            except:
                print("... ", temp, " was not removed")
        print("... ", len(temps), " temporary feature classes successfully removed")
        temps = [temp for temp in arcpy.ListRasters() if temp.lower().find("temp")]
        for temp in temps:
            try:
                os.remove(os.path.join(path, temp))
            except:
                print("... ", temp, " was not removed")
        print("... ", len(temps), " temporary rasters successfully removed")
        temps = [temp for temp in arcpy.ListTables() if temp.lower().find("temp")]
        for temp in temps:
            try:
                os.remove(os.path.join(path, temp))
            except:
                print("... ", temp, " was not removed")
        print("... ", len(temps), " temporary tables successfully removed")

    def createCentroidsFromLasFilenames(self, phase):
        print("Creating Centroids From Las Filenames")
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        if phase == 1:
            fc_las_attributes = "las_attributes_prepro_" + self.lotNumStr
            if not arcpy.Exists("las_attributes_prepro_" + self.lotNumStr):
                raise RuntimeError("Feature class las_attributes_prepro_" + self.lotNumStr + " does not exist. Please, create it first.")
        if phase == 2:
            fc_las_attributes = "las_attributes_postpro_" + self.lotNumStr
            if not arcpy.Exists("las_attributes_postpro_" + self.lotNumStr):
                raise RuntimeError("Feature class las_attributes_postpro_" + self.lotNumStr + " does not exist. Please, create it first.")
        arcpy.CreateFeatureclass_management(os.path.join(self.qcWorkPath, self.qcGdb), "las_centroids_from_filename", 'POINT')
        arcpy.AddField_management('las_centroids_from_filename', "lasid", 'GUID')
        arcpy.AddField_management('las_centroids_from_filename', "filename", 'TEXT')
        editor = arcpy.da.Editor(arcpy.env.workspace)
        editor.startEditing()
        editor.startOperation()
        ic = arcpy.da.InsertCursor("las_centroids_from_filename", ["SHAPE@", "lasid", "filename"])
        sc = arcpy.da.SearchCursor(fc_las_attributes, ['filename', 'lasid'])
        for scrow in sc:
            x = scrow[0].split("_")[3]
            y = scrow[0].split("_")[4]
            pnt = arcpy.Point(x, y)
            ic.insertRow([pnt, scrow[1], scrow[0]])
            print(pnt, scrow[1], scrow[0], "inserted")
        editor.stopOperation()
        editor.stopEditing(True)
        del editor
        print("Centroids created")

    def writeGpsTimesOfLasesToFile(self):
        print("Writing GPS Times Of LASes To File")
        resfile = open(os.path.join(self.qcWorkPath, "GPStime_stats.csv"), 'w')
        resfile.write("LAS;MIN_DATETIME;MAX_DATETIME\n")
        for file in os.listdir(self.lasPath):
            if file.lower().endswith(".las"):
                print("... Processing LAS " + file) 
                subprocess.check_output(["lasinfo64", "-v", os.path.join(self.lasPath, file), "-gw", "-nh", "-nv", "-nr", "-o", os.path.join(self.qcWorkPath, "tempstats.txt")], shell=True).decode("utf-8")
                statsfile = open(os.path.join(self.qcWorkPath, "tempstats.txt"), 'r')
                lines = [line.split() for line in statsfile.readlines()]
                for line in lines:
                    if line[0] == "gps_week":
                        mingpsweek = line[1]
                        maxgpsweek = line[2]
                    if line[0] == "gps_secs_of_week":
                        mingpsseconds = line[1]
                        maxgpsseconds = line[2]
                    else:
                        raise Exception('neither gps_week or gps_secs_of_week exist in LAS statistics. Please check the way gps_time is written in LAS metadata.')
                datetimeformat = "%Y-%m-%d %H:%M:%S"
                epoch = datetime.datetime.strptime("1980-01-06 00:00:00",datetimeformat)
                minelapsed = datetime.timedelta(days=(int(mingpsweek)*7),seconds=(float(mingpsseconds)))
                mindatetime = datetime.datetime.strftime(epoch + minelapsed,datetimeformat)
                maxelapsed = datetime.timedelta(days=(int(maxgpsweek)*7),seconds=(float(maxgpsseconds)))
                maxdatetime = datetime.datetime.strftime(epoch + maxelapsed,datetimeformat)
                print("... Start and End ", mindatetime, maxdatetime)
                resfile.write(file + ";" + mindatetime + ";" + maxdatetime + "\n")
                statsfile.close()
        os.remove(os.path.join(self.qcWorkPath, 'tempstats.txt'))
        resfile.close()

    def sendEmail(self, sender, password, recipient, subject="default", message="default"):
        print("Sending E-mail to", recipient)
        if subject == "default":
            subject = "QC"
        if message == "default":
            message = "Quality Control algorithm done"
        mail = MIMEText(message)
        mail['Subject'] = subject
        mail['From'] = sender
        mail['To'] = recipient

        s = smtplib.SMTP()
        s.connect('mail.skgeodesy.local', 25)
        s.login(sender, password)
        s.sendmail(sender, recipient, str(mail))
        s.quit()

    def selectAndCopyLasFiles(self, selectedFootprintsFc, sourceLasPath, destinLasPath, coresCount = 2):
        print("Selecting and Copying LAS files based on footprints feature class from ", sourceLasPath, " to ", destinLasPath)
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        las_list = []
        sc = arcpy.da.SearchCursor(selectedFootprintsFc,['filename'])
        for scrow in sc:
            las_list.append(scrow[0])
        las_in_destin = os.listdir(destinLasPath)
        
        pid = 0
        i = 1
        jobs = []
        for las in las_list:
            if las in las_in_destin:
                print("... LAS ", las, "is already in destination folder", i, "/", len(las_list))
                i += 1
            else:
                # multiprocessing
                pid += 1 
                print("... pid is ", pid)
                proc = multiprocessing.Process(target=self.helper_selectAndCopyPointClouds_multiproc, args=(pid, las, len(las_list), sourceLasPath, destinLasPath, i))
                jobs.append(proc)
                proc.start()
                numofalive = 0
                for proc in jobs:
                    if proc.is_alive():
                        numofalive += 1
                print("... Number of alive processes is ", numofalive)
                while numofalive >= coresCount:
                    time.sleep(2)
                    numofalive = 0
                    for proc in jobs:
                        if proc.is_alive():
                            numofalive += 1
                i += 1
        for proc in jobs:
            proc.join()
        print("... Selected LAS files successfully copied")

    def selectAndCopyLazFiles(self, selectedFootprintsFc, sourceLazPath, destinLazPath, endian=''):
        print("Selecting and Copying LAZ files based on footprints feature class from ", sourceLazPath, " to ", destinLazPath)
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        laz_list = []
        sc = arcpy.da.SearchCursor(selectedFootprintsFc,['filename'])
        for scrow in sc:
            lazName = scrow[0].split('.las')[0] + endian + '.laz'
            laz_list.append(lazName)
        laz_in_destin = os.listdir(destinLazPath)

        pid = 0
        i = 1
        jobs = []
        for laz in laz_list:
            if laz in laz_in_destin:
                print("... LAZ ", laz, "is already in destination folder", i, "/", len(laz_list))
                i += 1
            else:
                # multiprocessing
                pid += 1 
                print("... pid is ", pid)
                proc = multiprocessing.Process(target=self.helper_selectAndCopyPointClouds_multiproc, args=(pid, laz, len(laz_list), sourceLazPath, destinLazPath, i))
                jobs.append(proc)
                proc.start()
                numofalive = 0
                for proc in jobs:
                    if proc.is_alive():
                        numofalive += 1
                print("... Number of alive processes is ", numofalive)
                while numofalive >= coresCount:
                    time.sleep(2)
                    numofalive = 0
                    for proc in jobs:
                        if proc.is_alive():
                            numofalive += 1
                i += 1
        for proc in jobs:
            proc.join()

        print("... Selected LAZ files successfully copied")

    def helper_selectAndCopyPointClouds_multiproc(self, pid, pointCloud, pointCloudsCnt, sourcePath, destinPath, i):
        print("... Copying ", pointCloud, i, "/", pointCloudsCnt)
        src = os.path.join(sourcePath, pointCloud)
        dst = os.path.join(destinPath, pointCloud)
        tryNum = 1
        while True:
            try:
                shutil.copyfile(src, dst)
                break
            except Exception as e:
                if tryNum == 5:
                    raise e
                tryNum += 1
                arcpy.AddMessage('Something went wrong during the copying. Trying again. Try number: ' + str(tryNum) + ' of 5.\nException raised: ' + str(e))
                time.sleep(5)   

    def adjustGpsTimeBasedOnWeek(self, selectedLasPath, outputLasPath, gpsWeek):
        print("Adjusting GPS Time of selected LAS files based on GPS week")
        las_list = [las for las in os.listdir(selectedLasPath) if las.lower().endswith(".las")]
        i = 1
        for las in las_list:
            print("... Processing ", las, i, "/", len(las_list))
            i += 1
            subprocess.run(["las2las64", "-i", os.path.join(selectedLasPath, las), "-week_to_adjusted", str(gpsWeek), "-o", os.path.join(outputLasPath, las), "-v"], shell=True)
        print("... GPS Time successfully adjusted")

    ######################################
    # Used to generalize bad density areas
    ######################################
    def generalizePolygons(self, lastretdensityFcPolyCategorySrc, bufferM):
        print("Generalizing polygons")
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        arcpy.env.overwriteOutput = True

        arcpy.Buffer_analysis(lastretdensityFcPolyCategorySrc, "temp_pos_" + str(bufferM), bufferM, 'FULL', 'ROUND', 'ALL')
        arcpy.Buffer_analysis("temp_pos_" + str(bufferM), "temp_neg_" + str(bufferM), bufferM * (-1), 'FULL', 'ROUND', 'ALL')
        arcpy.EliminatePolygonPart_management("temp_neg_" + str(bufferM), lastretdensityFcPolyCategorySrc + "_badArea", 'AREA', 999999, '', 'CONTAINED_ONLY')

    def countOverlappingLASFiles(self):
        print("Counting overlapping LAS files and creating feature class las_overlaps_count_" + self.lotNumStr)
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        print("... Union")
        arcpy.Union_analysis("las_attributes_prepro_" + self.lotNumStr, "temp_union", 'ONLY_FID', "", 'GAPS')
        print("... Delete identical")
        arcpy.DeleteIdentical_management("temp_union", "Shape")
        print("... Spatial join")
        arcpy.SpatialJoin_analysis("temp_union", "las_attributes_prepro_" + self.lotNumStr, "las_overlaps_prepro_count_" + self.lotNumStr, 'JOIN_ONE_TO_ONE', 'KEEP_ALL', "", 'WITHIN')
        print("... Removing unnecessary fields")
        fields = [field.name for field in arcpy.ListFields("las_overlaps_prepro_count_" + self.lotNumStr) if not field.required]
        fields.remove("Join_Count")
        if len(fields) > 0:
            arcpy.DeleteField_management("las_overlaps_prepro_count_" + self.lotNumStr, fields)
        arcpy.AlterField_management("las_overlaps_prepro_count_" + self.lotNumStr, "Join_Count", "LAS_count")
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_union')

    def prepareFlathouseVerticesFromLasAndZbgis(self, tileSize, coresCount, shouldClassifyBuildings = True, HGT = 9, BFC = [17], customControlAreaPolySrc = None, shouldProcessLasesAsBatch = True, returnProcessFromFid = 0):
        print("Preparing flathouse vertices from LAS and ZBGIS")
        arcpy.env.workspace = os.path.join(self.assetsPath, self.zbgisBuildingsDir, self.zbgisBuildingsGdb)
        if len(BFC):
            where = "BFC IN (" + str(BFC)[1:-1] + ") AND HGT > " + str(HGT)
        else:
            where = "HGT > " + str(HGT)
        selectedBuildingsZbgisLyr = arcpy.MakeFeatureLayer_management(self.zbgisBuildingsFcTm34, 'flathouses_zbgis_lyr', where)
        
        if customControlAreaPolySrc and arcpy.Exists(customControlAreaPolySrc):
            print('... Quality control will be processed on the custom area:', customControlAreaPolySrc)
            arcpy.MakeFeatureLayer_management(customControlAreaPolySrc, 'temp_customControlAreaLyr')
        else:
            print('... Quality control will be processed on the area of LOT:', self.lotNumStr)
            where = "lot_number = '" + self.lotNumStr + "'"
            arcpy.MakeFeatureLayer_management(os.path.join(self.assetsPath, self.lidarLotsDir, self.lidarLotsGdb, self.lidarLotsFcTm34), 'temp_customControlAreaLyr', where)
        
        arcpy.SelectLayerByLocation_management(selectedBuildingsZbgisLyr, 'INTERSECT', 'temp_customControlAreaLyr', '', 'SUBSET_SELECTION')
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        arcpy.env.overwriteOutput = True # todo remove

        # Getting phase number according to las file endian
        files = [las for las in os.listdir(self.lasPath) if las.endswith('.las')]
        for las in files:
            if las.endswith('_r.las'):
                phase = 1
                break
            elif las.endswith('_c.las'):
                phase = 2
                break

        # Clipping LAS according to flathouse surroundings
        print('... Clipping LAS according to flathouse surroundings')
        try:
            os.mkdir(os.path.join(self.qcWorkPath, "temp"))
            os.mkdir(os.path.join(self.qcWorkPath, "temp", "shp_buffer"))
            os.mkdir(os.path.join(self.qcWorkPath, "temp", "clipped_all"))
        except:
            pass
        
        if arcpy.Exists(os.path.join(self.qcWorkPath, 'temp', 'shp_buffer', 'temp_flathouses_buffer_multipart.shp')):
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', os.path.join(self.qcWorkPath, 'temp', 'shp_buffer'), 'temp_flathouses_buffer_multipart.shp')
        flathousesBufferSpSrc = os.path.join(self.qcWorkPath, 'temp', 'shp_buffer', 'temp_flathouses_buffer_singlepart.shp')
        if arcpy.Exists(flathousesBufferSpSrc):
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', os.path.join(self.qcWorkPath, 'temp', 'shp_buffer'), 'temp_flathouses_buffer_singlepart.shp')
        arcpy.Buffer_analysis(selectedBuildingsZbgisLyr, os.path.join(self.qcWorkPath, 'temp', 'shp_buffer', 'temp_flathouses_buffer_multipart.shp'), "100 Meters", 'FULL', 'ROUND', 'ALL',"", 'PLANAR')
        arcpy.MultipartToSinglepart_management(os.path.join(self.qcWorkPath, 'temp', 'shp_buffer', 'temp_flathouses_buffer_multipart.shp'), flathousesBufferSpSrc)

        if int(arcpy.GetCount_management(flathousesBufferSpSrc).getOutput(0)) == 0:
            raise Exception('There are no buildings matching the selected criteria in the control area.')

        where = 'FID >= ' + str(returnProcessFromFid)
        sc = arcpy.da.SearchCursor(flathousesBufferSpSrc, ['FID'], where)
        for row in sc:
            print('... Processing feature', row[0])
            if os.path.exists(os.path.join(self.qcWorkPath, "temp", "shp_buffer", str(row[0]))):
                shutil.rmtree(os.path.join(self.qcWorkPath, "temp", "shp_buffer", str(row[0])))
            os.mkdir(os.path.join(self.qcWorkPath, "temp", "shp_buffer", str(row[0])))
            where = 'FID = ' + str(row[0])
            arcpy.Select_analysis(flathousesBufferSpSrc, os.path.join(self.qcWorkPath, "temp", "shp_buffer", str(row[0]), str(row[0]) + '.shp'), where)
            self.clipLasFiles(os.path.join(self.qcWorkPath, "temp", "shp_buffer", str(row[0]), str(row[0]) + '.shp'), phase, coresCount)
            src = os.path.join(os.path.join(self.qcWorkPath, "temp", "clipped"))
            dst = os.path.join(os.path.join(self.qcWorkPath, "temp", "clipped_all"))
            lases = os.listdir(src)
            for las in lases:
                shutil.copy2(os.path.join(src, las), os.path.join(dst, las.split('.las')[0] + '_' + str(row[0]) + '.las'))
        del sc
        classifiedDir = os.path.join(self.qcWorkPath, "temp", "clipped_all")

        if shouldClassifyBuildings:    
            # Classification
            if shouldProcessLasesAsBatch:
                self.lasBatchClassify(os.path.join(self.qcWorkPath, "temp", "clipped_all"), tileSize, coresCount)
                classifiedDir = os.path.join(self.qcWorkPath, "classified_originals")
            else:
                self.lasIndividualClassify(os.path.join(self.qcWorkPath, "temp", "clipped_all"), tileSize, coresCount)
                classifiedDir = os.path.join(self.qcWorkPath, "las_individual_classified")
        
        # Building points (by query filter) extraction 
        print('... Building points (by query filter) extraction')
        arcpy.Buffer_analysis(selectedBuildingsZbgisLyr, os.path.join(self.qcWorkPath, 'temp', 'shp_buffer', 'temp_flathouses_smallBuffer_multipart.shp'), "10 Meters", 'FULL', 'ROUND', 'ALL',"", 'PLANAR')
        arcpy.MultipartToSinglepart_management(os.path.join(self.qcWorkPath, 'temp', 'shp_buffer', 'temp_flathouses_smallBuffer_multipart.shp'), os.path.join(self.qcWorkPath, 'temp', 'shp_buffer', 'temp_flathouses_smallBuffer_singlepart.shp'))
        bufferShpSrc = os.path.join(self.qcWorkPath, 'temp', 'shp_buffer', 'temp_flathouses_smallBuffer_singlepart.shp')
        self.extractBuildingPointsFromClassifiedLases(classifiedDir, bufferShpSrc, coresCount)

        arcpy.CopyFeatures_management(selectedBuildingsZbgisLyr, 'temp_selectedBuildingsZbgis')     
        if shouldProcessLasesAsBatch:
            print('... Finalizing processing of LAS files as a batch')
            os.mkdir(os.path.join(self.qcWorkPath, "temp","flathouses_merged"))
            subprocess.run(["lasmerge64", "-i", os.path.join(self.qcWorkPath, "temp", "building_points_extracted", "*.las"), "-o", os.path.join(self.qcWorkPath, "temp", "flathouses_merged", "flathouses.las"), "-v"], shell=True)

            if os.path.isfile(os.path.join(self.qcWorkPath, "temp", "flathouses_merged", "flathouses.las")):
                # create log file
                open(os.path.join(self.qcWorkPath, "extractBuildingPointsFromClassifiedLasesLog.txt"), 'w').close()
                self.extractBuildingPolygonsAndVerticesFromLas(os.path.join(self.qcWorkPath, "temp", "flathouses_merged", "flathouses.las"), 'temp_selectedBuildingsZbgis')
                self.copyFeatureClassesFromTempGdbsToMainGdb(os.path.join(self.qcWorkPath, 'temp', 'tempGdbs'))
            else:
                raise Exception('File flathouses.las does not exist')
        else:
            print('... Finalizing processing of LAS files individually')
            self.finalizeProcessingOfLasFilesIndividually('temp_selectedBuildingsZbgis', coresCount)
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', os.path.join(self.qcWorkPath, self.qcGdb), 'temp_selectedBuildingsZbgis')

    def finalizeProcessingOfLasFilesIndividually(self, selectedBuildingsZbgisFcName, coresCount):
        buildingsPntsExtractedPath = os.path.join(self.qcWorkPath, "temp", "building_points_extracted")
        flathousesLases = [fl for fl in os.listdir(buildingsPntsExtractedPath) if os.path.isfile(os.path.join(buildingsPntsExtractedPath, fl)) and fl.lower().endswith(".las")]
        
        # create log file
        open(os.path.join(self.qcWorkPath, "extractBuildingPolygonsAndVerticesFromLasLog.txt"), 'w').close()

        pid = 0
        jobs = []
        for las in flathousesLases:
            # multiprocessing
            pid += 1 
            print("... pid is ", pid)
            proc = multiprocessing.Process(target=self.extractBuildingPolygonsAndVerticesFromLas, args=(os.path.join(self.qcWorkPath, "temp", "building_points_extracted", las), selectedBuildingsZbgisFcName, ['-disjoint'], pid))
            jobs.append(proc)
            proc.start()
            numofalive = 0
            for proc in jobs:
                if proc.is_alive():
                    numofalive += 1
            print("... Number of alive processes is ", numofalive)
            while numofalive >= coresCount:
                time.sleep(2)
                numofalive = 0
                for proc in jobs:
                    if proc.is_alive():
                        numofalive += 1
        for proc in jobs:
            proc.join()
        
        self.copyFeatureClassesFromTempGdbsToMainGdb(os.path.join(self.qcWorkPath, 'temp', 'tempGdbs'))
    
    def copyFeatureClassesFromTempGdbsToMainGdb(self, tempGdbsPath):
        arcpy.env.workspace = tempGdbsPath
        tempGdbs = arcpy.ListWorkspaces("temp*", "FileGDB")
        for tempGdb in tempGdbs:
            arcpy.env.workspace = os.path.join(self.qcWorkPath, 'temp', 'tempGdbs', tempGdb)
            fcs = arcpy.ListFeatureClasses()
            for fc in fcs:
                print('... Copying ' + fc + ' to the main gdb')
                arcpy.Copy_management(fc, os.path.join(self.qcWorkPath, self.qcGdb, fc))

    def extractBuildingPointsFromClassifiedLases(self, classifiedLasesPath, bufferShpSrc = None, coresCount = 1):
        os.mkdir(os.path.join(self.qcWorkPath, "temp", "buildings_extracted"))
        os.mkdir(os.path.join(self.qcWorkPath, "temp", "building_points_extracted"))
        classifiedLases = os.listdir(classifiedLasesPath)
        
        # create log file
        open(os.path.join(self.qcWorkPath, "extractBuildingPointsFromClassifiedLasesLog.txt"), 'w').close()

        pid = 0
        jobs = []
        for las in classifiedLases:
            # multiprocessing
            pid += 1 
            print("... pid is ", pid)
            proc = multiprocessing.Process(target=self.helper_extractBuildingPointsFromClassifiedLases_multiproc, args=(pid, len(classifiedLases), classifiedLasesPath, bufferShpSrc, las))
            jobs.append(proc)
            proc.start()
            numofalive = 0
            for proc in jobs:
                if proc.is_alive():
                    numofalive += 1
            print("... Number of alive processes is ", numofalive)
            while numofalive >= coresCount:
                time.sleep(2)
                numofalive = 0
                for proc in jobs:
                    if proc.is_alive():
                        numofalive += 1
        for proc in jobs:
            proc.join()
        
    def helper_extractBuildingPointsFromClassifiedLases_multiproc(self, pid, lasCount, classifiedLasesPath, bufferShpSrc, las):
        try:
            print('... Processing LAS', las,'in PID', pid, '/', lasCount)
            subprocess.run(["las2las64", "-i", os.path.join(classifiedLasesPath, las), "-odir", os.path.join(self.qcWorkPath, "temp", "buildings_extracted"), "-keep_class", "6", "-v"], shell=True)
            if arcpy.Exists(bufferShpSrc):
                subprocess.run(["lasclip64", "-i", os.path.join(self.qcWorkPath, "temp", "buildings_extracted", las), "-poly", bufferShpSrc, '-odir', os.path.join(self.qcWorkPath, "temp", "building_points_extracted"), "-v"], shell=True)
        except Exception as e:
            print("... Error in process", pid, ':', str(e))
            logfile = open(os.path.join(self.qcWorkPath, "extractBuildingPointsFromClassifiedLasesLog.txt"), 'a')
            logfile.write('Error occured in the process with PID ' + str(pid) + '\n')
            traceback.print_exc(file = logfile)
            logfile.close()

    def extractBuildingPolygonsAndVerticesFromLas(self, buildingsLasSrc, selectedBuildingsZbgisFcName = None, additionalLasBoundaryOpts=[], pid=-1):
        try:
            print('Extracting flathouse polygons and vertices from LAS', os.path.basename(buildingsLasSrc))
            arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
            arcpy.env.overwriteOutput = True
            
            shpOutSrc = self.extractBuildingPolygonsFromLas(buildingsLasSrc, 'flathouses_boundary.shp')
            outPolygonsFcName = "ha_flathouses_las_polygons" if os.path.basename(buildingsLasSrc) == 'flathouses.las' else 'ha_flathouses_las_polygons_' + os.path.basename(buildingsLasSrc).split('.')[0]
            
            print('... Creating', outPolygonsFcName)
            tempGdbsPath = os.path.join(self.qcWorkPath, 'temp', 'tempGdbs')
            if not os.path.exists(tempGdbsPath):
                os.makedirs(tempGdbsPath)
            if os.path.exists(os.path.join(tempGdbsPath, 'temp_' + outPolygonsFcName + '.gdb')):
                arcpy.Delete_management(os.path.join(tempGdbsPath, 'temp_' + outPolygonsFcName + '.gdb'))
            arcpy.CreateFileGDB_management(tempGdbsPath, 'temp_' + outPolygonsFcName + '.gdb', 'CURRENT')
            
            arcpy.FeatureClassToFeatureClass_conversion(os.path.join(os.path.dirname(shpOutSrc), "flathouses_boundary.shp"), os.path.join(tempGdbsPath, 'temp_' + outPolygonsFcName + '.gdb'), outPolygonsFcName)
            tempFlathousesBoundaryLyrSrc = os.path.join(tempGdbsPath, 'temp_' + outPolygonsFcName + '.gdb', 'temp_flathouses_boundary_lyr')
            arcpy.MakeFeatureLayer_management(os.path.join(tempGdbsPath, 'temp_' + outPolygonsFcName + '.gdb', outPolygonsFcName), tempFlathousesBoundaryLyrSrc)
            if arcpy.Exists(selectedBuildingsZbgisFcName):
                arcpy.SelectLayerByLocation_management(tempFlathousesBoundaryLyrSrc, 'INTERSECT', selectedBuildingsZbgisFcName, "", 'NEW_SELECTION')
                
            # Vertices extraction
            outVerticesFcName = 'ha_flathouses_las_vertices' if os.path.basename(buildingsLasSrc) == 'flathouses.las' else 'ha_flathouses_las_vertices_' + os.path.basename(buildingsLasSrc).split('.')[0]
            arcpy.FeatureVerticesToPoints_management(tempFlathousesBoundaryLyrSrc, os.path.join(tempGdbsPath, 'temp_' +  outPolygonsFcName + '.gdb', outVerticesFcName), 'ALL')
            arcpy.AddXY_management(os.path.join(tempGdbsPath, 'temp_' + outPolygonsFcName + '.gdb', outVerticesFcName))
        except Exception as e:
            print("... Error in process", pid, ':', str(e))
            logfile = open(os.path.join(self.qcWorkPath, "extractBuildingPolygonsAndVerticesFromLasLog.txt"), 'a')
            logfile.write('Error occured in the process with PID ' + str(pid) + '\n')
            traceback.print_exc(file = logfile)
            logfile.close()

    def extractBuildingPolygonsFromLas(self, lasSrc, outShpName = 'flathouses_boundary.shp', additionalLasBoundaryOpts = [], concavity = 0.5):
        flathouseLasPath = os.path.dirname(lasSrc)            
        if not os.path.exists(flathouseLasPath):
            raise Exception('Path to input LAS file does not exist')
        
        shpOutPath = os.path.join(flathouseLasPath, 'las_buildings_' + os.path.basename(lasSrc))
        if os.path.exists(shpOutPath):
            shutil.rmtree(shpOutPath)
        os.mkdir(shpOutPath)
        print('... shpOutPath created:', shpOutPath)
        subprocess.run(["lasboundary64", "-i", lasSrc, "-o", os.path.join(shpOutPath, outShpName), "-keep_class", "6", "-concavity", str(concavity), "-v"] + additionalLasBoundaryOpts, shell=True)
        return os.path.join(shpOutPath, outShpName)

    def prepareFlathouseVerticesFromCadastreAndZbgis(self, HGT = 9, BFC = [17], customControlAreaPolySrc = None):
        print("Preparing flathouse vertices from cadastre and ZBGIS")
        self.selectRelevantCadastralAreasAndBuildings(HGT, BFC, customControlAreaPolySrc)
        arcpy.env.workspace = os.path.join(self.assetsPath, self.zbgisBuildingsDir, self.zbgisBuildingsGdb)
        if len(BFC):
            where = "BFC IN (" + str(BFC)[1:-1] + ") AND HGT > " + str(HGT)
        else:
            where = "HGT > " + str(HGT)
        arcpy.MakeFeatureLayer_management(self.zbgisBuildingsFcTm34, 'buildings_zbgis_lyr', where)

        if customControlAreaPolySrc and arcpy.Exists(customControlAreaPolySrc):
            print('... Process will be held on the custom area:', customControlAreaPolySrc)
            arcpy.MakeFeatureLayer_management(customControlAreaPolySrc, 'temp_customControlAreaLyr')
            arcpy.SelectLayerByLocation_management('buildings_zbgis_lyr', 'INTERSECT', 'temp_customControlAreaLyr', '', 'SUBSET_SELECTION')        
            haFlathousesCadastreVerticesQuality1FcName = 'ha_flathouses_cadastre_vertices_quality_1_custom'
            haFlathousesCadastrePolygonsFcName =  'ha_flathouses_cadastre_polygons_custom'
        else:
            print('... Process will be held on the area of LOT:', self.lotNumStr)
            haFlathousesCadastreVerticesQuality1FcName = 'ha_flathouses_cadastre_vertices_quality_1'
            haFlathousesCadastrePolygonsFcName =  'ha_flathouses_cadastre_polygons'
        
        relevantVgiList = self.listIntersectingVgiFileNames(os.path.join(self.qcWorkPath, self.qcGdb, 'ha_cadastral_areas_' + self.lotNumStr), 'WITHIN')
        tempLocalPath = self.batchConvertVgiToFileGdbAndTransformToEtrs89Tm34(relevantVgiList, os.path.join(self.qcWorkPath, 'temp'))
        print("... Preparing Cadastral Buildings From VGI alike GDB")
        rtsTempOutputs = os.path.dirname(tempLocalPath)
        gdbPaths = [os.path.join(rtsTempOutputs, gdbDir) for gdbDir in os.listdir(rtsTempOutputs) if os.path.isdir(os.path.join(rtsTempOutputs, gdbDir))]
        firstGdb = True
        for gdbPath in gdbPaths:
            gdbSrc = os.path.join(gdbPath, os.listdir(gdbPath)[0])
            print(gdbSrc)
            arcpy.env.workspace = gdbSrc
            arcpy.env.overwriteOutput = True
            where = "SYMBOL = 45" # symbol 45 represents noncombustible structures
            arcpy.MakeFeatureLayer_management('kladpar_druhypozemkov', 'kladpar_druhypozemkov_nespalna_stavba_lyr', where)
            where = "KVALITA = 1" # kvalita 1 represents very high measurement quality
            arcpy.MakeFeatureLayer_management('body_kn', 'body_kn_kvalita_1_lyr', where)
            arcpy.MakeFeatureLayer_management('kladpar_plochy', 'kladpar_plochy_lyr')
            arcpy.SelectLayerByLocation_management('kladpar_plochy_lyr', 'INTERSECT', 'kladpar_druhypozemkov_nespalna_stavba_lyr', "", 'NEW_SELECTION')
            arcpy.SelectLayerByLocation_management('kladpar_plochy_lyr', 'INTERSECT', 'body_kn_kvalita_1_lyr', "", 'SUBSET_SELECTION')
            arcpy.SelectLayerByLocation_management('kladpar_plochy_lyr', 'INTERSECT', 'buildings_zbgis_lyr', "", 'SUBSET_SELECTION')
            arcpy.SelectLayerByLocation_management('body_kn_kvalita_1_lyr', 'BOUNDARY_TOUCHES', 'kladpar_plochy_lyr', "", 'SUBSET_SELECTION')
            if firstGdb:
                arcpy.CopyFeatures_management('body_kn_kvalita_1_lyr', os.path.join(self.qcWorkPath, self.qcGdb, haFlathousesCadastreVerticesQuality1FcName))
                arcpy.CopyFeatures_management('kladpar_plochy_lyr', os.path.join(self.qcWorkPath, self.qcGdb, haFlathousesCadastrePolygonsFcName))
                firstGdb = False
            else:
                arcpy.Append_management('body_kn_kvalita_1_lyr', os.path.join(self.qcWorkPath, self.qcGdb, haFlathousesCadastreVerticesQuality1FcName))
                arcpy.Append_management('kladpar_plochy_lyr', os.path.join(self.qcWorkPath, self.qcGdb, haFlathousesCadastrePolygonsFcName))
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        
        # this is workaround for waiting to lock release
        while (True):
            try:
                arcpy.AddXY_management(haFlathousesCadastreVerticesQuality1FcName)
                break
            except:
                arcpy.AddMessage('Waiting for lock release')
                time.sleep(2)
        return haFlathousesCadastreVerticesQuality1FcName

    def lasBatchClassify(self, sourceLasPath, tileSize, coresCount):
        print("LAS Batch Classify")

        self.lasBatchTiling(sourceLasPath, r'temp\tiles', tileSize, coresCount, 10, shouldRemoveTempDir = False)

        print("... Noise Classification")
        os.mkdir(os.path.join(self.qcWorkPath, "temp", "classified"))
        os.mkdir(os.path.join(self.qcWorkPath, "temp", "classified", "1_noise"))
        subprocess.run(["lasnoise64", "-i", os.path.join(self.qcWorkPath, "temp", "tiles", "tile_*.las"), "-cores", str(coresCount), "-odir", os.path.join(self.qcWorkPath, "temp", "classified", "1_noise"), "-v"], shell=True)

        print("... Ground Classification")
        os.mkdir(os.path.join(self.qcWorkPath, "temp", "classified", "2_ground"))
        subprocess.run(["lasground_new64", "-i", os.path.join(self.qcWorkPath, "temp", "classified", "1_noise", "tile_*.las"), "-step", "25", "-ignore_class", "7", "-cores", str(coresCount), "-compute_height", "-odir", os.path.join(self.qcWorkPath, "temp", "classified", "2_ground"), "-v"], shell=True)

        print("... Buildings And High Vegetation Classification")
        os.mkdir(os.path.join(self.qcWorkPath, "temp", "classified", "3_buildings_highvegetetation"))
        subprocess.run(["lasclassify64", "-i", os.path.join(self.qcWorkPath, "temp", "classified", "2_ground", "tile_*.las"), "-ignore_class", "7 2", "-cores", str(coresCount), "-odir", os.path.join(self.qcWorkPath, "temp", "classified", "3_buildings_highvegetetation"), "-v"], shell=True)

        self.lasBatchRestoring(os.path.join("temp", "classified", "3_buildings_highvegetetation"), os.path.join(self.qcWorkPath, "classified_originals"), coresCount, 10, os.path.join(self.qcWorkPath, r'temp\tiles', 'pointSourceIdToLasNameMapping.csv'))

        print("LAS Batch Classify Done")

    def lasIndividualClassify(self, sourceLasPath, tileSize, coresCount, buffer = 10):
        print('LAS individual classify')
        lases = [files for files in os.listdir(sourceLasPath) if files.lower().endswith(".las")]
        print(lases)

        # create log file
        open(os.path.join(self.qcWorkPath, "lasIndividualClassifyLog.txt"), 'w').close()

        if not os.path.isdir(os.path.join(self.qcWorkPath, "temp")):
            os.mkdir(os.path.join(self.qcWorkPath, "temp"))
        outPath = os.path.join(self.qcWorkPath, "las_individual_classified")
        if os.path.exists(outPath):
            shutil.rmtree(outPath)
        os.mkdir(outPath)

        pid = 0
        jobs = []
        for las in lases:
            # multiprocessing
            pid += 1 
            print("... pid is ", pid)
            proc = multiprocessing.Process(target=self.helper_lasIndividualClassify_multiproc, args=(pid, las, len(lases), sourceLasPath, tileSize, coresCount, buffer, outPath))
            jobs.append(proc)
            proc.start()
            numofalive = 0
            for proc in jobs:
                if proc.is_alive():
                    numofalive += 1
            print("... Number of alive processes is ", numofalive)
            while numofalive >= coresCount:
                time.sleep(2)
                numofalive = 0
                for proc in jobs:
                    if proc.is_alive():
                        numofalive += 1
        for proc in jobs:
            proc.join()
    
        print('LAS individual classify completed')

    def helper_lasIndividualClassify_multiproc(self, pid, las, lasCount, sourceLasPath, tileSize, coresCount, buffer, outPath):
        try:    
            print('... Processing LAS', las,'in PID', pid, '/', lasCount)
            lasIndividualOutPath = os.path.join(self.qcWorkPath, "temp", las)
            if os.path.exists(lasIndividualOutPath):
                os.remove(lasIndividualOutPath)
            os.mkdir(lasIndividualOutPath)
            os.mkdir(os.path.join(lasIndividualOutPath, "tiles"))
            
            print("... Tiling in PID", pid)
            if buffer > 0:
                subprocess.run(["lastile64", "-i", os.path.join(sourceLasPath, las), "-cores", str(coresCount), "-tile_size", str(tileSize), "-buffer", str(buffer), "-reversible", "-odir", os.path.join(lasIndividualOutPath, 'tiles'), "-o", "tile.las", "-v"], shell=True)
            else:
                subprocess.run(["lastile64", "-i", os.path.join(sourceLasPath, las), "-cores", str(coresCount), "-tile_size", str(tileSize), "-odir", os.path.join(lasIndividualOutPath, 'tiles'), "-o", "tile.las", "-v"], shell=True)
            
            print("... Noise Classification in PID", pid)
            os.mkdir(os.path.join(lasIndividualOutPath, "classified"))
            os.mkdir(os.path.join(lasIndividualOutPath, "classified", "1_noise"))
            subprocess.run(["lasnoise64", "-i", os.path.join(lasIndividualOutPath, "tiles", "tile_*.las"), "-cores", str(coresCount), "-odir", os.path.join(lasIndividualOutPath, "classified", "1_noise"), "-v"], shell=True)

            print("... Ground Classification in PID", pid)
            os.mkdir(os.path.join(lasIndividualOutPath, "classified", "2_ground"))
            subprocess.run(["lasground_new64", "-i", os.path.join(lasIndividualOutPath, "classified", "1_noise", "tile_*.las"), "-step", "25", "-ignore_class", "7", "-cores", str(coresCount), "-compute_height", "-odir", os.path.join(lasIndividualOutPath, "classified", "2_ground"), "-v"], shell=True)

            print("... Buildings And High Vegetation Classification in PID", pid)
            os.mkdir(os.path.join(lasIndividualOutPath, "classified", "3_buildings_highvegetetation"))
            subprocess.run(["lasclassify64", "-i", os.path.join(lasIndividualOutPath, "classified", "2_ground", "tile_*.las"), "-ignore_class", "7 2", "-cores", str(coresCount), "-odir", os.path.join(lasIndividualOutPath, "classified", "3_buildings_highvegetetation"), "-v"], shell=True)
        
            inputPath = os.path.join(lasIndividualOutPath, "classified", "3_buildings_highvegetetation")
            if buffer > 0:
                print("... Removing Buffer in PID", pid)
                os.mkdir(os.path.join(lasIndividualOutPath, "tiles_buffer_removed"))
                outputPath = os.path.join(lasIndividualOutPath, "tiles_buffer_removed")
                subprocess.run(["lastile64", "-i", os.path.join(inputPath, "*.las"), "-remove_buffer", "-cores", str(coresCount), "-odir", outputPath, "-v"], shell=True)
                inputPath = outputPath
            
            print('... Merging individual tiles in PID', pid)
            subprocess.run(["lasmerge64", "-i", os.path.join(inputPath, "*.las"), "-o", os.path.join(outPath, las), "-v"], shell=True)

            shutil.rmtree(lasIndividualOutPath)  
            print('... LAS successfully classified in PID', pid)
        except Exception as e:
            print("... Error in process", pid, ':', str(e))
            logfile = open(os.path.join(self.qcWorkPath, "lasIndividualClassifyLog.txt"), 'a')
            logfile.write('Error occured in the process with PID ' + str(pid) + '\n')
            traceback.print_exc(file = logfile)
            logfile.close()

    def selectRelevantCadastralAreasAndBuildings(self, HGT = 9, BFC = [17], customControlAreaPolySrc = None):
        print('Selecting relevant cadastral areas and buildings')
        
        # Selecting cadastral areas
        print('... Selecting cadastral areas')
        
        if customControlAreaPolySrc and arcpy.Exists(customControlAreaPolySrc):
            print('... Process will be held on the custom area:', customControlAreaPolySrc)
            arcpy.MakeFeatureLayer_management(customControlAreaPolySrc, 'temp_customControlAreaLyr')
            haFlathousesFcName = 'ha_flathouses_' + self.lotNumStr + '_custom'
        else:
            print('... Process will be held on the area of LOT:', self.lotNumStr)
            where = "lot_number = '" + self.lotNumStr + "'"
            arcpy.MakeFeatureLayer_management(os.path.join(self.assetsPath, self.lidarLotsDir, self.lidarLotsGdb, self.lidarLotsFcTm34), 'temp_customControlAreaLyr', where)
            haFlathousesFcName = 'ha_flathouses_' + self.lotNumStr
        
        arcpy.MakeFeatureLayer_management(os.path.join(self.assetsPath, self.cadastralAreasDir, self.cadastralAreasGdb, self.cadastralAreasFcTm34), 'cadastral_areas_lyr')
        arcpy.SelectLayerByLocation_management('cadastral_areas_lyr', 'INTERSECT', 'temp_customControlAreaLyr', '', 'NEW_SELECTION')    
        IDN5List = []
        sc = arcpy.da.SearchCursor('cadastral_areas_lyr', ['IDN5'])
        for row in sc:
            IDN5List.append(row[0])
        vgiList = [vgi for vgi in os.listdir(os.path.join(self.assetsPath, self.vgiDir)) if vgi.lower().endswith('.vgi')]
        IDN5VgiList = [int(vgi[vgi.lower().find('kn') + 2 : vgi.find('_')]) for vgi in vgiList if (vgi[vgi.find('_') + 1] == '1' or vgi[vgi.find('.vgi') - 1] == 'I')] # This controls if quality of VGI is 1 (1) or contains points with quality 1 (I) and extracts IDN5 from vgi name
        relevantIDN5List = [IDN5 for IDN5 in IDN5List if IDN5 in IDN5VgiList]
        where = "IDN5 IN (" + str([int(item) for item in relevantIDN5List])[1:-1] + ")"
        print(where)
        if (len(relevantIDN5List)):
            arcpy.SelectLayerByAttribute_management('cadastral_areas_lyr', 'SUBSET_SELECTION', where)
        else:
            raise RuntimeError('There are no vgi files intersecting lot number ' + self.lotNumStr)

        # Selecting flathouses
        print('... Selecting flathouses')
        arcpy.env.workspace = os.path.join(self.assetsPath, self.zbgisBuildingsDir, self.zbgisBuildingsGdb)
        if len(BFC):
            where = "BFC IN (" + str(BFC)[1:-1] + ") AND HGT > " + str(HGT)
        else:
            where = "HGT > " + str(HGT)
        arcpy.MakeFeatureLayer_management(self.zbgisBuildingsFcTm34, 'flathouses_zbgis_lyr', where)
        arcpy.SelectLayerByLocation_management('flathouses_zbgis_lyr', 'INTERSECT', 'temp_customControlAreaLyr', '', 'SUBSET_SELECTION')        

        # Selecting and copying relevant buildings and cadastral areas
        print('... Selecting and copying relevants')
        arcpy.SelectLayerByLocation_management('flathouses_zbgis_lyr', 'INTERSECT', 'cadastral_areas_lyr', '', 'SUBSET_SELECTION')        
        arcpy.SelectLayerByLocation_management('cadastral_areas_lyr', 'INTERSECT', 'flathouses_zbgis_lyr', '', 'SUBSET_SELECTION')        
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        arcpy.env.overwriteOutput = True
        arcpy.CopyFeatures_management('flathouses_zbgis_lyr', haFlathousesFcName)
        arcpy.CopyFeatures_management('cadastral_areas_lyr', 'ha_cadastral_areas_' + self.lotNumStr)

        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'flathouses_zbgis_lyr')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'cadastral_areas_lyr')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_customControlAreaLyr')
        print('Relevant cadastral areas and buildings selected')

    def lasinfo(self, lasSrc):
        subprocess.run(['lasinfo64', '-i', lasSrc, '-compute_density', '-o', os.path.join(self.qcWorkPath, 'temp.txt')])
        lasinfo = open(os.path.join(self.qcWorkPath, 'temp.txt'), 'r')
        textLines = lasinfo.readlines()
        stats = {}
        wasClassificationLeadingRow = False
        classificationList = []
        for line in textLines:
            header = line.split(':')
            other = line.split()
            if len(header) == 2:
                stats[header[0].strip()] = header[1].strip()
            if len(other) == 3:
                stats[other[0]] = [other[1], other[2]]
            if line.find('COORDINATE SYSTEM') is not -1:
                stats['crs'] = textLines[textLines.index(line) + 1].strip()
            if wasClassificationLeadingRow: 
                classificationList.append({'class': other[-1][1:-1], 'numberOfPoints': other[0]})
            if line.find('histogram of classification of points') is not -1:
                wasClassificationLeadingRow = True
        stats['classificationStats'] = classificationList
        lasinfo.close()
        os.remove(os.path.join(self.qcWorkPath, 'temp.txt'))
        return stats

    def renameTransformedLasFiles(self, sourceLasPath, oldSuffix = 'bpv_jtsk03.las', newSuffix = 'jtsk03_bpv.las'):
        lases = [las for las in os.listdir(sourceLasPath)]
        for las in lases:
            if las.endswith(oldSuffix):
                baseName = las.split(oldSuffix)[0]
                os.rename(os.path.join(sourceLasPath, las), os.path.join(sourceLasPath, baseName + newSuffix))    

    def lasFromEtrs89hToBpv(self, clippedLasesPath, outBasePath='default'):
        print('... LAS height transformation')
        arcpy.AddMessage('... LAS height transformation')
        quasigeoidSrc = os.path.join(self.assetsPath, self.transformationsDir, self.quasigeoidEtrs89hToBpv)
        outPath = os.path.join(self.qcWorkPath, 'temp', 'clipped_bpv') if outBasePath == 'default' else os.path.join(outBasePath, 'clipped_bpv')
        if outBasePath == 'default' and not os.path.exists(os.path.join(self.qcWorkPath, 'temp')):
            os.mkdir(os.path.join(self.qcWorkPath, 'temp'))
        if os.path.exists(outPath):
            shutil.rmtree(outPath)
        os.mkdir(outPath)    
        subprocess.run(['lasheight64', '-i', os.path.join(clippedLasesPath, '*.las'), '-ground_points', quasigeoidSrc, '-replace_z', '-odir', outPath, '-odix', '_bpv'], shell=True)
        arcpy.AddMessage('outPath at the end of las height transformation is ' + outPath)
        return outPath

    def lasPositionalTransformationBaseHandler(self, lasPath, orderName, coresCount, transformMethod, outBasePath, isArcTool):
        files = os.listdir(lasPath)
        lases = [fl for fl in files if fl.endswith('.las')]
        outPath = os.path.join(self.qcWorkPath, orderName) if outBasePath == 'default' else os.path.join(outBasePath, orderName)
        lasesCount = len(lases)
        if lasesCount:
            if os.path.exists(outPath):
                shutil.rmtree(outPath)
            os.mkdir(outPath)
        else:
            raise Exception('There are no lases to transform in', lasPath)
        if isArcTool:
            multiprocessing.set_executable(os.path.join(sys.exec_prefix, 'pythonw.exe')) # process in the new thread won't open the ArcGIS Pro, but pythonw.exe
        
        pid = 0
        jobs = []
        for bpvLas in lases:
            # multiprocessing
            pid += 1 
            print("... pid is ", pid)
            arcpy.AddMessage("... pid is " + str(pid))
            proc = multiprocessing.Process(target=getattr(self, transformMethod), args=(pid, lasPath, bpvLas, orderName, lasesCount, outPath))
            jobs.append(proc)
            proc.start()
            numofalive = 0
            for proc in jobs:
                if proc.is_alive():
                    numofalive += 1
            print("... Number of alive processes is ", numofalive)
            while numofalive >= int(coresCount):
                time.sleep(2)
                numofalive = 0
                for proc in jobs:
                    if proc.is_alive():
                        numofalive += 1
        for proc in jobs:
            proc.join()
        arcpy.AddMessage('outPath at the end of las transformation is ' + outPath)
        return outPath

    def lasFromEtrs89Tm34ToJtsk03(self, bpvLasesPath, orderName, coresCount, outBasePath='default', isArcTool=False):
        print('... LAS positional transformation from ETRS89 TM34 to S-JTSK (JTSK03)')
        arcpy.AddMessage('... LAS positional transformation from ETRS89 TM34 to S-JTSK (JTSK03)')
        outPath = self.lasPositionalTransformationBaseHandler(bpvLasesPath, orderName, coresCount, 'lasFromEtrs89Tm34ToJtsk03_multiproc', outBasePath, isArcTool)
        self.renameTransformedLasFiles(outPath, 'bpv_jtsk03.las', 'jtsk03_bpv.las')
        return outPath

    def lasFromEtrs89Tm34ToJtsk03_multiproc(self, pid, bpvLasesPath, bpvLas, orderName, bpvLasesCount, outPath):    
        print("... Processing LAS: " + bpvLas + " in process " + str(pid), "of", str(bpvLasesCount))
        self.inCheck.isNewPdalInstalled()
        aSrs = 'PROJCS["S-JTSK_[JTSK03]_Krovak_East_North",GEOGCS["S-JTSK_[JTSK03]",DATUM["S-JTSK_[JTSK03]",SPHEROID["Bessel_1841",6377397.155,299.1528128]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]],PROJECTION["Krovak"],PARAMETER["False_Easting",0.0],PARAMETER["False_Northing",0.0],PARAMETER["Pseudo_Standard_Parallel_1",78.5],PARAMETER["Scale_Factor",0.9999],PARAMETER["Azimuth",30.28813975277778],PARAMETER["Longitude_Of_Center",24.83333333333333],PARAMETER["Latitude_Of_Center",49.5],PARAMETER["X_Scale",-1.0],PARAMETER["Y_Scale",1.0],PARAMETER["XY_Plane_Rotation",90.0],UNIT["Meter",1.0],AUTHORITY["EPSG",8353]]'.replace('"', '\\"')
        bpvLasSrc = os.path.join(bpvLasesPath, bpvLas).replace('\\', '/')
        print('bpvLasSrc', bpvLasSrc)
        jtsk03LasName = bpvLas if bpvLas.find('_jtsk03') > -1 else bpvLas.split('.las')[0] + '_jtsk03.las'
        jtsk03LasOut = os.path.join(outPath, jtsk03LasName).replace('\\', '/')
        print('jtsk03LasOut', jtsk03LasOut)
        pipeline = (
            '{'
                '"pipeline":['
                    '"' + bpvLasSrc + '",'
                    '{'
                        '"type":"filters.ferry",'
                        '"dimensions":"Z => NormalZ"'
                    '},'
                    '{'
                        '"type":"filters.reprojection",'
                        '"in_srs":"EPSG:3046",'
                        '"out_srs":"+proj=krovak +lat_0=49.5 +lon_0=24.83333333333333 +alpha=30.28813972222222 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=485.021,169.465,483.839,7.786342,4.397554,4.102655,0 +units=m +no_defs"'
                    '},'
                    '{'
                        '"type":"filters.ferry",'
                        '"dimensions":"NormalZ => Z"'
                    '},'
                    '{'
                        '"type":"writers.las",'
                        '"a_srs":"' + aSrs + '",'
                        '"minor_version":"4",'
                        '"dataformat_id": "6",'
                        '"filename":"' + jtsk03LasOut + '"'
                    '}'
                ']'
            '}'
        )
        pipelineFile = codecs.open(os.path.join(bpvLasesPath, 'temp_pipeline_pid' + str(pid) +'.json'), 'w', 'utf-8')
        pipelineFile.write(pipeline)
        pipelineFile.close()
        subprocess.run(['type', os.path.join(os.path.abspath(bpvLasesPath), 'temp_pipeline_pid' + str(pid) + '.json'), '|', 'pdal', 'pipeline', '-s'], shell=True)
        print('temp pipeline', os.path.join(bpvLasesPath, 'temp_pipeline_pid' + str(pid) + '.json'))
        subprocess.run(['lasinfo', '-i', jtsk03LasOut, '-repair'], shell=True)
        os.remove(os.path.join(bpvLasesPath, 'temp_pipeline_pid' + str(pid) + '.json'))

    def lasFromJtsk03ToJtsk(self, jtsk03LasesPath, orderName, coresCount, outBasePath='default', isArcTool=False):
        print('... LAS positional transformation from JTSK03 datum to JTSK datum')
        arcpy.AddMessage('... LAS positional transformation from JTSK03 datum to JTSK datum')
        return self.lasPositionalTransformationBaseHandler(jtsk03LasesPath, orderName, coresCount, 'lasFromJtsk03ToJtsk_multiproc', outBasePath, isArcTool)
    
    def lasFromJtsk03ToJtsk_multiproc(self, pid, jtsk03LasesPath, jtsk03Las, orderName, jtsk03LasesCount, outPath):    
        print("... Processing LAS: " + jtsk03Las + " in process " + str(pid), "of", str(jtsk03LasesCount))
        self.inCheck.isPdal1_7Installed()
        aSrs = 'PROJCS["S-JTSK_Krovak_East_North",GEOGCS["GCS_S_JTSK",DATUM["D_S_JTSK",SPHEROID["Bessel_1841",6377397.155,299.1528128,AUTHORITY["EPSG",7004]],AUTHORITY["EPSG",6156]],PRIMEM["Greenwich",0.0,AUTHORITY["EPSG",8901]],UNIT["Degree",0.0174532925199433,AUTHORITY["EPSG",9102]],AUTHORITY["EPSG",4156]],PROJECTION["Krovak",AUTHORITY["Esri",43039]],PARAMETER["False_Easting",0.0,AUTHORITY["Esri",100001]],PARAMETER["False_Northing",0.0,AUTHORITY["Esri",100002]],PARAMETER["Pseudo_Standard_Parallel_1",78.5,AUTHORITY["Esri",100027]],PARAMETER["Scale_Factor",0.9999,AUTHORITY["Esri",100003]],PARAMETER["Azimuth",30.28813975277778,AUTHORITY["Esri",100004]],PARAMETER["Longitude_Of_Center",24.83333333333333,AUTHORITY["Esri",100012]],PARAMETER["Latitude_Of_Center",49.5,AUTHORITY["Esri",100022]],PARAMETER["X_Scale",-1.0,AUTHORITY["Esri",100037]],PARAMETER["Y_Scale",1.0,AUTHORITY["Esri",100038]],PARAMETER["XY_Plane_Rotation",90.0,AUTHORITY["Esri",100039]],UNIT["Meter",1.0,AUTHORITY["EPSG",9001]],AUTHORITY["EPSG",5514]],VERTCS["Baltic_1957_height",VDATUM["Baltic_1957",AUTHORITY["EPSG",1202]],PARAMETER["Vertical_Shift",0.0,AUTHORITY["Esri",100006]],PARAMETER["Direction",1.0,AUTHORITY["Esri",100007]],UNIT["Meter",1.0,AUTHORITY["EPSG",9001]],AUTHORITY["EPSG",8357]]'.replace('"', '\\"')
        jtsk03LasSrc = os.path.join(jtsk03LasesPath, jtsk03Las).replace('\\', '/')
        jtskLasName = jtsk03Las.replace('_jtsk03_bpv', '_jtsk_bpv') if jtsk03Las.find('_jtsk03_bpv') > -1 else jtsk03Las.split('.las')[0] + '_jtsk_bpv.las'
        jtskLasOut = os.path.join(outPath, jtskLasName).replace('\\', '/')
        jtsk03ToJtskShiftGridSrc = os.path.join(self.assetsPath, self.shiftGridDir, self.jtsk03ToJtskShiftGridFile).replace('\\', '/')
        pipeline = (
            '{'
                '"pipeline":['
                    '"' + jtsk03LasSrc + '",'
                    '{'
                        '"type":"filters.reprojection",'
                        '"in_srs": "+proj=krovak +lat_0=49.5 +lon_0=24.83333333333333 +alpha=30.28813972222222 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +pm=greenwich +units=m +no_defs",'
                        '"out_srs": "+proj=latlong +ellps=bessel +no_defs"'
                    '},'
                    '{'
                        '"type":"filters.reprojection",'
                        '"in_srs": "+proj=latlong +datum=NAD83",'
                        '"out_srs": "+proj=latlong +nadgrids=' + jtsk03ToJtskShiftGridSrc + ' +ellps=bessel"'
                    '},'
                    '{'
                        '"type":"filters.reprojection",'
                        '"in_srs": "+proj=latlong +nadgrids=' + jtsk03ToJtskShiftGridSrc + ' +ellps=bessel",'
                        '"out_srs": "+proj=krovak +lat_0=49.5 +lon_0=24.83333333333333 +alpha=30.28813972222222 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +pm=greenwich +units=m"'
                    '},'
                    '{'
                        '"type":"writers.las",'
                        '"a_srs":"' + aSrs + '",'
                        '"minor_version":"4",'
                        '"dataformat_id": "6",'
                        '"filename":"' + jtskLasOut + '"'
                    '}'
                ']'
            '}'
        )
        pipelineFile = codecs.open(os.path.join(jtsk03LasesPath, 'temp_pipeline_pid' + str(pid) +'.json'), 'w', 'utf-8')
        pipelineFile.write(pipeline)
        pipelineFile.close()
        subprocess.run(['type', os.path.join(os.path.abspath(jtsk03LasesPath), 'temp_pipeline_pid' + str(pid) + '.json'), '|', con.PDAL_v1_7_SRC, 'pipeline', '-s'], shell=True)
        subprocess.run(['lasinfo', '-i', jtskLasOut, '-repair'], shell=True)
        os.remove(os.path.join(jtsk03LasesPath, 'temp_pipeline_pid' + str(pid) + '.json'))

    def transformFcFromJtsk03ToTm34(self, fcSrc, outPath):
        print('Transformation of feature class from S-JTSK (JTSK03) to ETRS89-TM34')
        Rts = rtsComponent.RTS()
        if not os.path.exists(os.path.join(self.qcWorkPath, 'temp')):
            os.mkdir(os.path.join(self.qcWorkPath, 'temp'))
        if os.path.exists(os.path.join(self.qcWorkPath, 'temp', 'toRts')):
            shutil.rmtree(os.path.join(self.qcWorkPath, 'temp', 'toRts'))
        os.mkdir(os.path.join(self.qcWorkPath, 'temp', 'toRts'))
        desc = arcpy.Describe(fcSrc)
        if desc.dataElementType == 'DEShapeFile':
            fcName = desc.baseName.lower()
            ext = '.shp'
        elif desc.dataElementType == 'DEFeatureClass':
            fcName = desc.baseName.lower()
            ext = ''
        else:
            raise TypeError('Please, provide only feature class or shapefile to transform')
        arcpy.CreateFileGDB_management(os.path.join(self.qcWorkPath, 'temp', 'toRts'), fcName, 'CURRENT')
        arcpy.CopyFeatures_management(fcSrc, os.path.join(self.qcWorkPath, 'temp', 'toRts', fcName + '.gdb', fcName))
        tempRemoteDir = Rts.uploadToRts(os.path.join(self.qcWorkPath, 'temp', 'toRts'), fcName + '.gdb')
        outLocalPath = Rts.transformGdb('S-JTSK (JTSK03)', 'ETRS89-TM34', tempRemoteDir, os.path.join(self.qcWorkPath, 'temp'))
        if os.path.exists(outPath):
            shutil.rmtree(outPath)
        os.mkdir(outPath)
        if ext == '.shp':
            if arcpy.Exists(os.path.join(outPath, fcName + '.shp')):
                self.waitForLockReleaseAndProcess('arcpy.Delete_management', outPath, fcName + '.shp')
            outSrc = os.path.join(outPath, fcName + '.shp')
            arcpy.CopyFeatures_management(os.path.join(outLocalPath, fcName + '.gdb', fcName), outSrc)
        else:
            if os.path.exists(os.path.join(outPath, fcName + '.gdb')): # fcName is in this case gdbName
                arcpy.Delete_management(os.path.join(outPath, fcName + '.gdb'))
            arcpy.CreateFileGDB_management(outPath, fcName, 'CURRENT')
            outSrc = os.path.join(outPath, fcName + '.gdb', fcName)
            arcpy.CopyFeatures_management(os.path.join(outLocalPath, fcName + '.gdb', fcName), outSrc)
        
        # this is workaround for waiting to lock release
        while (True):
            try:
                arcpy.RepairGeometry_management(outSrc, 'DELETE_NULL', 'OGC')
                break
            except:
                arcpy.AddMessage('Waiting for lock release')
                time.sleep(2)

        shutil.rmtree(outLocalPath)
        shutil.rmtree(os.path.join(self.qcWorkPath, 'temp'))
        print('Transformation successfully done. Output generated on', outPath)
        return outSrc

    def transformFcFromTm34ToJtsk03(self, fcSrc, outPath):
        print('Transformation of feature class from ETRS89-TM34 to S-JTSK (JTSK03)')
        Rts = rtsComponent.RTS()
        if not os.path.exists(os.path.join(self.qcWorkPath, 'temp')):
            os.mkdir(os.path.join(self.qcWorkPath, 'temp'))
        if os.path.exists(os.path.join(self.qcWorkPath, 'temp', 'toRts')):
            shutil.rmtree(os.path.join(self.qcWorkPath, 'temp', 'toRts'))
        os.mkdir(os.path.join(self.qcWorkPath, 'temp', 'toRts'))
        desc = arcpy.Describe(fcSrc)
        if desc.dataType == 'ShapeFile':
            fcName = desc.name.lower().split('.shp')[0]
            ext = '.shp'
        elif desc.dataType == 'FeatureClass':
            fcName = desc.name.lower()
            ext = ''
        else:
            raise TypeError('Please, provide only feature class or shapefile to transform')
        arcpy.CreateFileGDB_management(os.path.join(self.qcWorkPath, 'temp', 'toRts'), fcName, 'CURRENT')
        arcpy.CopyFeatures_management(fcSrc, os.path.join(self.qcWorkPath, 'temp', 'toRts', fcName + '.gdb', fcName))
        tempRemoteDir = Rts.uploadToRts(os.path.join(self.qcWorkPath, 'temp', 'toRts'), fcName + '.gdb')
        outLocalPath = Rts.transformGdb('ETRS89-TM34', 'S-JTSK (JTSK03)', tempRemoteDir, os.path.join(self.qcWorkPath, 'temp'))
        if os.path.exists(outPath):
            shutil.rmtree(outPath)
        os.mkdir(outPath)
        if ext == '.shp':
            if arcpy.Exists(os.path.join(outPath, fcName + '.shp')):
                self.waitForLockReleaseAndProcess('arcpy.Delete_management', outPath, fcName + '.shp')
            outSrc = os.path.join(outPath, fcName + '.shp')
            arcpy.CopyFeatures_management(os.path.join(outLocalPath, fcName + '.gdb', fcName), outSrc)
        else:
            if os.path.exists(os.path.join(outPath, fcName + '.gdb')): # fcName is in this case gdbName
                arcpy.Delete_management(os.path.join(outPath, fcName + '.gdb'))
            shutil.copytree(os.path.join(outLocalPath, fcName + '.gdb'), os.path.join(outPath, fcName + '.gdb'))
            outSrc = os.path.join(outPath, fcName + '.gdb', fcName)
        
        # this is workaround for waiting to lock release
        while (True):
            try:
                arcpy.RepairGeometry_management(outSrc, 'DELETE_NULL', 'OGC')
                break
            except:
                arcpy.AddMessage('Waiting for lock release')
                time.sleep(2)
            
        shutil.rmtree(outLocalPath)
        shutil.rmtree(os.path.join(self.qcWorkPath, 'temp'))
        print('Transformation successfully done. Output generated on', outPath)
        return outSrc

    def waitForLockRelease(self, fileSrc):
        print('Waiting for lock release')
        arcpy.AddMessage('Waiting for lock release')
        basePath = os.path.split(fileSrc)[0]
        locked = True
        while (locked):
            time.sleep(20)
            locked = not arcpy.TestSchemaLock(fileSrc)
            arcpy.AddMessage('test schema lock result is ' + str(locked))

    def getLasNames(self):
        print('Getting LAS names as list')
        lases = [os.path.split(las)[1] for las in os.listdir(os.path.join(self.lasPath)) if las.lower().endswith('.las')]
        print(lases)
        return lases

    def clipLasFiles(self, clipPolygonSrc, phase, coresCount, orderWorkPath='default'):
        print('Clipping LAS files')
        arcpy.AddMessage('Clipping LAS files')
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        if orderWorkPath == 'default':
            orderWorkPath = self.qcWorkPath
        if not os.path.exists(os.path.join(orderWorkPath, 'temp')):
            os.mkdir(os.path.join(orderWorkPath, 'temp'))
        if os.path.exists(os.path.join(orderWorkPath, 'temp', 'clipped')):
            shutil.rmtree(os.path.join(orderWorkPath, 'temp', 'clipped'))
        os.mkdir(os.path.join(orderWorkPath, 'temp', 'clipped'))
        selection = False
        if phase == 1 and arcpy.Exists('las_attributes_prepro_' + self.lotNumStr):
            arcpy.MakeFeatureLayer_management('las_attributes_prepro_' + self.lotNumStr, 'footprints_lyr')
            selection = True
        elif phase == 2 and arcpy.Exists('las_attributes_postpro_' + self.lotNumStr):
            arcpy.MakeFeatureLayer_management('las_attributes_postpro_' + self.lotNumStr, 'footprints_lyr')
            selection = True
        else:
            lasclipInput = self.lasPath
        if selection:
            arcpy.SelectLayerByLocation_management('footprints_lyr', 'INTERSECT', clipPolygonSrc, '', 'NEW_SELECTION')
            arcpy.CopyFeatures_management('footprints_lyr', 'temp_selected_footprints')
            tempLasDestination = os.path.join(orderWorkPath, 'temp', 'las_selection')
            if not os.path.exists(tempLasDestination):
                os.mkdir(tempLasDestination) # If there already is tempLasDestination folder, LAS files will be added to existing files
            else:
                print('... tempLasDestination folder already exists')
                existingLases = os.listdir(tempLasDestination)
                for existingLas in existingLases:
                    lastModifiedDate = datetime.datetime.fromtimestamp(os.path.getmtime(os.path.join(tempLasDestination, existingLas)))
                    if lastModifiedDate < (datetime.datetime.now() - datetime.timedelta(hours = 3)): # experimental number of hours
                        print('... Removing old LAS file', existingLas)
                        os.remove(os.path.join(tempLasDestination, existingLas))
            self.selectAndCopyLasFiles('temp_selected_footprints', self.lasPath, tempLasDestination, coresCount)
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_selected_footprints')
            lasclipInput = tempLasDestination
        arcpy.AddMessage('lasclipInput is ' + lasclipInput)

        desc = arcpy.Describe(clipPolygonSrc)
        if desc.dataType == 'ShapeFile':
            pass
        elif desc.dataType == 'FeatureClass':
            if os.path.exists(os.path.join(orderWorkPath, 'temp', 'shp_jtsk03')):
                shutil.rmtree(os.path.join(orderWorkPath, 'temp', 'shp_jtsk03'))    
            os.mkdir(os.path.join(orderWorkPath, 'temp', 'shp_jtsk03'))
            arcpy.CopyFeatures_management(clipPolygonSrc, os.path.join(orderWorkPath, 'temp', 'shp_jtsk03', desc.name + '.shp'))
            clipPolygonSrc = os.path.join(orderWorkPath, 'temp', 'shp_jtsk03', desc.name + '.shp')            
            # this is workaround for waiting to lock release
            while (True):
                try:
                    arcpy.RepairGeometry_management(clipPolygonSrc, 'DELETE_NULL', 'OGC')
                    arcpy.RepairGeometry_management(clipPolygonSrc, 'DELETE_NULL')
                    break
                except:
                    arcpy.AddMessage('Waiting for lock release')
                    time.sleep(2)
        else:
            raise TypeError('Please, provide only feature class or shapefile as clip polygon')

        subprocess.run(["lasclip64", "-i", os.path.join(lasclipInput, "*.las"), "-cores", str(coresCount), "-poly", os.path.abspath(clipPolygonSrc), '-odir', os.path.join(orderWorkPath, "temp", "clipped"), "-v"], shell=True)
        print('LAS files clipped')
        arcpy.AddMessage('LAS files clipped')
        return os.path.join(orderWorkPath, "temp", "clipped")

    def clipLazFiles(self, lazPath, clipPolygonSrc, phase, coresCount, orderWorkPath='default'):
        print('Clipping LAZ files')
        arcpy.AddMessage('Clipping LAZ files')
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        if orderWorkPath == 'default':
            orderWorkPath = self.qcWorkPath
        if not os.path.exists(os.path.join(orderWorkPath, 'temp')):
            os.mkdir(os.path.join(orderWorkPath, 'temp'))
        if os.path.exists(os.path.join(orderWorkPath, 'temp', 'clipped')):
            shutil.rmtree(os.path.join(orderWorkPath, 'temp', 'clipped'))
        os.mkdir(os.path.join(orderWorkPath, 'temp', 'clipped'))
        selection = False
        arcpy.AddMessage('phase = ' + str(phase) + ', las_attributes_prepro exists: ' + str(arcpy.Exists('las_attributes_prepro_' + self.lotNumStr)))
        if phase == 1 and arcpy.Exists('las_attributes_prepro_' + self.lotNumStr):
            arcpy.AddMessage('LAZ selection will be refined by las_attributes_prepro_' + self.lotNumStr)
            arcpy.MakeFeatureLayer_management('las_attributes_prepro_' + self.lotNumStr, 'footprints_lyr')
            selection = True
        else:
            arcpy.AddMessage('WARNING: all LAZ files will be pushed to convert and clip operation')
            lazclipInput = lazPath
        if selection:
            if os.path.exists(os.path.join(orderWorkPath, 'temp', 'shp_tm34')):
                shutil.rmtree(os.path.join(orderWorkPath, 'temp', 'shp_tm34'))    
            os.mkdir(os.path.join(orderWorkPath, 'temp', 'shp_tm34'))
            clipPolygonSrcTm34 = self.transformFcFromJtsk03ToTm34(clipPolygonSrc, os.path.join(orderWorkPath, 'temp', 'shp_tm34'))
            arcpy.SelectLayerByLocation_management('footprints_lyr', 'INTERSECT', clipPolygonSrcTm34, '', 'NEW_SELECTION')
            arcpy.CopyFeatures_management('footprints_lyr', 'temp_selected_footprints')
            tempLazDestination = os.path.join(orderWorkPath, 'temp', 'laz_selection')
            if os.path.exists(tempLazDestination):
                shutil.rmtree(tempLazDestination)
            os.mkdir(tempLazDestination)
            self.selectAndCopyLazFiles('temp_selected_footprints', lazPath, tempLazDestination, '_jtsk03_bpv')
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_selected_footprints')
            lazclipInput = tempLazDestination
        
        # Convert LAZ files to LAS files
        lasclipInput = os.path.join(orderWorkPath, 'temp', 'las_from_laz')
        if os.path.exists(lasclipInput):
            shutil.rmtree(lasclipInput)
        os.mkdir(lasclipInput)
        subprocess.run(["laszip64", "-i", os.path.join(lazclipInput, "*.laz"), "-cores", str(coresCount), '-odir', lasclipInput, "-v"], shell=True)
        
        desc = arcpy.Describe(clipPolygonSrc)
        if desc.dataType == 'ShapeFile':
            pass
        elif desc.dataType == 'FeatureClass':
            if os.path.exists(os.path.join(orderWorkPath, 'temp', 'shp_jtsk03')):
                shutil.rmtree(os.path.join(orderWorkPath, 'temp', 'shp_jtsk03'))    
            os.mkdir(os.path.join(orderWorkPath, 'temp', 'shp_jtsk03'))
            arcpy.CopyFeatures_management(clipPolygonSrc, os.path.join(orderWorkPath, 'temp', 'shp_jtsk03', desc.name + '.shp'))
            clipPolygonSrc = os.path.join(orderWorkPath, 'temp', 'shp_jtsk03', desc.name + '.shp')            
            # this is workaround for waiting to lock release
            while (True):
                try:
                    arcpy.RepairGeometry_management(clipPolygonSrc, 'DELETE_NULL', 'OGC')
                    arcpy.RepairGeometry_management(clipPolygonSrc, 'DELETE_NULL')
                    break
                except:
                    arcpy.AddMessage('Waiting for lock release')
                    time.sleep(2)
        else:
            raise TypeError('Please, provide only feature class or shapefile as clip polygon')

        subprocess.run(["lasclip64", "-i", os.path.join(lasclipInput, "*.las"), "-cores", str(coresCount), "-poly", os.path.abspath(clipPolygonSrc), '-odir', os.path.join(orderWorkPath, "temp", "clipped"), "-v"], shell=True)
        print('LAZ files clipped as LAS files')
        arcpy.AddMessage('LAZ files clipped as LAS files')
        return os.path.join(orderWorkPath, "temp", "clipped")
    
    def createLasDatasetAndMakeLasDatasetLayer(self, lasDatasetLyrName, classCodeArr=[], returnValueArr=[]):
        print('Creating LAS dataset and making LAS dataset layer with the name', lasDatasetLyrName)
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        lasDatasetSrc = os.path.join(self.qcWorkPath, lasDatasetLyrName.split('.lasd')[0] + '.lasd')

        if arcpy.Exists(lasDatasetSrc):
            os.remove(lasDatasetSrc)
            print('... Existing LAS dataset removed')
        arcpy.CreateLasDataset_management(os.path.join(self.lasPath), lasDatasetSrc)
        print('... Making LAS dataset layer')
        lasDatasetLyr = arcpy.MakeLasDatasetLayer_management(lasDatasetSrc, lasDatasetLyrName, classCodeArr, returnValueArr)
        return lasDatasetLyr

    def calculateDensityStatsOnLasDatasetLayerPerLot(self, lasDatasetLyr, overwriteStatsRaster=False):
        print('Calculating Density statistics on LAS dataset layer', lasDatasetLyr, 'per LOT', self.lotNumStr)
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        arcpy.env.snapRaster = os.path.join(self.assetsPath, self.snapRasterDir, self.snapRasterRstTm34)
        
        if str(lasDatasetLyr) == 'lastretrast_prepro_' + self.lotNumStr: # do not create new statistic raster lasretrast_prepro
            statsRastName = str(lasDatasetLyr)
        else:
            statsRastName = 'statsRast_' + str(lasDatasetLyr)

        if not arcpy.Exists(statsRastName) or overwriteStatsRaster:
            arcpy.env.overwriteOutput = True
            print("... Calculating statistic raster")
            arcpy.LasPointStatsAsRaster_management(lasDatasetLyr, statsRastName, 'POINT_COUNT', 'CELLSIZE', 10) # Sampling value 10 means cellsize 10m x 10m
            arcpy.env.overwriteOutput = False
        statsRaster = arcpy.Raster(os.path.join(self.qcWorkPath, self.qcGdb, statsRastName))
        print('... Converting statistic raster to polygon')
        arcpy.RasterToPolygon_conversion(statsRaster, "tempr2poly", 'NO_SIMPLIFY', 'Value')
        print('... Selecting LOT envelope')
        where = "lot_number = '" + self.lotNumStr + "'"
        arcpy.Select_analysis(os.path.join(self.assetsPath, self.lidarLotsDir, self.lidarLotsGdb, self.lidarLotsFcTm34), os.path.join(self.qcWorkPath, self.qcGdb, "temp_lidarLot"), where)
        print('... Calculating intersection between LOT envelope and raster to polygon data')
        arcpy.Intersect_analysis([os.path.join(self.qcWorkPath, self.qcGdb, "tempr2poly"), os.path.join(self.qcWorkPath, self.qcGdb, "temp_lidarLot")], \
            os.path.join(self.qcWorkPath, self.qcGdb, 'temp_intersectedStatsPoly'), 'ALL')
        print('... Erasing areas behind a LOT boundary')
        arcpy.Erase_analysis('temp_lidarLot', 'temp_intersectedStatsPoly', 'temp_erase')
        
        lotArea = 0
        sc = arcpy.da.SearchCursor('temp_lidarLot', 'SHAPE@AREA')
        for row in sc:
            lotArea += row[0]
        del sc
        coveredArea = 0
        pointCount = 0
        sc = arcpy.da.SearchCursor('temp_intersectedStatsPoly', ['SHAPE@AREA', 'gridcode'])
        for row in sc:
            coveredArea += row[0]
            pointCount += row[1]
        del sc
        notCoveredArea = 0
        sc = arcpy.da.SearchCursor('temp_erase', 'SHAPE@AREA')
        for row in sc:
            notCoveredArea += row[0]
        del sc
        densityWithHoles = pointCount / lotArea
        densityWithoutHoles = pointCount / (lotArea - notCoveredArea)
        densityOnCoveredArea = pointCount / coveredArea
        
        print('... Area of LOT is', lotArea, 'm2')
        print('... Covered area is', coveredArea, 'm2')
        print('... Not covered area is', notCoveredArea, 'm2')
        print('... Number of points is', pointCount)
        print('... Density on LOT area with holes', densityWithHoles)
        print('... Density on LOT area without holes', densityWithoutHoles)
        print('... Density on covered area', densityOnCoveredArea)

        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'tempr2poly')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_lidarLot')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_diss')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_erase')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_intersectedStatsPoly')
        
        return {
            'densityWithHoles': densityWithHoles,
            'densityWithoutHoles': densityWithoutHoles,
            'densityOnCoveredArea': densityOnCoveredArea
        }
    
    def reportLotStatistics(self):
        print('Creating/updating PDF report with LOT ' + self.lotNumStr + ' statistics')
        self.inCheck.isWkhtmltopdfInstalled()
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        allFields = [field.name for field in arcpy.ListFields('lot_statistics_' + self.lotNumStr)]

        totalCount = None
        classesClassifData = None
        densityData = {}

        densityFields = ['total_density_with_holes', 'total_density_without_holes', 'last_return_density_with_holes', 'last_return_density_without_holes', 'ground_density_with_holes', 'ground_density_without_holes']
        for densityField in densityFields:
            densityFieldValue = None
            if densityField in allFields:
                sc = arcpy.da.SearchCursor('lot_statistics_' + self.lotNumStr, densityField)
                for row in sc:
                    densityFieldValue = row[0]
                    break
            densityData[densityField] = densityFieldValue
    
        if 'class_0_cnt' in allFields: # classification fields are added in lot_statistics table
            totalCount = 0
            classDict = {}
            classNames = list(con.CLASSIFICATION_CLASSES.values())
            for className in classNames:
                classDict[className] = 0

            classifFields = ['total_cnt'] + ['class_{}_cnt'.format(classId) for classId in con.CLASSIFICATION_CLASSES.keys()]
            sc = arcpy.da.SearchCursor('lot_statistics_' + self.lotNumStr, classifFields)
            for row in sc:
                i = 1
                for className in classNames:
                    classDict[className] += row[i] if row[i] is not None else 0
                    i += 1
                totalCount += row[0] if row[0] is not None else 0

            if totalCount > 0:
                classesClassifData = self.prepareClassifClassesDataToTemplate(classDict)
            else:
                totalCount = None

        scriptsPath = os.path.abspath(os.path.split(sys.argv[0])[0])
        templateLoader = jinja2.FileSystemLoader(searchpath=os.path.join(scriptsPath, 'templates'))
        templateEnv = jinja2.Environment(loader=templateLoader)
        statsTemplateName = 'lot_stats_template.html'
        statsTemplate = templateEnv.get_template(statsTemplateName)
        statsRendered = statsTemplate.render(
            scriptsBaseDir=scriptsPath.replace('\\', '/'),
            pieChartSrc=classesClassifData['pieChartSrc'].replace('\\', '/') if classesClassifData else None,
            lotCaption=self.lotNumStr,
            classes=classesClassifData['templateData'] if classesClassifData else None,
            totalCount=self.prettyNumber(totalCount).split(',')[0] if totalCount else None,
            created=time.strftime("%d.%m.%Y %H:%M:%S"),
            totalDensityWithHoles=self.prettyNumber(densityData['total_density_with_holes'], 1),
            totalDensityWithoutHoles=self.prettyNumber(densityData['total_density_without_holes'], 1),
            lastReturnDensityWithHoles=self.prettyNumber(densityData['last_return_density_with_holes'], 1),
            lastReturnDensityWithoutHoles=self.prettyNumber(densityData['last_return_density_without_holes'], 1),
            groundDensityWithHoles=self.prettyNumber(densityData['ground_density_with_holes'], 1),
            groundDensityWithoutHoles=self.prettyNumber(densityData['ground_density_without_holes'], 1)
        )
        htmlFile = codecs.open(os.path.join(self.qcWorkPath, 'lot_stats_' + self.lotNumStr + '.html'), 'w', 'utf-8')
        htmlFile.write(statsRendered)
        htmlFile.close()
        try:
            pdfkit.from_file(os.path.join(self.qcWorkPath, 'lot_stats_' + self.lotNumStr + '.html'), os.path.join(self.qcWorkPath, 'lot_stats_' + self.lotNumStr +'.pdf'))
        except Exception as e:
            arcpy.AddMessage(e)
        if classesClassifData:
            os.remove(classesClassifData['pieChartSrc'])
        os.remove(os.path.join(self.qcWorkPath, 'lot_stats_' + self.lotNumStr + '.html'))
    
    def prepareClassifClassesDataToTemplate(self, classDict):
        print('Making pie plot with distribution of points in classification classes')
        fig, ax = plt.subplots(figsize=(10, 5), subplot_kw=dict(aspect="equal"))
        ids = list(con.CLASSIFICATION_CLASSES.keys())
        counts = list(classDict.values())
        classes = list(classDict.keys())
        colors = list(con.CLASS_COLORS.values())
        percents = ['{:.3f}'.format(100*perc/np.sum(counts)).replace('.', ',') for perc in counts]
        classesPerc = copy.deepcopy(classes)
        for i, perc in enumerate(percents):
            classesPerc[i] += ' ' + perc + ' %'

        classifData = pandas.DataFrame({
            'counts': counts,
            'classes' : classes,
            'colors': colors,
            'classesPerc': classesPerc,
            'percents': percents,
            'ids': ids
        })

        sortedByCounts = classifData.sort_values(by=['counts'])

        wedges, texts = ax.pie(
            sortedByCounts['counts'], 
            startangle=90,
            colors=sortedByCounts['colors'],
        )
        
        ax.legend(wedges[::-1], sortedByCounts['classesPerc'][::-1],
                title=con.TRAN_CLASSES,
                loc="center left",
                bbox_to_anchor=(1, 0, 0.5, 1))
        pieChartSrc = os.path.join(self.qcWorkPath, 'temp_plot.png')
        plt.savefig(pieChartSrc)

        classTemplateData = []
        for index, row in sortedByCounts.iterrows():
            classTemplateData.insert(0, {
                'id': row['ids'],
                'name': row['classes'],
                'count': self.prettyNumber(row['counts']).split(',')[0],
                'perc': row['percents']
            })
        
        return {
            'templateData': classTemplateData,
            'pieChartSrc': pieChartSrc
        }
    
    def lasBatchTiling(self, sourceLasPath, outTileDir, tileSize, coresCount, buffer=10, shouldSetPointSourceId=True, shouldRemoveTempDir = True):
        print("LAS Batch Tiling")
        lases = [files for files in os.listdir(sourceLasPath) if files.lower().endswith(".las")]
        print(lases)

        if not os.path.isdir(os.path.join(self.qcWorkPath, "temp")):
            os.mkdir(os.path.join(self.qcWorkPath, "temp"))
        os.mkdir(os.path.join(self.qcWorkPath, "temp", "ids_added"))
        if os.path.exists(outTileDir):
            outTilePath = outTileDir
        else:
            outTilePath = os.path.join(self.qcWorkPath, outTileDir)
        if os.path.exists(outTilePath):
            shutil.rmtree(outTilePath)
        self.waitForLockReleaseAndProcess('os.mkdir', params=[outTilePath])

        inputPath = sourceLasPath
        if shouldSetPointSourceId:
            print("... Setting Point Source IDs")
            pointsourceid = 0
            idtofile = {}
            jobs = []

            for las in lases:  
                # multiprocessing
                proc = multiprocessing.Process(target=self.helper_setPntSrcId_multiproc, args=(inputPath, las, pointsourceid))
                idtofile[pointsourceid] = las
                pointsourceid += 1

                jobs.append(proc)
                proc.start()
                numofalive = 0
                for proc in jobs:
                    if proc.is_alive():
                        numofalive += 1
                print("... Number of alive processes is ", numofalive)
                while numofalive >= coresCount:
                    time.sleep(2)
                    numofalive = 0
                    for proc in jobs:
                        if proc.is_alive():
                            numofalive += 1
            for proc in jobs:
                proc.join()

            idtofile_out = open(os.path.join(outTilePath, 'pointSourceIdToLasNameMapping.csv'), 'w')
            for key, val in idtofile.items():
                idtofile_out.write('{};{};\n'.format(key, val))
            idtofile_out.close()
            inputPath = os.path.join(self.qcWorkPath, "temp", "ids_added")
        
        print("... Creating indexes (LAX)")
        subprocess.run(["lasindex64", "-i", os.path.join(inputPath, "*.las")], shell=True)
        print("... Tiling")
        if buffer > 0:
            subprocess.run(["lastile", "-i", os.path.join(inputPath, "*.las"), "-cores", str(coresCount), "-tile_size", str(tileSize), "-buffer", str(buffer), "-reversible", "-odir", outTilePath, "-o", "tile.las", "-v", "-merged"], shell=True)
        else:
            subprocess.run(["lastile", "-i", os.path.join(inputPath, "*.las"), "-cores", str(coresCount), "-tile_size", str(tileSize), "-odir", outTilePath, "-o", "tile.las", "-v", "-merged"], shell=True)
        if shouldRemoveTempDir:
            shutil.rmtree(os.path.join(self.qcWorkPath, "temp"))
        print('LAS Batch Tiling Done')
        return os.path.join(outTilePath)

    def helper_setPntSrcId_multiproc(self, inputPath, las, pointsourceid):
        subprocess.run(["las2las64", "-i", os.path.join(inputPath, las), "-set_point_source", str(pointsourceid), "-o", os.path.join(self.qcWorkPath, "temp", "ids_added", las), "-v"], shell=True)  

    def lasBatchRestoring(self, inTileDir, outRestoredLasPath, coresCount, buffer=10, pointSourceIdToLasNameMappingCsvSrc=None):
        print('LAS Batch Restoring')

        # create log file
        open(os.path.join(self.qcWorkPath, "lasBatchRestoringLog.txt"), 'w').close()

        if not os.path.exists(outRestoredLasPath):
            os.mkdir(outRestoredLasPath)

        if os.path.exists(inTileDir):
            inputPath = inTileDir
            workPath = inputPath
        else:
            inputPath = os.path.join(self.qcWorkPath, inTileDir)
            workPath = self.qcWorkPath
        if not os.path.exists(inputPath):
            raise IOError('Input LAS tiles path', inputPath, 'does not exist')
        if not os.path.isdir(os.path.join(workPath, "temp")):
            os.mkdir(os.path.join(workPath, "temp"))

        if buffer > 0:
            print("... Removing Buffer")
            os.mkdir(os.path.join(workPath, "temp", "tiles_buffer_removed"))
            outputPath = os.path.join(workPath, "temp", "tiles_buffer_removed")
            subprocess.run(["lastile64", "-i", os.path.join(inputPath, "*.las"), "-remove_buffer", "-cores", str(coresCount), "-odir", outputPath, "-v"], shell=True)
            inputPath = outputPath

        print("... Extracting Tiles According To Point Source ID")
        os.mkdir(os.path.join(workPath, "temp", "classified_originals_to_merge"))
        subprocess.run(["lassplit64", "-i", os.path.join(inputPath, "*.las"), "-cores", str(coresCount), "-odir", os.path.join(workPath, "temp", "classified_originals_to_merge"), "-olas", "-v"], shell=True)

        print("... Merging Tiles According To Point Source ID")
        files = os.listdir(os.path.join(workPath, "temp", "classified_originals_to_merge"))
        distinct_ends = set([item[-11:] for item in files])

        pid = 0
        i = 1
        jobs = []
        for distinct_end in distinct_ends:    
            # multiprocessing
            pid += 1 
            print("... pid is ", pid)
            proc = multiprocessing.Process(target=self.helper_lasBatchRestoring_multiproc, args=(pid, workPath, distinct_end, outRestoredLasPath, files))
            jobs.append(proc)
            proc.start()
            numofalive = 0
            for proc in jobs:
                if proc.is_alive():
                    numofalive += 1
            print("... Number of alive processes is ", numofalive)
            while numofalive >= coresCount:
                time.sleep(2)
                numofalive = 0
                for proc in jobs:
                    if proc.is_alive():
                        numofalive += 1
            i += 1
        for proc in jobs:
            proc.join()

        print("... Recovering Original Names")
        if pointSourceIdToLasNameMappingCsvSrc:
            idtofile_in = open(pointSourceIdToLasNameMappingCsvSrc, 'r')
        else:
            idtofile_in = open(os.path.join(workPath, inTileDir, 'pointSourceIdToLasNameMapping.csv'), 'r')
        lines = [line.split(';') for line in idtofile_in.readlines()]
        idtofile = {}
        for line in lines:
            idtofile[line[0]] = line[1]
        idtofile_in.close()
        print(idtofile)

        lases = [files for files in os.listdir(outRestoredLasPath) if files.lower().endswith(".las")]
        for las in lases:
            lasid = int(las.split(".")[0][-7:])
            os.rename(os.path.join(outRestoredLasPath, las), os.path.join(outRestoredLasPath, idtofile[str(lasid)]))
        
        print('LAS Batch Restoring done')
    
    def helper_lasBatchRestoring_multiproc(self, pid, workPath, distinctEnd, outRestoredLasPath, lasFiles):
        try:
            subprocess.run(["lasmerge64", "-i", os.path.join(workPath, "temp", "classified_originals_to_merge", "*" + distinctEnd), "-o", os.path.join(outRestoredLasPath, "merge_" + distinctEnd), "-v"], shell=True)
            filesSubset = filter(lambda fileName: fileName if distinctEnd in fileName else False, lasFiles)
            for lasFile in filesSubset:
                os.remove(os.path.join(workPath, 'temp', 'classified_originals_to_merge', lasFile)) # Remove input files to free disk space
        except Exception as e:
            print("... Error in process", pid, str(e))
            logfile = open(os.path.join(self.qcWorkPath, "lasBatchRestoringLog.txt"), 'a')
            logfile.write('Error occured in the process with PID ' + str(pid) + '\n')
            traceback.print_exc(file = logfile)
            logfile.close()

    """
    Possible **args: joinOperation:string, addJoinFcFields:list|boolean, mergeRule:string
    """
    def spatialJoinWrapper(self, targetFcSrc, joinFcSrc, outFcSrc, **args):
        print('Spatial joining of', joinFcSrc, 'on', targetFcSrc)
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb) 
        joinOperation = 'JOIN_ONE_TO_ONE' if 'joinOperation' not in args else args['joinOperation']
        mergeRule = 'join' if 'mergeRule' not in args else args['mergeRule']
        
        joinFieldNames = []
        addJoinFcTable = True
        if 'addJoinFcFields' in args and isinstance(args['addJoinFcFields'], list):
            joinFieldNames = args['addJoinFcFields']
        elif 'addJoinFcFields' in args and isinstance(args['addJoinFcFields'], bool):
            addJoinFcTable = args['addJoinFcFields']

        # Create field mapping object
        fieldmappings = arcpy.FieldMappings()
        fieldmappings.addTable(targetFcSrc)
        if addJoinFcTable and len(joinFieldNames) == 0:
            fieldmappings.addTable(joinFcSrc)
        elif addJoinFcTable and len(joinFieldNames) > 0:
            actualFieldNames = [field.name for field in arcpy.ListFields(joinFcSrc)]
            for fieldName in joinFieldNames:
                if fieldName in actualFieldNames:
                    newFieldMap = arcpy.FieldMap()
                    newFieldMap.addInputField(joinFcSrc, fieldName)
                    newFieldMap.mergeRule = mergeRule
                    newFieldMap.joinDelimiter = ', '
                    
                    newField = newFieldMap.outputField
                    newField.length = 4096
                    newField.name = fieldName
                    newFieldMap.outputField = newField
                    fieldmappings.addFieldMap(newFieldMap)
                    del newFieldMap
        

        # Run spatial join
        arcpy.SpatialJoin_analysis(targetFcSrc, joinFcSrc, outFcSrc, joinOperation, 'KEEP_ALL', fieldmappings, 'INTERSECT')
        print('Spatial join completed')

    def extractFeatureClassPointsToArray(self, pointFcSrc, where=None):
        print('Extracting points of feature class', os.path.basename(pointFcSrc), 'to array')
        arcpyPoints = []
        sc = arcpy.da.SearchCursor(pointFcSrc, ['SHAPE@XY'], where)
        for row in sc:
            arcpyPoints.append(row[0])
        return arcpyPoints

    @staticmethod
    def fetchOrdinatesToArray(arcpyPoints, xOrY='x'):
        print('Fetching', xOrY, 'ordinates to array of arcpy points', arcpyPoints)
        if xOrY == 'x':
            ordinatesArray = [arcpyPoint[0] for arcpyPoint in arcpyPoints]
        else:
            ordinatesArray = [arcpyPoint[1] for arcpyPoint in arcpyPoints]
        return ordinatesArray

    @staticmethod
    def findLinearRegressionCoeficients(xArray, yArray, applyModulo = 0):
        print('Finding linear regression a, b coeficients for xArray', xArray, 'and for yArray', yArray)
        if len(xArray) != len(yArray):
            raise Exception('xArray must be of the same length as yArray')
        npXArray = np.array(xArray)
        npYArray = np.array(yArray)
        if applyModulo:
            print('... Applying modulo', applyModulo)
            npXArray = np.mod(npXArray, applyModulo)
            npYArray = np.mod(npYArray, applyModulo)
        # sumX = np.sum(npXArray)
        # sumY = np.sum(npYArray)
        # sumXY = np.sum(npXArray * npYArray)
        # sumXSquared = np.sum(npXArray * npXArray)
        # n = len(npXArray)

        # a = (n * sumXY - sumX * sumY) / (n * sumXSquared - sumX ** 2)
        # b = (sumY - a * sumX) / n
        linregressRes = scipy.stats.linregress(npXArray, npYArray)
        a = linregressRes.slope
        b = linregressRes.intercept
        return {'a': a, 'b': b}

    @staticmethod
    def findIntersectionXYOfStraightLines(aLine1, bLine1, aLine2, bLine2):
        print('Finding XY coordinates of an intersection of line1', aLine1, bLine1, 'and line2', aLine2, bLine2)
        x = (bLine2 - bLine1) / (aLine1 - aLine2)
        y = (aLine2 * bLine1 - aLine1 * bLine2) / (aLine2 - aLine1)
        yCorrection1 = aLine1 * x + bLine1
        xCorrection1 = (y - bLine1) / aLine1
        yCorrection2 = aLine2 * x + bLine2
        xCorrection2 = (y - bLine2) / aLine2
        print('y =', y, 'yCorrection2 =', yCorrection2)
        print('x =', x, 'xCorrection2 =', xCorrection2)
        print('y =', y, 'yCorrection1 =', yCorrection1)
        print('x =', x, 'xCorrection1 =', xCorrection1)

        return [x, y]

    @staticmethod
    def findXByY(y, a, b):
        print('Finding X ordinate by Y', y, 'a', a, 'b', b)
        x = (y - b) / a
        print('x = ', x)
        return y
    
    @staticmethod
    def findYByX(x, a, b):
        print('Finding Y ordinate by X', x, 'a', a, 'b', b)
        y = a * x + b
        print('y = ', y)
        return y

    def addPointToFeatureClass(self, fcName, arcpyPoint, epsgCode = 3046):
        print('Adding point', arcpyPoint, 'to the feature class', fcName)
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        if not arcpy.Exists(fcName):
            print('... Feature class', fcName, 'does not exist. Creating empty feature class')
            sr = arcpy.SpatialReference(epsgCode)
            arcpy.CreateFeatureclass_management(arcpy.env.workspace, fcName, 'POINT', spatial_reference = sr)
        editor = arcpy.da.Editor(arcpy.env.workspace)
        editor.startEditing()
        editor.startOperation()
        ic = arcpy.da.InsertCursor(fcName, ['SHAPE@'])
        ic.insertRow([arcpyPoint])
        del ic
        editor.stopOperation()
        editor.stopEditing(True)
        del editor
        arcpy.AddXY_management(fcName)
        print('Point successfully added')

    @staticmethod
    def getModuloDeltas(point, modulo):
        deltaX = point[0] - (point[0] % modulo)
        deltaY = point[1] - (point[1] % modulo)
        print('modulo', modulo)
        print('x', point[0], 'Modulo delta X', deltaX)
        print('y', point[1], 'Modulo delta Y', deltaY)
        return [deltaX, deltaY]
    
    @staticmethod
    def applyModuloDeltasOnPoint(point, moduloDeltas):
        x = point[0] + moduloDeltas[0]
        y = point[1] + moduloDeltas[1]
        return [x, y]

    def prepareFlathouseVerticesFromLasFlaggedWalls(self, lasWithFlaggedWallsSrc, epsgCode = 3046):
        print('Preparing flathouse vertices from the points flagged as model keypoints and withheld')
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)

        if lasWithFlaggedWallsSrc: # could be None. Then this block will not be processed.
            if not os.path.exists(os.path.join(self.qcWorkPath, "temp")):
                os.mkdir(os.path.join(self.qcWorkPath, "temp"))
            os.mkdir(os.path.join(self.qcWorkPath, "temp", "shp_walls"))
            
            print('... Extracting model_keypoints and withheld points to SHP files')
            subprocess.run(['las2shp', '-i', lasWithFlaggedWallsSrc, '-keep_keypoint', '-o', os.path.join(self.qcWorkPath, "temp", "shp_walls", "walls_keypoint.shp")], shell = True)
            subprocess.run(['las2shp', '-i', lasWithFlaggedWallsSrc, '-keep_withheld', '-o', os.path.join(self.qcWorkPath, "temp", "shp_walls", "walls_withheld.shp")], shell = True)

            print('... Processing SHP files to find clusters (particular walls)')
            arcpy.MultipartToSinglepart_management(os.path.join(self.qcWorkPath, "temp", "shp_walls", "walls_keypoint.shp"), 'temp_walls_keypoint_sp')
            arcpy.MultipartToSinglepart_management(os.path.join(self.qcWorkPath, "temp", "shp_walls", "walls_withheld.shp"), 'temp_walls_withheld_sp')
            arcpy.FindPointClusters_gapro('temp_walls_keypoint_sp', 'ha_walls_keypoint_clusters', 'DBSCAN', 2, '100 Meters')
            arcpy.FindPointClusters_gapro('temp_walls_withheld_sp', 'ha_walls_withheld_clusters', 'DBSCAN', 2, '100 Meters')
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_walls_keypoint_sp')
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_walls_withheld_sp')
        
        sr = arcpy.SpatialReference(epsgCode)
        arcpy.DefineProjection_management('ha_walls_keypoint_clusters', sr)
        arcpy.DefineProjection_management('ha_walls_withheld_clusters', sr)
        clusterIdsKeypoint = self.getClusterIds('ha_walls_keypoint_clusters')
        clusterIdsWithheld = self.getClusterIds('ha_walls_withheld_clusters')
        if len(clusterIdsKeypoint) is not len(clusterIdsWithheld):
            raise Exception('Number of model_keypoint clusters and withheld clusters is not the same. Please contact Martin Kalivoda.')
        for clusterIdKeypoint in clusterIdsKeypoint:
            for clusterIdWithheld in clusterIdsWithheld:
                print('... Calculating intersection point of model_keypoint cluster', clusterIdKeypoint, 'and withheld cluster', clusterIdWithheld)
                wallPointsKeypoint = self.extractFeatureClassPointsToArray(os.path.join(arcpy.env.workspace, 'ha_walls_keypoint_clusters'), 'CLUSTER_ID = ' + str(clusterIdKeypoint))
                wallPointsWithheld = self.extractFeatureClassPointsToArray(os.path.join(arcpy.env.workspace, 'ha_walls_withheld_clusters'), 'CLUSTER_ID = ' + str(clusterIdWithheld))
                xArrKeypoint = self.fetchOrdinatesToArray(wallPointsKeypoint, 'x')
                yArrKeypoint = self.fetchOrdinatesToArray(wallPointsKeypoint, 'y')
                xArrWithheld = self.fetchOrdinatesToArray(wallPointsWithheld, 'x')
                yArrWithheld = self.fetchOrdinatesToArray(wallPointsWithheld, 'y')
                lineKeypoint = self.findLinearRegressionCoeficients(xArrKeypoint, yArrKeypoint)
                lineWithheld = self.findLinearRegressionCoeficients(xArrWithheld, yArrWithheld)
                intersectionKeypointWithheld = self.findIntersectionXYOfStraightLines(lineKeypoint['a'], lineKeypoint['b'], lineWithheld['a'], lineWithheld['b'])
                self.addPointToFeatureClass('ha_flathouses_las_vertices_flagged', intersectionKeypointWithheld)

    def getClusterIds(self, inFcName):
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        clusterIdsAll = []
        sc = arcpy.da.SearchCursor(inFcName, ['CLUSTER_ID'])
        for row in sc:
            clusterIdsAll.append(row[0])
        clusterIdsDistinct = set(clusterIdsAll)
        return clusterIdsDistinct

    def extendPointGeometryByZInformation(self, pointFcName, attributeZName, pointFcPath = 'default'):
        print('Extending 2D geometry to 3D of', pointFcName, 'from attribute', attributeZName)
        if pointFcPath == 'default':
            pointFcPath = os.path.join(self.qcWorkPath, self.qcGdb)
        elif not os.path.exists(pointFcPath):
            raise IOError('Path pointFcPath ' + str(pointFcPath) + ' does not exist')
        desc = arcpy.Describe(os.path.join(pointFcPath, pointFcName))
        epsgCode = desc.spatialReference.factoryCode
        sr = arcpy.SpatialReference(epsgCode)
        arcpy.CreateFeatureclass_management(pointFcPath, 'new_' + pointFcName, 'POINT', os.path.join(pointFcPath, pointFcName), 'DISABLED', 'ENABLED', sr)
        arcpy.Append_management(os.path.join(pointFcPath, pointFcName), os.path.join(pointFcPath, 'new_' + pointFcName), 'TEST')
        editor = arcpy.da.Editor(pointFcPath)
        editor.startEditing()
        uc = arcpy.da.UpdateCursor(os.path.join(pointFcPath, 'new_' + pointFcName),['SHAPE@', attributeZName])
        for row in uc:
            pnt = arcpy.Point(row[0].firstPoint.X, row[0].firstPoint.Y, row[1])
            row[0] = pnt
            editor.startOperation()
            uc.updateRow(row)
            editor.stopOperation()
        del uc
        editor.stopEditing(True)
        del editor
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', pointFcPath, pointFcName)
        arcpy.Copy_management(os.path.join(pointFcPath, 'new_' + pointFcName), os.path.join(pointFcPath, pointFcName))
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', pointFcPath, 'new_' + pointFcName)
        print('Geometry successfully extended to 3D')

    @staticmethod
    def waitForLockReleaseAndProcess(method, workspacePath: str = None, params: list = []): # params should be of any type corresponding to method
        # this is workaround for waiting to lock release
        while (True):
            try:
                # Extend method list
                if method == 'arcpy.Delete_management':
                    itemSrc = os.path.join(workspacePath, params)
                    arcpy.Delete_management(itemSrc)
                elif method == 'shutil.move':
                    shutil.move(params[0], params[1])
                elif method == 'os.mkdir':
                    os.mkdir(params[0])
                break
            except Exception as e:
                arcpy.AddMessage('Waiting for lock release due to the exception: ' + str(e))
                time.sleep(2)

    def helper_findOutUniqueOverlaps(self, phaseName):
        # find out unique overlaps
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        if not arcpy.Exists("las_overlaps_pivot_" + phaseName + "_" + self.lotNumStr):
            raise OSError('Table las_overlaps_pivot_' + phaseName + '_' + self.lotNumStr + ' does not exist. Please, create and populate it first.')
        overlaps_id = []
        sc = arcpy.da.SearchCursor("las_overlaps_pivot_" + phaseName + "_" + self.lotNumStr, ['lasid1', 'lasid2'])
        for scrow in sc:
            overlaps_id.append([scrow[0], scrow[1]])
        
        for item in overlaps_id:
            item.sort()
        delete_id = []
        for i in range(0, len(overlaps_id)):
            for j in range(i+1, len(overlaps_id)):
                if overlaps_id[i] == overlaps_id[j]:
                    delete_id.append(j)
        delete_id.sort(reverse=True)
        for index in delete_id:
            del overlaps_id[index]
        return overlaps_id

    def copyHeightDiffsRastersFromtTemporaryGdbsToMainGdb(self, rasterSuffix):
        heightDiffsGdb =  'height_diffs_' + self.lotNumStr + '.gdb' if rasterSuffix == '' else 'height_diffs_' + rasterSuffix + '_' + self.lotNumStr + '.gdb' 
        print("... Copying height diffs rasters from temporary GDBs to", heightDiffsGdb)
        arcpy.env.workspace = self.qcWorkPath
        arcpy.env.overwriteOutput = True

        if not arcpy.Exists(os.path.join(self.qcWorkPath, heightDiffsGdb)):
            arcpy.CreateFileGDB_management(self.qcWorkPath, heightDiffsGdb, 'CURRENT')

        tempgdbs = arcpy.ListWorkspaces("temp*", "FileGDB")
        i = 1
        for tempgdb in tempgdbs:
            print("... " +  str(i) + " / " + str(len(tempgdbs)))
            arcpy.env.workspace = tempgdb
            print(tempgdb)
            try:
                likeStr = 'height_diffs_' + rasterSuffix + '*'
                raster = arcpy.ListRasters(likeStr)[0]
                arcpy.Copy_management(os.path.join(tempgdb, raster), os.path.join(self.qcWorkPath, heightDiffsGdb, raster))
                arcpy.Delete_management(tempgdb)
            except Exception as e:
                print("Error occured during processing of " + tempgdb)
                logfile = open(os.path.join(self.qcWorkPath, "heightDiffs" + rasterSuffix + "Log.txt"), 'a')
                logfile.write("Error occured during processing of " + tempgdb + "\n")
                traceback.print_exc(file = logfile)
                logfile.close()
            i += 1
        arcpy.env.overwriteOutput = False
        return heightDiffsGdb

    def helper_hdiff_multiproc(self, pid = 1, indices = [], overlapsCount = 0, phaseName = 'prepro', rasterSuffix = '', classCodeArr = [], pixelSize = 10):
        try:
            print("... Processing pair: " + indices[0] + " + " + indices[1] + " in process " + str(pid), "of", str(overlapsCount))
            arcpy.CheckOutExtension('3D')
            arcpy.env.overwriteOutput = True
            arcpy.CreateFileGDB_management(self.qcWorkPath, "temp" + str(pid) + ".gdb")
            arcpy.env.workspace = os.path.join(self.qcWorkPath, "temp" + str(pid) + ".gdb")
            arcpy.env.snapRaster = os.path.join(self.assetsPath, self.snapRasterDir, self.snapRasterRstTm34)
            qcgdb = os.path.join(self.qcWorkPath, self.qcGdb)
            tempgdb = os.path.join(self.qcWorkPath, "temp" + str(pid) + ".gdb")
            where = "lasid IN ('" + indices[0] + "', '" + indices[1] + "')"

            workdir = os.path.join(self.qcWorkPath, "tempdir" + str(pid))
            if not os.path.exists(workdir):
                os.mkdir(workdir)

            if (len(classCodeArr) > 0):
                sc = arcpy.da.SearchCursor(os.path.join(qcgdb, "las_attributes_" + phaseName +"_" + self.lotNumStr), ['filename', 'OBJECTID'], where)    
                for row in sc:
                    self.extractLasEnvelopeToPolygonFC(row[0], 'las_envelope_' + str(row[1]), workdir, 'temp_envelope.lasd', classCodeArr, True)
                del sc
                envelopeFcList = arcpy.ListFeatureClasses('las_envelope_*', 'Polygon')
                print('... Envelope feature class list is', envelopeFcList)
                arcpy.Intersect_analysis(envelopeFcList, "tempoverlap" + str(pid))
            else:
                arcpy.MakeFeatureLayer_management(os.path.join(qcgdb, "las_attributes_" + phaseName + "_" + self.lotNumStr), os.path.join(tempgdb, "templyr" + str(pid)), where)
                arcpy.Intersect_analysis(os.path.join(tempgdb, "templyr" + str(pid)), "tempoverlap" + str(pid))
            
            if rasterSuffix == '':
                outRast = "height_diffs_" + str(indices[0]).replace("{", "").replace("}", "").replace("-","_") + "__" + str(indices[1]).replace("{", "").replace("}", "").replace("-","_")
            else:
                outRast = "height_diffs_" + rasterSuffix + '_' + str(indices[0]).replace("{", "").replace("}", "").replace("-","_") + "__" + str(indices[1]).replace("{", "").replace("}", "").replace("-","_")

            if int(arcpy.GetCount_management("tempoverlap" + str(pid)).getOutput(0)) > 0:
                # Calculate models
                sc = arcpy.da.SearchCursor(os.path.join(qcgdb, "las_attributes_" + phaseName +"_" + self.lotNumStr), ["filename"], where)
                idwnames = []
                for row in sc:
                    if not arcpy.Exists(os.path.join(workdir, "temp" + str(pid) + ".lasd")):
                        arcpy.CreateLasDataset_management(os.path.join(self.lasPath, row[0]), os.path.join(workdir, "temp" + str(pid) + ".lasd"))
                        lasDatasetLyr = arcpy.MakeLasDatasetLayer_management(os.path.join(workdir, "temp" + str(pid) + ".lasd"), "temp" + str(pid), classCodeArr)
                    else:
                        arcpy.AddFilesToLasDataset_management(os.path.join(workdir, "temp" + str(pid) + ".lasd"), os.path.join(self.lasPath, row[0]))
                        lasDatasetLyr = arcpy.MakeLasDatasetLayer_management(os.path.join(workdir, "temp" + str(pid) + ".lasd"), "temp" + str(pid), classCodeArr)

                    print("... Extracting overlaping LAS ", row[0], "in process", pid)
                    arcpy.ExtractLas_3d(lasDatasetLyr, workdir,"", os.path.join(tempgdb, 'tempoverlap' + str(pid)), 'PROCESS_EXTENT', "", out_las_dataset = os.path.join(workdir, "templas2dem" + str(pid) + ".lasd"))

                    desc = arcpy.Describe(os.path.join(workdir, "templas2dem" + str(pid) + ".lasd"))
                    if desc.fileCount > 0: # Check if there is 1 las file at least (e.g. LAS file don't have to have any ground points in the provided overlap)
                        logfile = open(os.path.join(self.qcWorkPath, "heightDiffs" + rasterSuffix + "Log.txt"), 'a')
                        msgParams = str('Parameters to LASToMultipoint in PID: ' + str(pid) + '\n' +
                            'input: ' + os.path.join(workdir, row[0]) + '\n' +
                            'out_feature_class: ' + os.path.join(tempgdb, "tempLAS2pnts" + str(pid)) + '\n' +
                            'average_point_spacing: ' + str(0.1) + '\n' +
                            'class_code: ' + str(classCodeArr) + '\n' +
                            'return: ' + 'ANY_RETURNS' + '\n\n'
                        )
                        logfile.write(msgParams)
                        logfile.close()
                        arcpy.LASToMultipoint_3d(os.path.join(workdir, row[0]), os.path.join(tempgdb, "tempLAS2pnts" + str(pid)), 0.1, classCodeArr, 'ANY_RETURNS')
                        idwnames.append("idw_" + row[0].lower().split(".las")[0] + "_process" + str(pid))
                        print("... IDW in process " + str(pid))
                        arcpy.Idw_3d(os.path.join(tempgdb, 'tempLAS2pnts' + str(pid)), 'Shape.Z', os.path.join(tempgdb, "idw_temp" + str(pid)), pixelSize, 2, 12)
                        extent = arcpy.Describe(os.path.join(tempgdb, "tempoverlap" + str(pid))).Extent
                        envelope = str(extent.XMin) + " " + str(extent.YMin) + " " + str(extent.XMax) + " " + str(extent.YMax)
                        arcpy.Clip_management(os.path.join(tempgdb, 'idw_temp' + str(pid)), envelope, os.path.join(tempgdb,"idw_" + row[0].lower().split(".las")[0] + "_process" + str(pid)),\
                            os.path.join(tempgdb, "tempoverlap" + str(pid)), "", 'ClippingGeometry', 'NO_MAINTAIN_EXTENT')
                        arcpy.RemoveFilesFromLasDataset_management(os.path.join(workdir, "templas2dem" + str(pid) + ".lasd"), workdir)
                        arcpy.RemoveFilesFromLasDataset_management(os.path.join(workdir, "temp" + str(pid) + ".lasd"), self.lasPath)
                        os.remove(os.path.join(workdir, "templas2dem" + str(pid) + ".lasd"))
                        self.waitForLockReleaseAndProcess('arcpy.Delete_management', tempgdb, "idw_temp" + str(pid))
                        temp_las = [item for item in os.listdir(workdir) if item.lower().startswith(row[0].lower().split('.las')[0])]
                        for item in temp_las:
                            os.remove(os.path.join(workdir, item))
                        self.waitForLockReleaseAndProcess('arcpy.Delete_management', tempgdb, "tempLAS2pnts" + str(pid))
                    else:
                        print('... There is no LAS file in the las dataset templas2dem')
                    if arcpy.Exists(lasDatasetLyr):
                        arcpy.Delete_management(lasDatasetLyr)
                    del lasDatasetLyr
                del sc
                os.remove(os.path.join(workdir, "temp" + str(pid) + ".lasd"))    
            
                try:
                    print("... Raster subtraction and absolute values in process " + str(pid))
                    arcpy.CheckOutExtension('Spatial')
                    minusrast = arcpy.sa.Minus(os.path.join(tempgdb, idwnames[0]), os.path.join(tempgdb, idwnames[1]))
                    absrast = arcpy.sa.Abs(minusrast)
                    absrast.save(os.path.join(tempgdb, outRast))
                    arcpy.CheckInExtension('Spatial')
                except Exception as e:
                    print('... WARNING: There is an exception during the postprocess of IDW rasters (subtraction, absolute values):', e)
                    arcpy.CreateRasterDataset_management(tempgdb, outRast, pixelSize, '32_BIT_FLOAT')
            else: # Create empty raster due to "check if exists algorithm"
                print('WARNING: There is no overlap between given LAS files (it could be due to different envelopes for particullar point class)')
                arcpy.CreateRasterDataset_management(tempgdb, outRast, pixelSize, '32_BIT_FLOAT')
            
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', tempgdb, "tempoverlap" + str(pid))
            arcpy.Delete_management(workdir)
            print("... Raster", outRast, "created in process " + str(pid)) 
            arcpy.CheckInExtension('3D')
        except Exception as e:
            print("... Error in process", pid, str(e))
            logfile = open(os.path.join(self.qcWorkPath, "heightDiffs" + rasterSuffix + "Log.txt"), 'a')
            logfile.write('Error occured in the process with PID ' + str(pid) + '\n')
            traceback.print_exc(file = logfile)
            methodName = 'prepro.checkRelativeHeightAccuracy' if rasterSuffix == '' else 'postpro.checkRelativeHeightAccuracyOn' + rasterSuffix
            logfile.write(methodName + '("' + indices[0] + '", "' + indices[1] + '", pixelSize = pixelSizeOfHDRasters)\n\n')
            logfile.close()
            if os.path.exists(os.path.join(self.qcWorkPath, "tempdir" + str(pid))):
                arcpy.Delete_management(os.path.join(self.qcWorkPath, "tempdir" + str(pid)))

    def extractLasEnvelopeToPolygonFC(self, lasName, outputName, workPath = None, lasDatasetName = 'temp.lasd', classCodeArr = [], shouldRemoveLasDataset = False):
        if workPath and os.path.exists(workPath):
            finalWorkPath = workPath
        else:
            finalWorkPath = self.qcWorkPath

        if not arcpy.Exists(os.path.join(finalWorkPath, lasDatasetName)):
            arcpy.CreateLasDataset_management(os.path.join(self.lasPath, lasName), os.path.join(finalWorkPath, lasDatasetName))
        else:
            arcpy.AddFilesToLasDataset_management(os.path.join(finalWorkPath, lasDatasetName), os.path.join(self.lasPath, lasName))
        
        print("... Generating the envelope of LAS area", lasName)
        arcpy.env.snapRaster = os.path.join(self.assetsPath, self.snapRasterDir, self.snapRasterRstTm34)

        samplingValue = 10
        if (len(classCodeArr) > 0):
            is000800Error = False
            arcpy.env.overwriteOutput = True
            try:
                lasDatasetLyr = arcpy.MakeLasDatasetLayer_management(os.path.join(finalWorkPath, lasDatasetName), "temp_lasd_lyr", classCodeArr)
            except Exception as e:
                if str(e).find('ERROR 000800') > -1: # The value is not a member of classes in current LAS dataset. The class has 0 points.
                    is000800Error = True
                    print('WARNING: provided class is empty. Empty LAS statistics raster will be created.')
                else:
                    raise(e)
            arcpy.env.overwriteOutput = False
            if is000800Error: # Create empty tempstats_i raster
                arcpy.CreateRasterDataset_management(arcpy.env.workspace, 'tempstats_i', samplingValue, '32_BIT_SIGNED')
            else:
                while True:
                    try:
                        arcpy.env.overwriteOutput = True
                        arcpy.LasPointStatsAsRaster_management(lasDatasetLyr, "tempstats_i", 'POINT_COUNT', 'CELLSIZE', samplingValue) # Sampling value 10 means cellsize 10m x 10m
                        arcpy.env.overwriteOutput = False
                        break
                    except Exception as e:
                        samplingValue = round(samplingValue / 2) if (samplingValue / 2) > 1 else samplingValue / 2
                        print('WARNING: LasPointStatsAsRaster_management has crashed. Trying smaller sampling value: ', samplingValue)
        else:
            while True:
                try:
                    arcpy.env.overwriteOutput = True
                    arcpy.LasPointStatsAsRaster_management(os.path.join(finalWorkPath, lasDatasetName), "tempstats_i", 'POINT_COUNT', 'CELLSIZE', samplingValue) # Sampling value 10 means cellsize 10m x 10m
                    arcpy.env.overwriteOutput = False
                    break
                except Exception as e:
                    samplingValue = round(samplingValue / 2) if (samplingValue / 2) > 1 else samplingValue / 2
                    print('WARNING: LasPointStatsAsRaster_management has crashed. Trying smaller sampling value: ', samplingValue)
        arcpy.RasterToPolygon_conversion("tempstats_i", "temppoly", 'NO_SIMPLIFY')
        arcpy.Dissolve_management('temppoly', str(outputName).rstrip(), "", "", 'MULTI_PART')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'tempstats_i')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temppoly')
        if shouldRemoveLasDataset:
            arcpy.RemoveFilesFromLasDataset_management(os.path.join(finalWorkPath, lasDatasetName), self.lasPath)
            os.remove(os.path.join(finalWorkPath, lasDatasetName))

    def mosaicHeightDiffsRastersWithMosaicDataset(self, rasterNamesToAdd=[], rasterSuffix=''):
        print("Mosaicking", rasterSuffix, "height differences rasters utilizing a mosaic dataset")
        
        heightDiffsGdb =  'height_diffs_' + self.lotNumStr + '.gdb' if rasterSuffix == '' else 'height_diffs_' + rasterSuffix + '_' + self.lotNumStr + '.gdb' 
        if not arcpy.Exists(os.path.join(self.qcWorkPath, heightDiffsGdb)):
            raise OSError(heightDiffsGdb + ' may not be found in ' + self.qcWorkPath)
        
        arcpy.env.workspace = os.path.join(self.qcWorkPath, heightDiffsGdb)
        arcpy.env.overwriteOutput = True
        
        if len(rasterNamesToAdd) > 0:
            inputRasters = []
            for rasterName in rasterNamesToAdd:
                if arcpy.Exists(rasterName):
                    print('... Raster', rasterName, 'exists in the GDB')
                    inputRasters.append(rasterName)
                else:
                    print('WARNING: Raster', rasterName, 'does not exist in the database', heightDiffsGdb)
        else:
            inputRasters = arcpy.ListRasters('height_diffs_' + rasterSuffix + '*')
        print('... Input rasters:', inputRasters)
        
        # Check if input rasters have the same cellsize and remove them with different cellsize
        cellSizeXStr = arcpy.GetRasterProperties_management(inputRasters[0], "CELLSIZEX").getOutput(0)
        prevCellSizeX = float(cellSizeXStr.replace(',', '.')) 
        removeRasters = []
        for raster in inputRasters[1:]:
            cellSizeXStr = arcpy.GetRasterProperties_management(raster, "CELLSIZEX").getOutput(0)
            currCellSizeX = float(cellSizeXStr.replace(',', '.'))
            if currCellSizeX != prevCellSizeX:
                print('... Warning: raster', raster, 'has different cell size', currCellSizeX, 'compared to the cell size of previous raster', prevCellSizeX)
                removeRasters.append(raster)
            else:
                prevCellSizeX = currCellSizeX
        [inputRasters.remove(raster) for raster in removeRasters]

        sr = arcpy.SpatialReference(3046)
        mosaicDatasetName = 'height_diffs' if rasterSuffix == '' else 'height_diffs_' + rasterSuffix
        if not arcpy.Exists(mosaicDatasetName):
            arcpy.CreateMosaicDataset_management(arcpy.env.workspace, mosaicDatasetName, sr, 1, '32_BIT_FLOAT', 'NONE')
        
        mosaicDataset = os.path.join(arcpy.env.workspace, mosaicDatasetName)
        print('... Adding rasters to mosaic dataset')
        arcpy.AddRastersToMosaicDataset_management(mosaicDataset, 'Raster Dataset', inputRasters, 'UPDATE_CELL_SIZES', 'UPDATE_BOUNDARY', 'NO_OVERVIEWS', -1, '', '', sr, '', 'NO_SUBFOLDERS', 'ALLOW_DUPLICATES', 'BUILD_PYRAMIDS', 'CALCULATE_STATISTICS', 'NO_THUMBNAILS', '', 'NO_FORCE_SPATIAL_REFERENCE')    
        extractedRasterName = 'height_diffs_maximum_mosaicked' if rasterSuffix == '' else 'height_diffs_' + rasterSuffix +'_maximum_mosaicked'
        print('... Creating raster', extractedRasterName)
        arcpy.SetMosaicDatasetProperties_management(mosaicDataset, mosaic_operator='MAX')
        arcpy.CopyRaster_management(mosaicDataset, os.path.join(self.qcWorkPath, self.qcGdb, extractedRasterName))
        extractedRasterName = 'height_diffs_minimum_mosaicked' if rasterSuffix == '' else 'height_diffs_' + rasterSuffix +'_minimum_mosaicked'
        print('... Creating raster', extractedRasterName)
        arcpy.SetMosaicDatasetProperties_management(mosaicDataset, mosaic_operator='MIN')
        arcpy.CopyRaster_management(mosaicDataset, os.path.join(self.qcWorkPath, self.qcGdb, extractedRasterName))
        print("Height differences rasters succesfully mosaicked")

    def checkIfAllHeightDiffsRastersCreated(self, rasterSuffix=''):
        print('Checking if there are all', rasterSuffix, 'height differences rasters created')
        heightDiffsGdb =  'height_diffs_' + self.lotNumStr + '.gdb' if rasterSuffix == '' else 'height_diffs_' + rasterSuffix + '_' + self.lotNumStr + '.gdb' 
        logFile = open(os.path.join(self.qcWorkPath, "heightDiffsCheckLog.txt"), 'w')
        if rasterSuffix == '':
            print('... Finding out unique overlaps in a prepro phase')
            overlaps_id = self.helper_findOutUniqueOverlaps('prepro')
        else:
            print('... Finding out unique overlaps in a postpro phase')
            overlaps_id = self.helper_findOutUniqueOverlaps('postpro')
        arcpy.env.workspace = os.path.join(self.qcWorkPath, heightDiffsGdb)
        missingRastersCount = 0
        hdRastNames = []

        for indices in overlaps_id:
            rasterBaseName = 'height_diffs_' if rasterSuffix == '' else 'height_diffs_' + rasterSuffix + '_'
            hdRastName = rasterBaseName + str(indices[0]).replace("{", "").replace("}", "").replace("-","_") + "__" + str(indices[1]).replace("{", "").replace("}", "").replace("-","_")
            
            if not arcpy.Exists(hdRastName):
                print('...', hdRastName, 'does not exist')
                # methodName = 'prepro.checkRelativeHeightAccuracy' if rasterSuffix == '' else 'postpro.checkRelativeHeightAccuracyOn' + rasterSuffix
                phaseName = 'prepro' if rasterSuffix == '' else 'postpro'
                logFile.write("{'fName': misc.helper_hdiff_multiproc, 'kwargs': {'indices': " + str(indices) + ", 'phaseName': '" + phaseName + "', 'rasterSuffix': rasterSuffix, 'pixelSize': pixelSizeOfHDRasters}},\n")
                hdRastNames.append(hdRastName)
                missingRastersCount += 1
        logFile.write(str(hdRastNames))
        logFile.close()

        if missingRastersCount == 0:
            os.remove(os.path.join(self.qcWorkPath, "heightDiffsCheckLog.txt"))
        print('Checking done. There are', missingRastersCount, 'missing height diffs rasters.')

    def checkRelativeHorizontalAccuracyOnBuildings(self, tileSize, coresCount, relevanceConstraint = 75, clusterTolerance = 0.5, shouldClassifyBuildings = False, customControlAreaPolySrc = None, returnProcessFromFid = 0):
        print('Checking relative horizontal accuracy of LAS files based on buildings')
        self.prepareFlathouseVerticesFromLasAndZbgis(tileSize, coresCount, shouldClassifyBuildings, 0, [], customControlAreaPolySrc, False, returnProcessFromFid)
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        buildingPolyFcs = arcpy.ListFeatureClasses('ha_flathouses_las_polygons*', 'POLYGON')
        print('... Union')
        arcpy.Union_analysis(buildingPolyFcs, 'temp_buildings_union', 'ONLY_FID', '', 'GAPS')
        print('... Dissolve')
        arcpy.Dissolve_management('temp_buildings_union', 'temp_buildings_diss', '', '', 'SINGLE_PART')
        arcpy.AddField_management('temp_buildings_union', 'LAS_count', 'SHORT')
        arcpy.AddField_management('temp_buildings_union', 'MAX_LAS_count', 'SHORT')
        arcpy.AddField_management('temp_buildings_union', 'relevance', 'FLOAT')
        arcpy.AddField_management('temp_buildings_union', 'filenames', 'TEXT', '', '', 4096)
        fields = [field.name for field in arcpy.ListFields('temp_buildings_union', 'FID_*')]
        editor = arcpy.da.Editor(arcpy.env.workspace)
        editor.startEditing()
        editor.startOperation()
        print('... Adding LAS filenames to attributes')
        uc = arcpy.da.UpdateCursor('temp_buildings_union', ['LAS_count', 'filenames'] + fields)
        editedFieldCnt = 2 # based on the number of updated fields
        for row in uc:
            lasCnt = 0
            filenames = ''
            for i in range(editedFieldCnt, len(row)):
                if row[i] > -1:
                    lasCnt += 1
                    filenames += fields[i - editedFieldCnt].split('ha_flathouses_las_polygons_')[1] + '.las, '
            filenames = filenames[:-2] # removing of the last comma
            row[0] = lasCnt
            row[1] = filenames
            uc.updateRow(row)
        del uc

        editor.stopOperation()
        editor.stopEditing(True)
        arcpy.DeleteField_management('temp_buildings_union', fields)
        editor.startEditing()
        editor.startOperation()

        print('... Calculating relevance')
        arcpy.MakeFeatureLayer_management('temp_buildings_union', 'temp_buildings_union_lyr')
        sc = arcpy.da.SearchCursor('temp_buildings_diss', ['SHAPE@'])
        for scrow in sc:
            arcpy.SelectLayerByLocation_management('temp_buildings_union_lyr', 'INTERSECT', scrow[0], '', 'NEW_SELECTION')
            uc = arcpy.da.UpdateCursor('temp_buildings_union_lyr', ['LAS_count', 'MAX_LAS_count', 'relevance'])
            lasCountMax = 0
            for ucrow in uc:
                lasCountMax = ucrow[0] if ucrow[0] > lasCountMax else lasCountMax
            uc.reset()
            for ucrow in uc:
                ucrow[1] = lasCountMax
                ucrow[2] = 100 * ucrow[0] / lasCountMax
                uc.updateRow(ucrow)
        del uc, sc
        editor.stopOperation()
        editor.stopEditing(True)
        del editor
        arcpy.Copy_management('temp_buildings_union', 'building_parts_' + self.lotNumStr)

        print('... Retaining building polygon parts with a relevance smaller than ' + str(relevanceConstraint))
        where = 'relevance >= ' + str(relevanceConstraint)
        arcpy.SelectLayerByAttribute_management('temp_buildings_union_lyr', 'NEW_SELECTION', where)
        arcpy.DeleteFeatures_management('temp_buildings_union_lyr')
        arcpy.Dissolve_management('temp_buildings_union', 'temp_building_strips', '', '', 'SINGLE_PART')

        print('... Generalizing sliver polygons')
        arcpy.Copy_management('temp_building_strips', 'temp_building_strips_integrate')
        arcpy.Integrate_management('temp_building_strips_integrate', clusterTolerance)
        arcpy.Clip_analysis('temp_building_strips_integrate', 'temp_building_strips', 'temp_building_raw_inconsistences')
        arcpy.MultipartToSinglepart_management('temp_building_raw_inconsistences', 'temp_building_sp_inconsistences')
        where = 'Shape_Area >= 5' 
        arcpy.Select_analysis('temp_building_sp_inconsistences', 'temp_building_inconsistences', where)

        print('... Adding LAS filenames information to polygons')
        # Getting name of las_attributes feature class according to the las file endian
        files = [las for las in os.listdir(self.lasPath) if las.endswith('.las')]
        for las in files:
            if las.endswith('_r.las'):
                lasAttributesFc = 'las_attributes_prepro_' + self.lotNumStr
                break
            elif las.endswith('_c.las'):
                lasAttributesFc = 'las_attributes_postpro_' + self.lotNumStr
                break
        fieldMapping = arcpy.FieldMappings()
        fieldMapping.addTable('temp_building_inconsistences')
        filenameFieldMap = arcpy.FieldMap()
        filenameFieldMap.addInputField(lasAttributesFc, 'filename')
        filenameFieldMap.mergeRule = 'join'
        filenameFieldMap.joinDelimiter = ', '
        filenameField = filenameFieldMap.outputField
        filenameField.length = 4096
        filenameField.name = 'filenames'
        filenameFieldMap.outputField = filenameField
        fieldMapping.addFieldMap(filenameFieldMap)
        arcpy.SpatialJoin_analysis('temp_building_inconsistences', lasAttributesFc, 'building_inconsistences_' + self.lotNumStr, 'JOIN_ONE_TO_ONE', 'KEEP_ALL', fieldMapping, 'INTERSECT')

        # Cleaning workspace
        buildingPntFcs = arcpy.ListFeatureClasses('ha_flathouses_las_vertices*', 'POINT')
        for buildingPntFc in buildingPntFcs:
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, buildingPntFc)
        for buildingPolyFc in buildingPolyFcs:
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, buildingPolyFc)
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_buildings_union_lyr')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_buildings_union')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_buildings_diss')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_building_strips_integrate')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_building_strips')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_building_sp_inconsistences')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_building_raw_inconsistences')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'temp_building_inconsistences')
        print('Checking relative horizontal accuracy of LAS files based on buildings done')

    def prepareProjectedRasterWithAndWithoutSnapRaster(self, rasterSrc, interestedAreaPolygonName, shouldConvertAscToEsriGrid, shouldUseSnapRaster, snapRasterSrc = None):
        print('Preparing projected raster from ETRS89-TM34 to S-JTSK (JTSK3) with and without snap raster')
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        arcpy.env.snapRaster = None

        if shouldUseSnapRaster:
            if snapRasterSrc and os.path.exists(snapRasterSrc):
                arcpy.env.snapRaster = snapRasterSrc
            else:
                arcpy.env.snapRaster = os.path.join(self.assetsPath, self.snapRasterDir, self.snapRasterRstTm34)
        else:
            arcpy.env.snapRaster = None

        if shouldConvertAscToEsriGrid:
            print('... Converting raster from ASC to ESRI GRID')
            arcpy.RasterToOtherFormat_conversion(rasterSrc, arcpy.env.workspace)
            rasterSrc = os.path.join(arcpy.env.workspace, os.path.basename(rasterSrc).split('.')[0])

        arcpy.env.snapRaster = None
        inCrs = arcpy.SpatialReference(3046)
        outCrs = arcpy.SpatialReference(8353)
        print('... Projecting without snap raster')
        arcpy.ProjectRaster_management(rasterSrc, 'out_jtsk03_no_snap_' + self.lotNumStr, outCrs, 'NEAREST', '1 METER', 'ETRS89doJTSK03', '', inCrs)
        
        desc = arcpy.Describe(interestedAreaPolygonName).Extent
        envelope = str(round(desc.XMin)) + ' ' + str(round(desc.YMin)) + ' ' + str(round(desc.XMax)) + ' ' + str(round(desc.YMax))
        print('... Clipping do not snapped raster by interested area polygon')
        arcpy.Clip_management('out_jtsk03_no_snap_' + self.lotNumStr, envelope, 'interested_jtsk03_no_snap_' + self.lotNumStr, interestedAreaPolygonName, '', 'ClippingGeometry')

        print('... Converting raster no snap to points')
        arcpy.RasterToPoint_conversion('interested_jtsk03_no_snap_' + self.lotNumStr, 'interested_pnts_jtsk03_no_snap_' + self.lotNumStr)

        print('... Creating hillshade from raster no snap')
        arcpy.HillShade_3d('interested_jtsk03_no_snap_' + self.lotNumStr, 'hillshade_jtsk03_no_snap_' + self.lotNumStr, 315, 45, 'NO_SHADOWS', 1)
        print('Projected rasters prepared')

    def prepareRasterForSnappingAnalysis(self, rasterSrc, interestedAreaPolygonName, nameSuffix, shouldConvertAscToEsriGrid, shouldUseSnapRaster, snapRasterSrc = None):
        print('Preparing raster for snapping analysis')
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)

        if shouldUseSnapRaster:
            if snapRasterSrc and os.path.exists(snapRasterSrc):
                arcpy.env.snapRaster = snapRasterSrc
            else:
                arcpy.env.snapRaster = os.path.join(self.assetsPath, self.snapRasterDir, self.snapRasterRstTm34)
        else:
            arcpy.env.snapRaster = None

        if shouldConvertAscToEsriGrid:
            print('... Converting raster from ASC to ESRI GRID')
            arcpy.RasterToOtherFormat_conversion(rasterSrc, arcpy.env.workspace)
            rasterSrc = os.path.join(arcpy.env.workspace, os.path.basename(rasterSrc).split('.')[0])
        
        arcpy.env.snapRaster = None
        desc = arcpy.Describe(interestedAreaPolygonName).Extent
        envelope = str(round(desc.XMin)) + ' ' + str(round(desc.YMin)) + ' ' + str(round(desc.XMax)) + ' ' + str(round(desc.YMax))
        print('... Clipping raster by interested area polygon')
        arcpy.Clip_management(rasterSrc, envelope, 'interested_' + nameSuffix + '_' + self.lotNumStr, interestedAreaPolygonName, '', 'ClippingGeometry')

        print('... Converting raster to points')
        arcpy.RasterToPoint_conversion('interested_' + nameSuffix + '_' + self.lotNumStr, 'interested_pnts_' + nameSuffix + '_' + self.lotNumStr)

        print('... Creating hillshade')
        arcpy.HillShade_3d('interested_' + nameSuffix + '_' + self.lotNumStr, 'hillshade_' + nameSuffix + '_' + self.lotNumStr, 315, 45, 'NO_SHADOWS', 1)
        
        print('Preparing raster for snapping analysis done')

    def mosaicRastersForSnappingAnalysis(self, qcGdbSrc1, qcGdbSrc2):
        print('Mosaicking rasters for snapping analysis')
        arcpy.env.workspace = qcGdbSrc1
        inRaster1Name = [raster for raster in arcpy.ListRasters('interested_*')][0]
        print('... inRaster1Name', inRaster1Name)
        arcpy.env.workspace = qcGdbSrc2
        inRaster2Name = [raster for raster in arcpy.ListRasters('interested_*')][0]
        print('... inRaster2Name', inRaster2Name)
        rastersToMosaic = [os.path.join(qcGdbSrc1, inRaster1Name), os.path.join(qcGdbSrc2, inRaster2Name)]
        print('... Mosaicking rasters to', qcGdbSrc1)
        try:
            arcpy.MosaicToNewRaster_management(rastersToMosaic, qcGdbSrc1, inRaster1Name + '_' + inRaster2Name, '', '32_BIT_FLOAT', 1, 1, 'MEAN')
        except Exception as e:
            traceback.print_exc(file = sys.stdout)
        rastersToMosaic = [os.path.join(qcGdbSrc2, inRaster2Name), os.path.join(qcGdbSrc1, inRaster1Name)]
        print('... Mosaicking rasters to', qcGdbSrc2)
        try:
            arcpy.MosaicToNewRaster_management(rastersToMosaic, qcGdbSrc2, inRaster2Name + '_' + inRaster1Name, '', '32_BIT_FLOAT', 1, 1, 'MEAN')
        except Exception as e:
            traceback.print_exc(file = sys.stdout)
        rasterToHillshadeSrc1 = os.path.join(qcGdbSrc1, inRaster1Name + '_' + inRaster2Name)
        hillshadeOutSrc1 = os.path.join(qcGdbSrc1, 'hillshade_' + inRaster1Name + '_' + inRaster2Name)
        print('... Hillshading of mosaicked raster in', qcGdbSrc1)
        arcpy.HillShade_3d(rasterToHillshadeSrc1, hillshadeOutSrc1, 315, 45, 'NO_SHADOWS', 1)
        rasterToHillshadeSrc2 = os.path.join(qcGdbSrc2, inRaster2Name + '_' + inRaster1Name)
        hillshadeOutSrc2 = os.path.join(qcGdbSrc2, 'hillshade_' + inRaster2Name + '_' + inRaster1Name)
        print('... Hillshading of mosaicked raster in', qcGdbSrc2)
        arcpy.HillShade_3d(rasterToHillshadeSrc2, hillshadeOutSrc2, 315, 45, 'NO_SHADOWS', 1)
        print('Mosaicking and hillshading done')
    
    def moveHeightDiffsRastersToSeparateGdb(self, rasterSuffix = '', shouldOnlyDeleteRasters = False):
        heightDiffsGdb =  'height_diffs_' + self.lotNumStr + '.gdb' if rasterSuffix == '' else 'height_diffs_' + rasterSuffix + '_' + self.lotNumStr + '.gdb'
        if shouldOnlyDeleteRasters:
            print('Deleting height diffs rasters from', self.qcGdb)
        else:
            print('Moving height diffs rasters from', self.qcGdb, 'to', heightDiffsGdb)


        if not shouldOnlyDeleteRasters and not arcpy.Exists(os.path.join(self.qcWorkPath, heightDiffsGdb)):
            arcpy.CreateFileGDB_management(self.qcWorkPath, heightDiffsGdb, 'CURRENT')
        
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        rastersAll = arcpy.ListRasters('height_diffs_' + rasterSuffix + '*')
        rasters = [raster for raster in rastersAll if raster.find('mosaicked') == -1]
        mosaicDatasetName = 'height_diffs' if rasterSuffix == '' else 'height_diffs_' + rasterSuffix

        if not shouldOnlyDeleteRasters:
            arcpy.env.workspace = os.path.join(self.qcWorkPath, heightDiffsGdb)
            arcpy.env.overwriteOutput = True

        if arcpy.Exists(os.path.join(self.qcWorkPath, self.qcGdb, mosaicDatasetName)):
            if not shouldOnlyDeleteRasters:
                print('... Moving mosaic dataset', mosaicDatasetName)
                arcpy.Copy_management(os.path.join(self.qcWorkPath, self.qcGdb, mosaicDatasetName), mosaicDatasetName)
            else:
                print('... Deleting mosaic dataset', mosaicDatasetName)
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', os.path.join(self.qcWorkPath, self.qcGdb), mosaicDatasetName)
        else:
            print('... WARNING: mosaic dataset does not exist in the path', os.path.join(self.qcWorkPath, self.qcGdb))

        rasterCount = len(rasters)
        i = 1
        if rasterCount == 0:
            print('... WARNING: There is no height diffs raster', rasterSuffix, 'in the path', os.path.join(self.qcWorkPath, self.qcGdb))
        for raster in rasters:
            if not shouldOnlyDeleteRasters:
                print('... Moving raster', i, '/', rasterCount, raster)
                arcpy.Copy_management(os.path.join(self.qcWorkPath, self.qcGdb, raster), raster)
            else:
                print('... Deleting raster', i, '/', rasterCount, raster)
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', os.path.join(self.qcWorkPath, self.qcGdb), raster)
            i += 1
        
        if shouldOnlyDeleteRasters:
            print('Height diffs rasters successfully deleted from', self.qcGdb)
        else:
            print('Height diffs rasters successfully moved from', self.qcGdb, 'to', heightDiffsGdb)

    @staticmethod
    def writeCellValuesToPointAttributes(inFeaturesSrc, inRasterSrc, toAttributeName = 'grid_code'):
        print('Writing cell values of raster', inRasterSrc, 'to features', inFeaturesSrc)
        if not arcpy.Exists(inFeaturesSrc):
            raise Exception('Input features does not exist.')
        if not arcpy.Exists(inRasterSrc):
            raise Exception('Input raster does not exist.')

        try:
            arcpy.AddField_management(inFeaturesSrc, toAttributeName, 'FLOAT')
            print('... Field', toAttributeName, 'added')
        except Exception as e:
            print(e)
        
        uc = arcpy.da.UpdateCursor(inFeaturesSrc, ['SHAPE@', toAttributeName])
        for row in uc:
            pntText = str(row[0].getPart().X) + ' ' + str(row[0].getPart().Y)
            print('... pntText', pntText)
            cellValueRaw = arcpy.GetCellValue_management(inRasterSrc, pntText)
            cellValue = float(str(cellValueRaw).replace(',', '.'))
            print('... cellValue', cellValue)
            row[1] = cellValue
            uc.updateRow(row)
        del uc
        print('Process succesfully done')

    @staticmethod
    def defineNewLasDimension(inLasSrc, dimensionName, dataType, dimensionDescription, defaultValue):
        print('... Defining new LAS dimension', dimensionName, 'to the LAS file', inLasSrc)
        if not os.path.exists(inLasSrc):
            raise IOError('Provided LAS file does not exist')
        path = os.path.dirname(inLasSrc)
        fileName = os.path.basename(inLasSrc)
        os.rename(inLasSrc, os.path.join(path, 'temp_original.las'))
        inLasSrc = os.path.join(path, 'temp_original.las')

        origLasFile = laspy.file.File(inLasSrc, mode = 'r')
        newLasFile = laspy.file.File(os.path.join(path, fileName), mode = 'w', header = origLasFile.header)
        newLasFile.define_new_dimension(name = dimensionName, data_type = dataType, description = dimensionDescription)
        
        # Copy all other dimensions data
        for dimension in origLasFile.point_format:
            dimensionData = origLasFile.reader.get_dimension(dimension.name)
            newLasFile.writer.set_dimension(dimension.name, dimensionData)
        
        # Set default values to the new dimension
        defaultValues = np.full(dimensionData.shape, defaultValue)
        newLasFile.writer.set_dimension(dimensionName, defaultValues)

        origLasFile.close()
        newLasFile.close()
        os.remove(inLasSrc)
        print('... New LAS dimension', dimensionName, 'defined')

    @staticmethod
    def shortestDistanceFromPlane(coordsXYZArr, a, b, c, d):  
        nominator = abs((a * coordsXYZArr[0] + b * coordsXYZArr[1] + c * coordsXYZArr[2] + d))  
        denominator = (math.sqrt(a * a + b * b + c * c)) 
        return nominator / denominator # which is the shortest distance from plane

    @staticmethod
    def getFittedPlane(pointsNpArr): # pointsNpArr is XYZ numpy array
        (rows, cols) = pointsNpArr.shape
        G = np.ones((rows, 3))
        G[:, 0] = pointsNpArr[:, 0]  #X
        G[:, 1] = pointsNpArr[:, 1]  #Y
        Z = pointsNpArr[:, 2]
        (a, b, c), resid, rank, s = np.linalg.lstsq(G, Z)
        normal = (a, b, -1)
        nn = np.linalg.norm(normal)
        normal = normal / nn
        point = np.array([0.0, 0.0, c])
        d = -point.dot(normal)
        return { # a, b, c, d as a plane coeficients
            'a': normal[0],
            'b': normal[1],
            'c': normal[2],
            'd': d
        }

    @staticmethod
    def setLasFlagsToZero(inLasSrc, flagCommands = ['-set_withheld_flag', '-set_synthetic_flag', '-set_keypoint_flag', '-set_overlap_flag']):
        print('... Setting LAS flags to zero')
        if not os.path.exists(inLasSrc):
            raise IOError('Provided LAS file does not exist')
        path = os.path.dirname(inLasSrc)
        lasName = os.path.basename(inLasSrc)

        for flagCommand in flagCommands:
            os.rename(inLasSrc, os.path.join(path, 'temp_orig.las'))
            inLasSrc = os.path.join(path, 'temp_orig.las')
            subprocess.run(["las2las64", "-i", inLasSrc, flagCommand, "0", "-o", os.path.join(path, lasName)])
            inLasSrc = os.path.join(path, lasName)
            os.remove(os.path.join(path, 'temp_orig.las'))    
        print('... LAS flags set to zero')

    @staticmethod
    def setFlagOfOverlappingLASPoints(flagCommand, inLasSrc, overlapShpSrc):
        print('... Setting LAS flag ', flagCommand, 'to points which overlap', overlapShpSrc)
        if not os.path.exists(inLasSrc):
            raise IOError('Provided LAS file does not exist')
        flagCommands = ['-flag_as_withheld', '-flag_as_synthetic', '-flag_as_keypoint', '-flag_as_overlap']
        if not flagCommand in flagCommands:
            raise Exception('Provided flag command ' + flagCommand + ' cannot be used')
        path = os.path.dirname(inLasSrc)
        lasName = os.path.basename(inLasSrc)

        os.rename(inLasSrc, os.path.join(path, 'temp_orig.las'))
        inLasSrc = os.path.join(path, 'temp_orig.las')
        subprocess.run(['lasclip64', '-i', inLasSrc, '-poly', overlapShpSrc, flagCommand, '-interior', '-o', os.path.join(path, lasName)], shell = True)
        # subprocess.run(['lasclip64', '-i', os.path.join(path, lasName), '-poly', overlapShpSrc, '-o', os.path.join(path, 'sel_' + lasName)], shell = True) # todo delete
        inLasSrc = os.path.join(path, lasName)
        os.remove(os.path.join(path, 'temp_orig.las'))    
        print('... LAS flag set')
        
    @staticmethod
    def getXYZNpArrFromShapeJson(shapeJsonStr):
        XYZ = np.empty((0,3), dtype = 'float')
        jsonObj = json.loads(shapeJsonStr)
        for outer in jsonObj['rings']:
            for inner in outer:
                currXYZ = np.array([[inner[0], inner[1], inner[2]]])
                XYZ = np.concatenate((XYZ, currXYZ))
        print('... XYZ array from SHAPE@JSON', XYZ)
        return XYZ

    def extractPolygonVerticesToFile(self, inPolyFcSrc, objectId, outFileSrc):
        print('Extracting polygon', os.path.basename(inPolyFcSrc) , 'vertices to file', outFileSrc)
        if not os.path.exists(inPolyFcSrc):
            raise IOError('Input polygon feature class does not exist')
        outFile = open(outFileSrc, 'w')

        OIDFieldName = arcpy.Describe(inPolyFcSrc).OIDFieldName
        where = OIDFieldName + ' = ' + str(objectId)
        sc = arcpy.da.SearchCursor(inPolyFcSrc, [OIDFieldName, 'SHAPE@JSON'], where)
        XYZNpArr = np.empty((0, 3))
        for row in sc:
            XYZNpArr = self.getXYZNpArrFromShapeJson(row[1])

        if XYZNpArr.size > 0:
            for row in XYZNpArr:
                outFile.write('{};{};{}\n'.format(row[0], row[1], row[2]))
        outFile.close()

    def createShpFootprintOfLasClass(self, inLasSrc, outShpSrc, classArr: list):
        print('... Creating SHP footprint of class/-es', classArr, 'from', inLasSrc)
        if not os.path.exists(inLasSrc):
            raise IOError('Provided LAS file does not exist')
        if not os.path.exists(os.path.dirname(outShpSrc)):
            raise IOError('Path', os.path.dirname(outShpSrc), 'where to create new SHP does not exist')
        if arcpy.Exists(outShpSrc):
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', os.path.dirname(outShpSrc), os.path.basename(outShpSrc))
        
        if len(classArr) == 0:
            raise Exception('Empty class array provided')
        classArrStr = str(classArr)[1:-1].replace(',', '')
        subprocess.run(["lasboundary64", "-i", inLasSrc, "-o", outShpSrc, "-concavity", "2", "-keep_class", classArrStr, "-disjoint", "-v"], shell=True)
    
    @staticmethod
    def makeBackupOfLas(inLasSrc):
        print('... Making backup of LAS', inLasSrc)
        if not os.path.exists(inLasSrc):
            raise IOError('Provided LAS file', os.path.basename(inLasSrc) , 'does not exist')
        backupPath = os.path.join(os.path.dirname(inLasSrc), 'backup')
        if not os.path.exists(backupPath):
            os.mkdir(backupPath)
        if not os.path.exists(os.path.join(backupPath, os.path.basename(inLasSrc))):
            shutil.copyfile(inLasSrc, os.path.join(backupPath, os.path.basename(inLasSrc)))
        else:
            print('... WARNING: backup of LAS file', os.path.basename(inLasSrc), 'already exists')
    
    @staticmethod
    def removeBackupOfLas(inLasSrc):
        print('... Removing backup of LAS', inLasSrc)
        backupLasSrc = os.path.join(os.path.dirname(inLasSrc), 'backup', os.path.basename(inLasSrc))
        if os.path.exists(backupLasSrc):
            os.remove(backupLasSrc)
        if len(os.listdir(os.path.dirname(backupLasSrc))) == 0:
            shutil.rmtree(os.path.dirname(backupLasSrc))
    
    @staticmethod
    def restoreBackupOfLas(inLasSrc):
        print('... Restoring backup of LAS', inLasSrc)
        backupLasSrc = os.path.join(os.path.dirname(inLasSrc), 'backup', os.path.basename(inLasSrc))
        if os.path.exists(backupLasSrc):
            shutil.move(backupLasSrc, inLasSrc)
        else:
            print('... Backup of LAS does not exist')
        if os.path.exists(os.path.dirname(backupLasSrc)) and len(os.listdir(os.path.dirname(backupLasSrc))) == 0:
            shutil.rmtree(os.path.dirname(backupLasSrc))
    
    @staticmethod
    def removeUnnecessaryDirectory(path):
        print('... Removing unnecessary directory path', path)
        if os.path.exists(path):
            shutil.rmtree(path)
        else:
            print('... Provided path does not exist')
    
    def clipOrthophotoByPolygon(self, clipPolygonSrc, outSrc, isPolyEtrs89Tm34 = True):
        print('... Clipping orthophoto by polygon', os.path.basename(clipPolygonSrc))
        if not arcpy.Exists(clipPolygonSrc):
            raise IOError('Clip polygon', os.path.basename(clipPolygonSrc), 'does not exist')
        if not arcpy.Exists(con.ORTHOPHOTO_JTSK03_SRC):
            raise IOError('No orthophoto found on', con.ORTHOPHOTO_JTSK03_SRC)
        if not os.path.exists(os.path.dirname(outSrc)):
            os.makedirs(os.path.dirname(outSrc))

        if isPolyEtrs89Tm34:
            if not os.path.exists(os.path.join(os.path.dirname(outSrc), 'temp')):
                os.mkdir(os.path.join(os.path.dirname(outSrc), 'temp'))
            self.defineTransformationGrs80ToBessel1841()
            self.defineTransformationBessel1841ToGrs80_for5514()
            
            jtsk03ShpSrc = os.path.join(os.path.dirname(outSrc), 'temp', 'clipPolyJtsk03.shp')
            try:
                arcpy.Project_management(
                    clipPolygonSrc,
                    jtsk03ShpSrc, 
                    arcpy.SpatialReference(8353), 
                    con.TRANSFORMATION_GRS80_TO_BESSEL1841, 
                    # 'S-JTSK_[JTSK03]_To_ETRS_1989_1',
                    arcpy.SpatialReference(3046)
                )
            except:
                print('... WARNING: Exception was thrown, but trying another way of vector transformation\n')
                arcpy.Project_management(
                    clipPolygonSrc,
                    jtsk03ShpSrc, 
                    arcpy.SpatialReference(8353), 
                    'S-JTSK_[JTSK03]_To_ETRS_1989_1',
                    arcpy.SpatialReference(3046)
                )
            
            desc = arcpy.Describe(jtsk03ShpSrc).Extent
            envelope = str(round(desc.XMin)) + ' ' + str(round(desc.YMin)) + ' ' + str(round(desc.XMax)) + ' ' + str(round(desc.YMax))
            rastJtsk03Src = os.path.join(os.path.dirname(outSrc), 'temp', 'jtsk03Clip.tif')
            arcpy.Clip_management(con.ORTHOPHOTO_JTSK03_SRC, envelope, rastJtsk03Src, '', '')
            
            cellSizeXStr = arcpy.GetRasterProperties_management(con.ORTHOPHOTO_JTSK03_SRC, "CELLSIZEX").getOutput(0)
            cellSizeX = float(cellSizeXStr.replace(',', '.'))   
            try:
                arcpy.ProjectRaster_management(
                    rastJtsk03Src, 
                    outSrc, 
                    arcpy.SpatialReference(3046), 
                    'NEAREST', 
                    cellSizeX, 
                    con.TRANSFORMATION_BESSEL1841_TO_GRS80_FOR_EPSG_5514, 
                    '', 
                    arcpy.SpatialReference(5514) # ortho is jtsk03, but EPSG 5514 
                )
            except:
                print('... WARNING: Exception was thrown, but trying another way of raster transformation\n')
                self.workaroundProjectRasterFromJtsk03ToTm34(rastJtsk03Src, outSrc, cellSizeX)
            shutil.rmtree(os.path.join(os.path.dirname(outSrc), 'temp'))
        else:
            desc = arcpy.Describe(clipPolygonSrc).Extent
            envelope = str(round(desc.XMin)) + ' ' + str(round(desc.YMin)) + ' ' + str(round(desc.XMax)) + ' ' + str(round(desc.YMax))
            arcpy.Clip_management(con.ORTHOPHOTO_JTSK03_SRC, envelope, outSrc, '', '')
    
    def workaroundProjectRasterFromJtsk03ToTm34(self, inSrc, outSrc, cellSizeX):
        arcpy.CreateFileGDB_management(os.path.dirname(outSrc), 'temp.gdb', 'CURRENT')
        print('... Raster to GDB, in raster src:', inSrc, ', out GDB:', os.path.join(os.path.dirname(outSrc), 'temp.gdb'))
        print('... In raster src exists?', arcpy.Exists(inSrc))
        # res = arcpy.conversion.RasterToGeodatabase(inSrc, os.path.join(os.path.dirname(outSrc), 'temp.gdb'))
        newInSrc = os.path.join(os.path.dirname(outSrc), 'temp.gdb', os.path.basename(inSrc).split('.')[0])
        res = arcpy.management.CopyRaster(inSrc, newInSrc)
        print('... Result of Raster to GDB is', res)
        print('... New raster src is', newInSrc,', and exists?', arcpy.Exists(newInSrc))
        tempOutSrc = os.path.join(os.path.dirname(outSrc), 'temp.gdb', os.path.basename(outSrc).split('.')[0])
        arcpy.DefineProjection_management(newInSrc, arcpy.SpatialReference(8353))
        arcpy.ProjectRaster_management(
            newInSrc, 
            tempOutSrc, 
            arcpy.SpatialReference(3046), 
            'NEAREST', 
            cellSizeX, 
            'ETRS_1989_To_S-JTSK_[JTSK03]_1', 
            '', 
            arcpy.SpatialReference(8353)
        )
        arcpy.management.CopyRaster(tempOutSrc, outSrc)
        arcpy.Delete_management(os.path.join(os.path.dirname(outSrc), 'temp.gdb'))

    @staticmethod
    def defineTransformationGrs80ToBessel1841():
        print('... Defining custom geographic transformation from GRS80 to Bessel 1841')
        jtsk03GcsSrs = arcpy.SpatialReference(8351)
        tm34GcsSrs = arcpy.SpatialReference(4258)
        arcpy.env.overwriteOutput = True
        customGeoTransfm = 'GEOGTRAN[METHOD["Coordinate_Frame"],PARAMETER["X_Axis_Translation",-485.014],PARAMETER["Y_Axis_Translation",-169.474],PARAMETER["Z_Axis_Translation",-483.843],PARAMETER["X_Axis_Rotation",7.78625453],PARAMETER["Y_Axis_Rotation",4.39770887],PARAMETER["Z_Axis_Rotation",4.10248899],PARAMETER["Scale_Difference",0.0]]'
        try:
            arcpy.CreateCustomGeoTransformation_management(con.TRANSFORMATION_GRS80_TO_BESSEL1841, tm34GcsSrs, jtsk03GcsSrs, customGeoTransfm)
        except Exception as e:
            print('WARNING:', e)
        arcpy.env.overwriteOutput = False

    @staticmethod
    def defineTransformationBessel1841ToGrs80():
        print('... Defining custom geographic transformation from Bessel 1841 to GRS80')
        jtsk03GcsSrs = arcpy.SpatialReference(8351)
        tm34GcsSrs = arcpy.SpatialReference(4258)
        arcpy.env.overwriteOutput = True
        customGeoTransfm = 'GEOGTRAN[METHOD["Coordinate_Frame"],PARAMETER["X_Axis_Translation",485.021],PARAMETER["Y_Axis_Translation",169.465],PARAMETER["Z_Axis_Translation",483.839],PARAMETER["X_Axis_Rotation",-7.786342],PARAMETER["Y_Axis_Rotation",-4.397554],PARAMETER["Z_Axis_Rotation",-4.102655],PARAMETER["Scale_Difference",0.0]]'
        try:
            arcpy.CreateCustomGeoTransformation_management(con.TRANSFORMATION_BESSEL1841_TO_GRS80, jtsk03GcsSrs, tm34GcsSrs, customGeoTransfm)
        except Exception as e:
            print('WARNING:', e)
        arcpy.env.overwriteOutput = False

    @staticmethod
    def defineTransformationBessel1841ToGrs80_for5514():
        print('... Defining custom geographic transformation from Bessel 1841 to GRS80 for EPSG 5514')
        jtsk03GcsSrs = arcpy.SpatialReference(4156)
        tm34GcsSrs = arcpy.SpatialReference(4258)
        arcpy.env.overwriteOutput = True
        customGeoTransfm = 'GEOGTRAN[METHOD["Coordinate_Frame"],PARAMETER["X_Axis_Translation",485.021],PARAMETER["Y_Axis_Translation",169.465],PARAMETER["Z_Axis_Translation",483.839],PARAMETER["X_Axis_Rotation",-7.786342],PARAMETER["Y_Axis_Rotation",-4.397554],PARAMETER["Z_Axis_Rotation",-4.102655],PARAMETER["Scale_Difference",0.0]]'
        try:
            arcpy.CreateCustomGeoTransformation_management(con.TRANSFORMATION_BESSEL1841_TO_GRS80_FOR_EPSG_5514, jtsk03GcsSrs, tm34GcsSrs, customGeoTransfm)
        except Exception as e:
            print('WARNING:', e)
        arcpy.env.overwriteOutput = False

    @staticmethod
    def defineTransformationJtskToJtsk03Nadcon():
        print('... Defining custom geographic transformation from JTSK03 to JTSK realisation by NADCON grid')
        jtsk03Srs = arcpy.SpatialReference(8353)
        jtskSrs = arcpy.SpatialReference(5514)
        arcpy.env.overwriteOutput = True
        installInfo = arcpy.GetInstallInfo()
        if installInfo['ProductName'] != 'ArcGISPro':
            raise Exception('This tool works only on ArcGIS Pro')
        inNadconLasSrc = os.path.join(con.ASSETS_PATH, con.SHIFT_GRID_DIR, con.JTSK_TO_JTSK03_SHIFT_GRID_NADCON_NAME + '.LAS')
        inNadconLosSrc = os.path.join(con.ASSETS_PATH, con.SHIFT_GRID_DIR, con.JTSK_TO_JTSK03_SHIFT_GRID_NADCON_NAME + '.LOS')
        outNadconLasSrc = os.path.join(installInfo['InstallDir'], 'Resources', 'pedata', 'nadcon', con.JTSK_TO_JTSK03_SHIFT_GRID_NADCON_NAME + '.LAS')
        outNadconLosSrc = os.path.join(installInfo['InstallDir'], 'Resources', 'pedata', 'nadcon', con.JTSK_TO_JTSK03_SHIFT_GRID_NADCON_NAME + '.LOS')

        if not os.path.exists(outNadconLasSrc):
            try:
                shutil.copyfile(inNadconLasSrc, outNadconLasSrc)
            except Exception as e:
                print('ERROR:', e)
                raise Exception('Try to launch scripts as admin or copy LAS and LOS NADCON files manually to ' + os.path.dirname(outNadconLasSrc) + ' from ' + os.path.dirname(inNadconLasSrc))
        if not os.path.exists(outNadconLosSrc):
            try:
                shutil.copyfile(inNadconLosSrc, outNadconLosSrc)
            except Exception as e:
                print('ERROR:', e)
                print('Try to launch scripts as admin or copy LAS and LOS NADCON files manually to ' + os.path.dirname(outNadconLasSrc) + ' from ' + os.path.dirname(inNadconLasSrc))
        
        # ArcGIS automatically adds the word PARAMETER (WTF?)
        # Result should be 'GEOGTRAN[METHOD["NADCON"],PARAMETER["Dataset_Slovakia_JTSK03_to_JTSK",0.0]]'
        # customGeoTransfm = 'GEOGTRAN[METHOD["NADCON"],' + con.JTSK_TO_JTSK03_SHIFT_GRID_NADCON_NAME.split('Dataset_')[1] + ',0.0]]'
        # customGeoTransfm = 'GEOGTRAN[METHOD[\"NADCON\"], PARAMETER[\"Slovakia_JTSK03_to_JTSK\", 0.0]]'
        customGeoTransfm = r'GEOGTRAN[METHOD["NADCON"],PARAMETER["Slovakia_JTSK03_to_JTSK", 0.0]]'
        try:
            arcpy.CreateCustomGeoTransformation_management(con.TRANSFORMATION_JTSK_TO_JTSK03, jtskSrs, jtsk03Srs, customGeoTransfm)
        except Exception as e:
            print('WARNING:', e)
        arcpy.env.overwriteOutput = False

    @staticmethod
    def copyPathWithCheck(fromPath, toPath):
        if not os.path.exists(fromPath):
            print('... WARNING: Source path', fromPath, 'does not exist')
            return
        if os.path.exists(toPath):
            print('... WARNING: Destination path', toPath, 'already exist')
            return
        print('... Copying', fromPath, 'to', toPath)
        shutil.copytree(fromPath, toPath)

    @staticmethod
    def getLasPointsCount(lasSrc):
        lasFile = laspy.file.File(lasSrc, mode = 'r')
        try:
            count = lasFile.header.records_count
        except Exception as e:
            print('Exception occured:', e)
        finally:
            lasFile.close()
        return count

    def createTinFromLas(self, inLasSrc, outTinSrc, inTileSize = 1000):
        print('... Creating TIN from the LAS', os.path.basename(inLasSrc))
        MAX_TIN_NODE_CNT = 15000000
        tinPath = os.path.dirname(outTinSrc)
        lasTileName = os.path.basename(inLasSrc).split('.las')[0]
        
        pntCnt = self.getLasPointsCount(inLasSrc)
        if pntCnt > MAX_TIN_NODE_CNT:
            tileSize = round(inTileSize / 2)
            print('... Input LAS is too large to create a TIN. Number of points', pntCnt,'exceeds the ArcGIS limit. Tiling with the tile_size parameter', tileSize)
            if not os.path.exists(os.path.join(tinPath, 'temp_smallerTiles')):
                os.mkdir(os.path.join(tinPath, 'temp_smallerTiles'))
            subprocess.run(['lastile64', '-i', inLasSrc, '-tile_size', str(tileSize), '-odir', os.path.join(tinPath, 'temp_smallerTiles'), '-o', 'tile_' + str(tileSize) + '.las'], shell = True)
            tilesPreparedSrcs = [os.path.join(tinPath, 'temp_smallerTiles', lasTile) for lasTile in os.listdir(os.path.join(tinPath, 'temp_smallerTiles')) if lasTile.startswith('tile_' + str(tileSize))]
            for tilePreparedSrc in tilesPreparedSrcs:
                outTinSrc = os.path.join(tinPath, 'tin_' + os.path.basename(tilePreparedSrc).split('.las')[0]) 
                self.createTinFromLas(tilePreparedSrc, outTinSrc, tileSize)
        else:
            arcpy.CheckOutExtension('3D')
            arcpy.LasDatasetToTin_3d(inLasSrc, outTinSrc, 'NONE', '', '', MAX_TIN_NODE_CNT)
            arcpy.CheckInExtension('3D')

        tempSmallerTilePath = os.path.join(tinPath, 'temp_smallerTiles')
        if os.path.exists(tempSmallerTilePath):
            if os.path.dirname(inLasSrc) == tempSmallerTilePath:
                os.remove(inLasSrc)
            if not os.listdir(tempSmallerTilePath): # path is empty
                shutil.rmtree(tempSmallerTilePath)

    def transformGdbFcsFromJtskToTm34(self, inGdbSrc, outTempWorkPath):
        print('Transforming feature classes of the GDB', os.path.basename(inGdbSrc), ' content from S-JTSK (JTSK) to ETRS89-TM34')
        if arcpy.Describe(inGdbSrc).workspaceType != 'LocalDatabase':
            raise Exception('Input path is not a GDB')
        fullOutGdbPath = os.path.join(outTempWorkPath, 'CUSTOMTRANSF', ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(20)))
        os.makedirs(fullOutGdbPath)
        arcpy.CreateFileGDB_management(fullOutGdbPath, os.path.basename(inGdbSrc), 'CURRENT')
        outGdbSrc = os.path.join(fullOutGdbPath, os.path.basename(inGdbSrc))

        arcpy.env.workspace = inGdbSrc
        fcs = arcpy.ListFeatureClasses()
        for fc in fcs:
            inFcSrc = os.path.join(inGdbSrc, fc)
            outFcSrc = os.path.join(outGdbSrc, fc + '_jtsk03')
            self.transformFcFromJtskToJtsk03Custom(inFcSrc, outFcSrc)
            inFcSrc = outFcSrc
            outFcSrc = os.path.join(outGdbSrc, fc)
            self.transformFcFromJtsk03ToTm34Custom(inFcSrc, outFcSrc)
            # self.waitForLockReleaseAndProcess('arcpy.Delete_management', fullOutGdbPath, fc + '_jtsk03')
        print('Feature classes transformed')
        return fullOutGdbPath
            
    def transformFcFromJtskToJtsk03Custom(self, inFcSrc, outFcSrc):
        print('... Transforming feature class', os.path.basename(inFcSrc), ' from S-JTSK (JTSK) to S-JTSK (JTSK03)')
        self.defineTransformationJtskToJtsk03Nadcon()
        outSrs = arcpy.SpatialReference(8353)
        arcpy.Project_management(inFcSrc, outFcSrc, outSrs, con.TRANSFORMATION_JTSK_TO_JTSK03)
        arcpy.Project_management(inFcSrc, outFcSrc, outSrs, "nadcon")

    def transformFcFromJtsk03ToTm34Custom(self, inFcSrc, outFcSrc):
        print('... Transforming feature class', os.path.basename(inFcSrc), ' from S-JTSK (JTSK03) to ETRS89-TM34')
        self.defineTransformationBessel1841ToGrs80()
        outSrs = arcpy.SpatialReference(3046)
        arcpy.Project_management(inFcSrc, outFcSrc, outSrs, con.TRANSFORMATION_BESSEL1841_TO_GRS80)

    @staticmethod
    def defineJtsk03SRSInLAS(lasSrc, pid=1):    
        print("... Defining S-JTSK (JTSK03) SRS to the LAS " + os.path.basename(lasSrc))

        logFileSrc = os.path.join(os.path.dirname(lasSrc),'batchDefineJtsk03SRSInLAS.txt')
        if not os.path.exists(logFileSrc):
            # create log file
            open(logFileSrc, 'w').close()

        try:
            aSrs = 'PROJCS["S-JTSK_[JTSK03]_Krovak_East_North",GEOGCS["S-JTSK_[JTSK03]",DATUM["S-JTSK_[JTSK03]",SPHEROID["Bessel_1841",6377397.155,299.1528128]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]],PROJECTION["Krovak"],PARAMETER["False_Easting",0.0],PARAMETER["False_Northing",0.0],PARAMETER["Pseudo_Standard_Parallel_1",78.5],PARAMETER["Scale_Factor",0.9999],PARAMETER["Azimuth",30.28813975277778],PARAMETER["Longitude_Of_Center",24.83333333333333],PARAMETER["Latitude_Of_Center",49.5],PARAMETER["X_Scale",-1.0],PARAMETER["Y_Scale",1.0],PARAMETER["XY_Plane_Rotation",90.0],UNIT["Meter",1.0],AUTHORITY["EPSG",8353]]'.replace('"', '\\"')
            lasSrcModif = lasSrc.replace('\\', '/')
            outLas = os.path.join(os.path.dirname(lasSrc), 'temp_' + os.path.basename(lasSrc))
            outLasModif = outLas.replace('\\', '/')
            pipeline = (
                '{'
                    '"pipeline":['
                        '"' + lasSrcModif + '",'
                        '{'
                            '"type":"writers.las",'
                            '"a_srs":"' + aSrs + '",'
                            '"minor_version":"4",'
                            '"dataformat_id": "6",'
                            '"filename":"' + outLasModif + '"'
                        '}'
                    ']'
                '}'
            )
            pipelineJsonName = 'temp_pipeline_' + os.path.basename(lasSrc).split('.las')[0] + '.json'
            pipelineFile = codecs.open(os.path.join(os.path.dirname(lasSrc), pipelineJsonName), 'w', 'utf-8')
            pipelineFile.write(pipeline)
            pipelineFile.close()
            subprocess.run(['type', os.path.join(os.path.dirname(lasSrc), pipelineJsonName), '|', 'pdal', 'pipeline', '-s'], shell=True)
            subprocess.run(['lasinfo', '-i', outLas, '-repair'], shell=True)
            os.remove(os.path.join(os.path.dirname(lasSrc), pipelineJsonName))
            
            if os.path.exists(outLas):
                os.remove(lasSrc)
                os.rename(outLas, lasSrc)
                
        except Exception as e:
            print("... Error in process", pid, ':', str(e))
            logfile = open(logFileSrc, 'a')
            logfile.write('Error occured in the process with PID ' + str(pid) + '\n')
            traceback.print_exc(file = logfile)
            logfile.close()
    
    def batchDefineJtsk03SRSInLAS(self, lasesPath, coresCount = 6):
        print("Batch define S-JTSK (JTSK03) SRS in LASes reside in the path", lasesPath)
        lasSrcs = [os.path.join(lasesPath, las) for las in os.listdir(lasesPath) if las.lower().endswith('.las')]
        print('... Number of LAS files:', len(lasSrcs))
        pid = 0
        i = 1
        jobs = []
        for lasSrc in lasSrcs:
            # multiprocessing
            pid += 1 
            print("... pid is ", pid)
            proc = multiprocessing.Process(target=self.defineJtsk03SRSInLAS, args=(lasSrc, pid))
            jobs.append(proc)
            proc.start()
            numofalive = 0
            for proc in jobs:
                if proc.is_alive():
                    numofalive += 1
            print("... Number of alive processes is ", numofalive)
            while numofalive >= coresCount:
                time.sleep(2)
                numofalive = 0
                for proc in jobs:
                    if proc.is_alive():
                        numofalive += 1
            i += 1
        for proc in jobs:
            proc.join()
        
        logFileSrc = os.path.join(lasesPath, 'batchDefineJtsk03SRSInLAS.txt')
        if os.path.exists(logFileSrc) and os.path.getsize(logFileSrc) == 0:
            os.remove(logFileSrc)
        print("... Batch SRS defined")

    def clipDmrByLas(self, dmrSrc, lasSrc, outDmrSrc, lasFootprintSrc=None):
        if not arcpy.Exists(dmrSrc):
            raise IOError('Input DMR ' + dmrSrc + ' does not exist')
        if not arcpy.Exists(lasSrc):
            raise IOError('Input LAS ' + lasSrc + ' does not exist')
        if not os.path.exists(os.path.dirname(outDmrSrc)):
            raise IOError('Output path ' + os.path.dirname(outDmrSrc) + ' does not exist')
        
        arcpy.env.overwriteOutput = True
        if not arcpy.Exists(lasFootprintSrc):
            print('... Creating LAS tile footprint of', os.path.basename(lasSrc))
            self.lasPath = os.path.dirname(lasSrc)
            if not os.path.exists(os.path.join(os.path.dirname(lasSrc), 'temp')):
                os.mkdir(os.path.join(os.path.dirname(lasSrc), 'temp'))
            if not arcpy.Exists(os.path.join(os.path.dirname(lasSrc), 'temp', 'temp.gdb')):
                arcpy.CreateFileGDB_management(os.path.join(os.path.dirname(lasSrc), 'temp'), 'temp.gdb', 'CURRENT')
            arcpy.env.workspace = os.path.join(os.path.dirname(lasSrc), 'temp', 'temp.gdb')
            self.extractLasEnvelopeToPolygonFC(os.path.basename(lasSrc), os.path.basename(lasSrc).split('.las')[0], os.path.dirname(lasSrc), shouldRemoveLasDataset = True)
            lasFootprintSrc = os.path.join(arcpy.env.workspace, os.path.basename(lasSrc).split('.las')[0])

        desc = arcpy.Describe(lasSrc).Extent
        envelope = str(round(desc.XMin)) + ' ' + str(round(desc.YMin)) + ' ' + str(round(desc.XMax)) + ' ' + str(round(desc.YMax))
        arcpy.Clip_management(dmrSrc, envelope, outDmrSrc, lasFootprintSrc, '', 'ClippingGeometry')
        arcpy.env.overwriteOutput = False
    
    def batchConvertVgiToFileGdbAndTransformToEtrs89Tm34(self, vgiFileSrcsOrNames: list, outGdbsRootPath):
        print('... Converting to File GDB and transforming to ETRS89-TM34', len(vgiFileSrcsOrNames), 'VGI files')
        if len(vgiFileSrcsOrNames) == 0:
            return None
        if not os.path.exists(os.path.join(self.qcWorkPath, 'temp')):
            os.mkdir(os.path.join(self.qcWorkPath, 'temp'))
        if not os.path.exists(outGdbsRootPath):
            os.mkdir(outGdbsRootPath)
        Rts = rtsComponent.RTS()
        tempLocalPath = ''
        for vgiFileSrcOrName in vgiFileSrcsOrNames:
            vgiPath = os.path.join(self.assetsPath, self.vgiDir)
            vgiName = vgiFileSrcOrName
            if os.path.isfile(vgiFileSrcOrName):
                vgiPath = os.path.dirname(vgiFileSrcOrName)
                vgiName = os.path.basename(vgiFileSrcOrName)
            print('... VGI path', vgiPath)
            print('... VGI name', vgiName)
            if not os.path.exists(os.path.join(vgiPath, vgiName)):
                print('... VGI file', vgiName, 'could not be found at', vgiPath)
                continue
            tempRemoteDir = Rts.uploadToRts(vgiPath, vgiName)
            tempLocalPath = Rts.convert('VGI', 'GEODATABASE_FILE', tempRemoteDir, os.path.join(self.qcWorkPath, 'temp'))
            gdb = [gdb for gdb in os.listdir(tempLocalPath) if gdb.lower().endswith('.gdb')][0]
            tempRemoteDir = Rts.uploadToRts(tempLocalPath, gdb)
            tempLocalPathToRm = tempLocalPath
            tempLocalPath = Rts.transformGdb('S-JTSK (JTSK)', 'ETRS89-TM34', tempRemoteDir, outGdbsRootPath)
            # tempLocalPath = self.transformGdbFcsFromJtskToTm34(os.path.join(tempLocalPath, gdb), outGdbsRootPath)
            shutil.rmtree(tempLocalPathToRm)
        return tempLocalPath

    def addZInformationToPolygonsFc(self, polygonsFcSrc, dmrSrc):
        print('... Adding Z information to', os.path.basename(polygonsFcSrc), 'from', dmrSrc)
        workPath = os.path.dirname(polygonsFcSrc)
        fcName = os.path.basename(polygonsFcSrc)
        desc = arcpy.Describe(polygonsFcSrc)
        if desc.dataElementType == 'DEShapeFile':
            ext = '.shp'
        elif desc.dataElementType == 'DEFeatureClass':
            ext = ''
        arcpy.CheckOutExtension('3D')
        arcpy.InterpolateShape_3d(dmrSrc, polygonsFcSrc, os.path.join(workPath, 'temp_3D' + ext), '', 1, 'NEAREST', 'VERTICES_ONLY',  '', 'PRESERVE')
        arcpy.CheckInExtension('3D')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', workPath, fcName)
        arcpy.Copy_management(os.path.join(workPath, 'temp_3D' + ext), os.path.join(workPath, fcName))
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', workPath, os.path.join(workPath, 'temp_3D' + ext))

    def extractBuildingsFromVgiLikeGdb(self, gdbSrc, outGdbSrc, outBuildingsFcName):
        print('... Extracting buildings from VGI like GDB', os.path.basename(gdbSrc))
        if not arcpy.Exists(os.path.join(gdbSrc, 'kladpar_druhypozemkov')):
            raise IOError('Feature class kladpar_druhypozemkov is needed and cannot be found in ' + gdbSrc)
        parcelsSrc = os.path.join(os.path.join(gdbSrc, 'kladpar_plochy'))
        if not arcpy.Exists(parcelsSrc):
            raise IOError('Feature class kladpar_plochy is needed and cannot be found in ' + gdbSrc)
        
        if not os.path.exists(outGdbSrc):
            arcpy.CreateFileGDB_management(os.path.dirname(outGdbSrc), os.path.basename(outGdbSrc))
        if arcpy.Exists(os.path.join(outGdbSrc, outBuildingsFcName)):
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', outGdbSrc, outBuildingsFcName)
        arcpy.management.CreateFeatureclass(outGdbSrc, outBuildingsFcName, 'POLYGON', parcelsSrc,  'DISABLED', 'ENABLED')        
            
        arcpy.env.workspace = os.path.join(outGdbSrc)
        where = "SYMBOL = 45" # symbol 45 represents noncombustible structures
        arcpy.MakeFeatureLayer_management(os.path.join(gdbSrc, 'kladpar_druhypozemkov'), 'kladpar_druhypozemkov_nespalna_stavba_lyr', where)
        arcpy.MakeFeatureLayer_management(parcelsSrc, 'kladpar_plochy_lyr')
        arcpy.SelectLayerByLocation_management('kladpar_plochy_lyr', 'INTERSECT', 'kladpar_druhypozemkov_nespalna_stavba_lyr', "", 'NEW_SELECTION')
        arcpy.Append_management('kladpar_plochy_lyr', outBuildingsFcName)
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'kladpar_druhypozemkov_nespalna_stavba_lyr')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'kladpar_plochy_lyr')
        arcpy.env.workspace = None
        return os.path.join(outGdbSrc, outBuildingsFcName)

    def listIntersectingVgiFileNames(self, polygonFcSrc, relationship = 'INTERSECT'):
        print('... Finding intersecting VGI files with', os.path.basename(polygonFcSrc))
        vgiList = [vgi for vgi in os.listdir(os.path.join(self.assetsPath, self.vgiDir)) if vgi.lower().endswith('.vgi')]
        IDN5List = []
        arcpy.env.workspace = os.path.join(self.qcWorkPath, self.qcGdb)
        cadastralAreasSrc = os.path.join(self.assetsPath, self.cadastralAreasDir, self.cadastralAreasGdb, self.cadastralAreasFcTm34)
        arcpy.MakeFeatureLayer_management(polygonFcSrc, 'selection_poly_lyr')
        arcpy.MakeFeatureLayer_management(cadastralAreasSrc, 'cadastral_areas_lyr')
        arcpy.SelectLayerByLocation_management('cadastral_areas_lyr', relationship, 'selection_poly_lyr', '', 'NEW_SELECTION')

        sc = arcpy.da.SearchCursor('cadastral_areas_lyr', ['IDN5'])
        for row in sc:
            IDN5List.append(row[0])
        
        relevantVgiList = []
        for IDN5 in IDN5List:
            for vgi in vgiList:
                if str(IDN5) in vgi:
                    relevantVgiList.append(vgi)
                    break

        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'selection_poly_lyr')
        self.waitForLockReleaseAndProcess('arcpy.Delete_management', arcpy.env.workspace, 'cadastral_areas_lyr')
        return relevantVgiList

    @classmethod
    def listAllNestedGdbSrcs(cls, path):
        subpaths = [os.path.join(path, subdir) for subdir in os.listdir(path)]
        gdbSrcs = []
        for subpath in subpaths:
            if subpath.lower().endswith('.gdb'):
                gdbSrcs.append(subpath)
            elif os.path.isdir(subpath):
                gdbSrcs.extend(cls.listAllNestedGdbSrcs(subpath))
        return gdbSrcs

    def reclassifyLASPointsUnderPolygon(self, lasSrc, polygonsFcSrc, inClassList=[], toClass=21, arcToolName='SetLasClassCodesUsingFeatures_3d', shouldRollbackClassif=True):
        print('... Reclassifying LAS points under polygon', polygonsFcSrc, ' of the LAS', lasSrc, ' to the class', toClass)
        arcpy.CheckOutExtension('3D')
        workPath = os.path.dirname(lasSrc)
        if shouldRollbackClassif:
            self.rollbackLasClassificationToGround(lasSrc, toClass)
        self.makeBackupOfLas(lasSrc)
        
        if not os.path.exists(os.path.join(workPath, 'temp')):
            os.mkdir(os.path.join(workPath, 'temp'))
        if not os.path.exists(os.path.join(workPath, 'temp', 'temp.gdb')):
            arcpy.CreateFileGDB_management(os.path.join(workPath, 'temp'), 'temp.gdb')
        arcpy.env.workspace = os.path.join(workPath, 'temp', 'temp.gdb')
        arcpy.env.overwriteOutput = True

        lasDatasetSrc = os.path.join(workPath, 'temp.lasd')
        if arcpy.Exists(lasDatasetSrc):
            os.remove(lasDatasetSrc)
            print('... Existing LAS dataset removed')
        
        arcpy.CreateLasDataset_management(lasSrc, lasDatasetSrc)
        print('... Making LAS dataset layer')
        lasDatasetLyr = arcpy.MakeLasDatasetLayer_management(lasDatasetSrc, 'temp_lasDatasetLyr', inClassList, '')

        if arcToolName == 'LocateLasPointsByProximity_3d':
            multipointLow = self.getMultipointGeometryFromExtentWithZ(polygonsFcSrc, 0)
            multipointHigh = self.getMultipointGeometryFromExtentWithZ(polygonsFcSrc, 1000)
            arcpy.management.CreateFeatureclass(os.path.join(workPath, 'temp', 'temp.gdb'), 'multipointLow', 'MULTIPOINT', has_z = 'ENABLED')
            arcpy.management.CreateFeatureclass(os.path.join(workPath, 'temp', 'temp.gdb'), 'multipointHigh', 'MULTIPOINT', has_z = 'ENABLED')
            editor = arcpy.da.Editor(os.path.join(workPath, 'temp', 'temp.gdb'))
            editor.startEditing()
            editor.startOperation()
            ic = arcpy.da.InsertCursor(os.path.join(workPath, 'temp', 'temp.gdb', 'multipointLow'), ['SHAPE@'])
            ic.insertRow([multipointLow])
            ic = arcpy.da.InsertCursor(os.path.join(workPath, 'temp', 'temp.gdb', 'multipointHigh'), ['SHAPE@'])
            ic.insertRow([multipointHigh])
            del ic
            editor.stopOperation()
            editor.stopEditing(True)
            del editor
            print('... Creating high and low TIN')
            arcpy.ddd.CreateTin(os.path.join(workPath, 'tinLow'), '', '{0} Shape.Z masspoints'
                .format(os.path.join(workPath, 'temp', 'temp.gdb', 'multipointLow')), 'DELAUNAY')
            arcpy.ddd.CreateTin(os.path.join(workPath, 'tinHigh'), '', '{0} Shape.Z masspoints'
                .format(os.path.join(workPath, 'temp', 'temp.gdb', 'multipointHigh')), 'DELAUNAY')
            if arcpy.Exists(os.path.join(workPath, 'temp', 'temp.gdb', 'exrtuded_' + os.path.basename(polygonsFcSrc).split('.')[0])):
                self.waitForLockReleaseAndProcess('arcpy.Delete_management', os.path.join(workPath, 'temp', 'temp.gdb'), 'exrtuded_' + os.path.basename(polygonsFcSrc).split('.')[0])
            print('... Extruding polygons between TINs')
            arcpy.ddd.ExtrudeBetween(os.path.join(workPath, 'tinLow'), os.path.join(workPath, 'tinHigh'), 
                polygonsFcSrc, os.path.join(workPath, 'temp', 'temp.gdb', 'exrtuded_' + os.path.basename(polygonsFcSrc).split('.')[0]))
            
            # Find LAS points in the multipatch (from 2D polygon)
            print('... Locating LAS points in extruded polygons')
            arcpy.LocateLasPointsByProximity_3d(lasDatasetLyr, os.path.join(workPath, 'temp', 'temp.gdb', 'exrtuded_' + os.path.basename(polygonsFcSrc).split('.')[0]), 
                0.1, 'lasPntCnt', os.path.join(workPath, 'las_pnts_found_' + os.path.basename(lasSrc).split('.las')[0] + '.shp'), 'POINT', toClass, 'NO_COMPUTE_STATS')

            os.remove(os.path.join(workPath, 'tinHigh'))
            os.remove(os.path.join(workPath, 'tinLow'))
        else:
            print('... Locating LAS points under polygons')
            arcpy.ddd.SetLasClassCodesUsingFeatures(lasDatasetLyr, polygonsFcSrc + ' 0 ' + str(toClass) + ' NO_CHANGE NO_CHANGE NO_CHANGE', 'NO_COMPUTE_STATS')

        arcpy.env.workspace = None
        arcpy.env.overwriteOutput = False
        os.remove(lasDatasetSrc)
        self.removeBackupOfLas(lasSrc)
        arcpy.CheckInExtension('3D')
        
    @staticmethod
    def getMultipointGeometryFromExtentWithZ(polygonsFcSrc, Z):
        extent = arcpy.Describe(polygonsFcSrc).Extent
        pnt1 = arcpy.Point(extent.XMin, extent.YMin, Z)
        pnt2 = arcpy.Point(extent.XMin, extent.YMax, Z)
        pnt3 = arcpy.Point(extent.XMax, extent.YMin, Z)
        pnt4 = arcpy.Point(extent.XMax, extent.YMax, Z)
        pntArr = arcpy.Array([pnt1, pnt2, pnt3, pnt4])
        print('...', pnt1)
        print('...', pntArr.getObject(0))
        spatial_reference = None
        has_z = True
        multipoint = arcpy.Multipoint(pntArr, spatial_reference, has_z)
        print('...', multipoint.getPart(0))
        return multipoint
    
    def extractLASPointsToClusters(self, lasSrc, extractClasses: list, outBuildingClustersSrc, dmrSrc):
        print('... Extracting LAS point to SHP clusters')
        if not os.path.exists(lasSrc):
            raise IOError('LAS file ' + lasSrc + ' does not exist')
        if os.path.dirname(outBuildingClustersSrc).lower().endswith('.gdb'):
            raise Exception('Output path cannot be a geodatabase')
        if not os.path.exists(os.path.join(os.path.dirname(lasSrc), 'temp')):
            os.mkdir(os.path.join(os.path.dirname(lasSrc), 'temp'))
        if not os.path.exists(os.path.dirname(outBuildingClustersSrc)):
            os.makedirs(os.path.dirname(outBuildingClustersSrc))
        elif arcpy.Exists(outBuildingClustersSrc):
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', os.path.dirname(outBuildingClustersSrc), os.path.basename(outBuildingClustersSrc))
        tempShpSrc = os.path.join(os.path.dirname(lasSrc), 'temp', 'temp_extractedPnts.shp')
        if arcpy.Exists(tempShpSrc):
            self.waitForLockReleaseAndProcess('arcpy.Delete_management', os.path.dirname(tempShpSrc), os.path.basename(tempShpSrc))
        subprocess.run(['las2shp', '-i', lasSrc, '-keep_class', ' '.join([str(item) for item in extractClasses]), '-o', tempShpSrc], shell = True)        
        self.make3DClustersFromPoints(os.path.dirname(lasSrc), tempShpSrc, outBuildingClustersSrc, dmrSrc)
        
    @staticmethod
    def make3DClustersFromPoints(workPath, inPntsSrc, outClustersSrc, dmrSrc):
        if workPath.lower().endswith('.gdb'):
            raise Exception('Work path cannot be a geodatabase')
        if not os.path.exists(os.path.join(workPath, 'temp')):
            os.mkdir(os.path.join(workPath, 'temp'))
        arcpy.env.overwriteOutput = True
        print('... Buffering')
        arcpy.Buffer_analysis(inPntsSrc, os.path.join(workPath, 'temp', 'temp_buffer.shp'), 1, 'FULL', 'FLAT', 'ALL', '', 'PLANAR')
        print('... Multipart to singlepart')
        arcpy.MultipartToSinglepart_management(os.path.join(workPath, 'temp', 'temp_buffer.shp'),
            os.path.join(workPath, 'temp', 'temp_singlepart.shp'))
        arcpy.CheckOutExtension('3D')
        print('... Interpolating shape 3D')
        arcpy.InterpolateShape_3d(dmrSrc, os.path.join(workPath, 'temp', 'temp_singlepart.shp'), outClustersSrc, '', 1, 'NEAREST', 'VERTICES_ONLY',  '', 'PRESERVE')
        arcpy.CheckInExtension('3D')
        arcpy.env.overwriteOutput = False
    
    def rollbackLasClassificationToGround(self, inLasSrc, defectClass):
        print('... Rolling back the classification from defect class (' + str(defectClass) + ') to ground class (2)')
        if not os.path.exists(inLasSrc):
            raise IOError('Provided LAS file ' + inLasSrc + ' does not exist')
        restoredLasSrc = os.path.join(os.path.dirname(inLasSrc), 'temp_restored.las')
        subprocess.run(['las2las64', '-i', inLasSrc, '-change_classification_from_to', str(defectClass), '2', '-o', restoredLasSrc])
        os.remove(inLasSrc)
        os.rename(restoredLasSrc, inLasSrc)

    @staticmethod
    def thinBuildingsFcWithNegativeBuffer(origBuildingsFcSrc, bufferM):
        print('... Thinning original buildings', os.path.basename(origBuildingsFcSrc), 'with negative buffer', bufferM)
        if not os.path.exists(os.path.join(os.path.dirname(origBuildingsFcSrc), 'temp')):
            os.mkdir(os.path.join(os.path.dirname(origBuildingsFcSrc), 'temp'))
        arcpy.env.workspace = os.path.join(os.path.dirname(origBuildingsFcSrc), 'temp')
        arcpy.env.overwriteOutput = True
        dissFcSrc = os.path.join(os.path.join(os.path.dirname(origBuildingsFcSrc), 'temp', 'temp_dissolvedBuildings.shp'))
        arcpy.management.Dissolve(origBuildingsFcSrc, dissFcSrc, '', '', 'SINGLE_PART', 'UNSPLIT_LINES')
        buffFcSrc = os.path.join(os.path.join(os.path.dirname(origBuildingsFcSrc), 'temp', 'temp_bufferedBuildings.shp'))
        arcpy.Buffer_analysis(dissFcSrc, buffFcSrc, str(-1 * bufferM) + ' Meters', 'FULL', 'FLAT', 'ALL','', 'PLANAR')
        spFcSrc = os.path.join(os.path.join(os.path.dirname(origBuildingsFcSrc), 'temp', 'temp_spBuildings.shp'))
        arcpy.management.MultipartToSinglepart(buffFcSrc, spFcSrc)
        thinnedFcSrc = os.path.join(os.path.dirname(origBuildingsFcSrc), 'thinnedBuildings_' + str(bufferM).replace('.', '_') + '.shp')
        arcpy.CopyFeatures_management(spFcSrc, thinnedFcSrc)
        arcpy.env.overwriteOutput = False
        shutil.rmtree(os.path.join(os.path.dirname(origBuildingsFcSrc), 'temp'))
        return thinnedFcSrc

    def updateHGTByDSMMinusDTM(self, inHgtFcSrc, outUpdatedHgtFcSrc, holesFcSrc, dsmSrc, dtmSrc, dsmZonalStatType='MAXIMUM', dtmZonalStatType='MINIMUM', pntLnBuffM = 2.5):
        print('Updating HGT of', os.path.basename(inHgtFcSrc), 'by DSM', os.path.basename(dsmSrc), 'and DTM', os.path.basename(dtmSrc))
        if not arcpy.Exists(inHgtFcSrc):
            raise IOError('Input feature class ' + inHgtFcSrc + ' does not exist')
        if not arcpy.Exists(dsmSrc):
            raise IOError('Input DSM ' + inHgtFcSrc + ' does not exist')
        if not arcpy.Exists(dtmSrc):
            raise IOError('Input DTM ' + inHgtFcSrc + ' does not exist')
        if not os.path.exists(os.path.join(self.qcWorkPath, 'temp')):
            os.mkdir(os.path.join(self.qcWorkPath, 'temp'))
        if arcpy.Exists(os.path.join(self.qcWorkPath, 'temp', 'temp.gdb')):
            arcpy.Delete_management(os.path.join(self.qcWorkPath, 'temp', 'temp.gdb'))
        arcpy.CreateFileGDB_management(os.path.join(self.qcWorkPath, 'temp'), 'temp.gdb', 'CURRENT')
        arcpy.env.workspace = os.path.join(self.qcWorkPath, 'temp', 'temp.gdb')
        arcpy.env.cellSize = 0.5 # For zonal statistic rasters
        desc = arcpy.Describe(inHgtFcSrc)

        zoneField = ''
        if desc.hasOID:
            zoneField = desc.OIDFieldName
            print('... Zone field set to the field', desc.OIDFieldName)
        else:
            for field in desc.fields:
                if field.type == 'Guid' or field.type == 'GlobalID':
                    zoneField = field.name
                    print('... Zone field set to the field', field.name)
                    break
        if len(zoneField) == 0:
            raise Exception('No appropriate zone field found. Please add object ID field to the input feature class.')

        helperFcSrc = inHgtFcSrc
        if desc.shapeType == 'Point':
            print('... Input shape type is', desc.shapeType, ', applying buffer', pntLnBuffM, '(meters)')
            outName = os.path.basename(inHgtFcSrc).split('.')[0] + '_buffer'
            arcpy.Buffer_analysis(inHgtFcSrc, outName, pntLnBuffM, 'FULL', 'ROUND', 'NONE', '', 'PLANAR')
            helperFcSrc = os.path.join(arcpy.env.workspace, outName)
        elif desc.shapeType == 'Multipoint':
            print('... Input shape type is', desc.shapeType, ', making singlepart and applying buffer', pntLnBuffM, '(meters)')
            spName = os.path.basename(inHgtFcSrc).split('.')[0] + '_sp'
            arcpy.MultipartToSinglepart_management(inHgtFcSrc, spName)
            outName = os.path.basename(inHgtFcSrc).split('.')[0] + '_buffer'
            arcpy.Buffer_analysis(spName, outName, pntLnBuffM, 'FULL', 'ROUND', 'NONE', '', 'PLANAR')
            helperFcSrc = os.path.join(arcpy.env.workspace, outName)
        elif desc.shapeType in ['MultiPatch', 'Polyline']:
            raise IOError('Input HGT feature class must not be of the type ' + desc.shapeType)
        arcpy.CheckOutExtension('Spatial')
        print('... Rasterizing input features')
        dsmZonalRst = arcpy.sa.ZonalStatistics(helperFcSrc, zoneField, dsmSrc, dsmZonalStatType, 'DATA')
        dsmZonalRst.save(os.path.basename(helperFcSrc).split('.shp')[0] + '_dsm_zones_' + dsmZonalStatType)
        dtmZonalRst = arcpy.sa.ZonalStatistics(helperFcSrc, zoneField, dtmSrc, dtmZonalStatType, 'DATA')
        dtmZonalRst.save(os.path.basename(helperFcSrc).split('.shp')[0] + '_dtm_zones_' + dtmZonalStatType)
        minusZonalRst = arcpy.sa.Minus(dsmZonalRst, dtmZonalRst)
        minusZonalRst.save(os.path.basename(helperFcSrc).split('.shp')[0] + '_minus_zones')
        
        if desc.shapeType == 'Polygon':
            print('... Extracting centroids')
            centroidsFc = 'centroids_' + os.path.basename(inHgtFcSrc).split('.shp')[0]
            arcpy.FeatureToPoint_management(inHgtFcSrc, centroidsFc, 'INSIDE')
        else: # Point feature class
            centroidsFc = inHgtFcSrc
        
        centroidsWithHGTFc = os.path.join(arcpy.env.workspace, 'centroids_hgt_' + os.path.basename(inHgtFcSrc).split('.shp')[0])
        arcpy.sa.ExtractValuesToPoints(centroidsFc, minusZonalRst, centroidsWithHGTFc, 'NONE', 'VALUE_ONLY')
        where = 'RASTERVALU IS NULL'
        arcpy.MakeFeatureLayer_management(centroidsWithHGTFc, 'temp_centroids_lyr', where)
        arcpy.CalculateField_management('temp_centroids_lyr', 'RASTERVALU', -9999)
        joinedFc = os.path.join(arcpy.env.workspace, 'joined_' + os.path.basename(inHgtFcSrc).split('.shp')[0])
        arcpy.CheckInExtension('Spatial')
        self.spatialJoinWrapper(inHgtFcSrc, centroidsWithHGTFc, joinedFc, joinOperation = 'JOIN_ONE_TO_ONE', addJoinFcFields = ['RASTERVALU'], mergeRule = 'first')
        self.autoHgtFinalTuning(joinedFc, outUpdatedHgtFcSrc, holesFcSrc)
        print('HGT automatically updated')

    def autoHgtFinalTuning(self, inHgtFcSrc, outHgtFcSrc, holesFcSrc):
        arcpy.management.AlterField(inHgtFcSrc, 'RASTERVALU' , 'HGT_AUTO', 'Automaticky získaná výška nad povrchom (m)')
        arcpy.CalculateField_management(inHgtFcSrc, 'HGT_AUTO', 'round(!HGT_AUTO!, 1)', 'PYTHON3')       
        arcpy.MakeFeatureLayer_management(inHgtFcSrc, 'temp_hgt_fc_lyr')
        arcpy.MakeFeatureLayer_management(holesFcSrc, 'temp_holes_fc_lyr')
        arcpy.management.SelectLayerByLocation('temp_hgt_fc_lyr', 'COMPLETELY_WITHIN', 'temp_holes_fc_lyr', 0, 'NEW_SELECTION', 'NOT_INVERT')
        where = 'ACH = 997 OR HGT_AUTO < 2'
        arcpy.management.SelectLayerByAttribute('temp_hgt_fc_lyr', 'ADD_TO_SELECTION', where)
        arcpy.CalculateField_management('temp_hgt_fc_lyr', 'HGT_AUTO', -32767, 'PYTHON3')
        where = 'EXS = 33'
        arcpy.management.SelectLayerByAttribute('temp_hgt_fc_lyr', 'NEW_SELECTION', where)
        arcpy.CalculateField_management('temp_hgt_fc_lyr', 'HGT_AUTO', -32768, 'PYTHON3')
        if arcpy.Exists('temp_hgt_fc_lyr'):
            arcpy.Delete_management('temp_hgt_fc_lyr')
        if arcpy.Exists('temp_holes_fc_lyr'):
            arcpy.Delete_management('temp_holes_fc_lyr')
        arcpy.AddField_management(inHgtFcSrc, 'HGT_DELTA', 'FLOAT')
        where = 'HGT >= 0 AND HGT_AUTO >= 0'
        arcpy.MakeFeatureLayer_management(inHgtFcSrc, 'temp_positive_hgt_lyr', where)
        arcpy.CalculateField_management('temp_positive_hgt_lyr', 'HGT_DELTA', 'abs(!HGT_AUTO! - !HGT!)', 'PYTHON3')
        if arcpy.Exists('temp_positive_hgt_lyr'):
            arcpy.Delete_management('temp_positive_hgt_lyr')
        retainFields = ['ZBGIS_ID', 'HGT', 'HGT_AUTO', 'HGT_DELTA', 'ACH', 'EXS']
        deleteFields = [field.name for field in arcpy.ListFields(inHgtFcSrc) if not field.required]
        deleteFields = [field for field in deleteFields if field not in retainFields]
        arcpy.management.DeleteField(inHgtFcSrc, deleteFields)
        arcpy.CopyFeatures_management(inHgtFcSrc, outHgtFcSrc)

    def copyPath(self, src, dst):
        items = [os.path.join(src, item) for item in os.listdir(src)]
        for item in items:
            if item.lower().endswith('.gdb'):
                print('... Moving GDB', item)
                arcpy.management.Copy(item, os.path.join(dst, os.path.basename(item)))
                arcpy.management.Delete(item)
            elif item.lower().endswith('.lock'):
                pass
            elif os.path.isfile(item):
                print('... Moving item', item)
                shutil.copyfile(item, os.path.join(dst, os.path.basename(item)))
            else:
                os.makedirs(os.path.join(dst, os.path.basename(item)))
                self.copyPath(item, os.path.join(dst, os.path.basename(item)))
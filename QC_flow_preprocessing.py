###################################################################
# File: QC_flow_preprocessing.py                 ###    #####    ##
# File version: 28/01/2019                       ##  ##  ###  #####
# Python version: 3.*.*                          ##  ##  ###  #####
# Author: Msc. Martin Kalivoda                   ###   #  ###    ##
###################################################################

from components import QC_initDb as qcInitDbComponent
from components import QC_populateDb as qcPopulateDbComponent
from components import QC_misc as qcMiscComponent
from components import QC_preprocessing as qcPreprocessingComponent
from components import beforeProcess as beforeProcessComponent
import time

# Begin assigning variables ###############################################################################################################################################################################################

qcWorkPath = r"F:\Testing\preprocessing\PartLOT06"
qcGdb = "selectedHD"
lotNum = 6
lasPath = r"F:\Testing\preprocessing\PartLOT06\las"
pixelSizeOfHDRasters = 1
densityConstraint = 15
overlapConstraint = 40
coresCount = 6
flightPointsDir = r'F:\Testing\checkingGpsSignalVoids\LOT02\Body letovej dráhy'
timeAttributeName = 'Time'
dateAttributeName = 'Date'
timeFormat = '%H:%M:%S'
dateFormat = '%d.%m.%Y'
bufferM = 150
fromPid = 0

# End assigning variables #################################################################################################################################################################################################


# Begin instantiation #####################################################################################################################################################################################################

idb = qcInitDbComponent.QC_initDb(qcWorkPath, qcGdb, lotNum)
pdb = qcPopulateDbComponent.QC_populateDb(lasPath, qcWorkPath, qcGdb, lotNum)
prepro = qcPreprocessingComponent.QC_preprocessing(lasPath, qcWorkPath, qcGdb, lotNum)
misc = qcMiscComponent.QC_misc(lasPath, qcWorkPath, qcGdb, lotNum)

# End instantiation #######################################################################################################################################################################################################

# Begin performing checks #################################################################################################################################################################################################

if __name__ == "__main__":
    bp = beforeProcessComponent.BeforeProcess
    bp.check()
    bp.checkConnectionToResource(qcWorkPath, 'qcWorkPath')
    bp.checkConnectionToResource(lasPath, 'lasPath')
    bp.checkConnectionToResource(flightPointsDir, 'flightPointsDir')

# End performing checks ###################################################################################################################################################################################################

# Begin list of methods to be executed ####################################################################################################################################################################################

    # print(time.strftime(' %c'))
    # idb.initGdbs()
    # print(time.strftime(' %c'))
    # idb.initTables_prepro()
    # print(time.strftime(' %c'))
    # pdb.populateLasFields_prepro()
    # print(time.strftime(' %c'))
    # pdb.populateOverlapInfo_prepro()
    # print(time.strftime(' %c'))
    # prepro.checkFileName()
    # print(time.strftime(' %c'))
    # prepro.checkFileSize()
    # print(time.strftime(' %c'))
    # prepro.checkLasCover()
    # print(time.strftime(' %c'))
    # prepro.checkDensity(densityConstraint)
    # print(time.strftime(' %c'))
    # pdb.populateLotStatistics_prepro()
    # print(time.strftime(' %c'))
    # misc.reportLotStatistics()
    # print(time.strftime(' %c'))
    # prepro.checkPointSpacing()
    # print(time.strftime(' %c'))
    # prepro.checkOverlap(overlapConstraint)
    # print(time.strftime(' %c'))
    # prepro.checkFormatAndAttributes()
    # print(time.strftime(' %c'))
    # prepro.checkCRS()
    # print(time.strftime(' %c'))
    # prepro.calculateStatsOnLowDensityPolygons()
    # print(time.strftime(' %c'))
    # misc.createCentroidsFromLasFilenames(1)
    print(time.strftime(' %c'))
    prepro.checkRelativeHeightAccuracy(coresCount=coresCount, pixelSize = pixelSizeOfHDRasters, fromPid = fromPid)
    print(time.strftime(' %c'))
    # prepro.createVoidPolygonsByGpsTimeOnWholeLot(flightPointsDir, timeAttributeName, timeFormat, dateAttributeName, dateFormat, bufferM)
    # print(time.strftime(' %c'))
    # misc.writeGpsTimesOfLasesToFile()
    # print(time.strftime(' %c'))

# End list of methods to be executed ######################################################################################################################################################################################